<?php require_once('core/system.php');
if(empty($_GET['p'])){ $page = 1; } else { $page = $_GET['p']; }
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20; $start=($previous*$limit);
$a = new database();
$a->query('SELECT * FROM wo ORDER BY wo_id DESC LIMIT '.$start.', '.$limit);
$a->execute();
$b = $a->resultset();
$c = new database();
$c->query('SELECT COUNT(*) as TheCounts FROM wo');
$c->execute();
$d = $c->resultset();
foreach ($d as $d) {
  $TheCount = $d['TheCounts'];
}
$lastpage = ceil($TheCount/$limit);
$secondtolast = $lastpage - 1;
$thirdtolast = $lastpage - 2;
$call = "WorksShow";
PrepPagination();
?>
<h4 class="display-5">Works Commencing</h4>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>WO #</th>
      <th>Customer</th>
      <th>Qty</th>
      <th>Product</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
<?php
foreach ($b as $b) {
  $wo = $b['wo_no'];
  $cn = $b['CustomerName'];
  $qty = $b['qty_o'];
  $fn = $b['FriendlyName'];
  echo '<tr ondblclick="WorksView(\''.$wo.'\')">
    <td>'.$wo.'</td>
    <td>'.$cn.'</td>
    <td>'.$qty.'</td>
    <td>'.$fn.'</td>
    <td>
      <button id="Actions" type="button" class="badge badge-primary btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
        <h6 class="dropdown-header">Actions for Works Order '.$wo.'</h6>
        <button class="btn dropdown-item" onclick="WorksView(\''.$wo.'\')"><b>View</b></button>
        <button class="btn dropdown-item" onclick="WorksEdit(\''.$wo.'\')">Edit</button>
      </div>
    </td>
  </tr>'; }
print_r(' </tbody>
</table>
<div class="row justify-content-center">
  <nav aria-label="Pages for Works Orders">');
  Pagination();
print_r('</nav>
</div>'); ?>
