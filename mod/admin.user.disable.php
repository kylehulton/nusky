<?php require_once('core/system.php');
$u = htmlspecialchars($_POST['u']);
$status = 'Disabled';
$audu = new database();
$audu->query('UPDATE login SET status=:status WHERE username=:username');
$audu->bind(':username', $u);
$audu->bind(':status', $status);
$audu->execute();
?>
