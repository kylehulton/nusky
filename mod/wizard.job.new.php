<?php require_once("core/system.php");
function newJobNo(){
  $db = new Database();
  $db->query("SELECT Job_NO FROM jobs ORDER BY Job_NO DESC LIMIT 1");
  $db->execute();
  $GotYourNumber = $db->resultset();
  foreach ($GotYourNumber as $jn){
    echo $jn['Job_NO']+1;
  }
} ?>
<h4 class="display-5">New Job Wizard</h4>
<div id="innerJobCreator" class="carousel slide" data-interval="false" data-wrap="false">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <h5>Step 1 - Create Job...</h5>
      <form autocomplete="off">
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Job No #</label>
          <div class="col-sm-10">
            <input type="text" class="form-control-plaintext form-control-sm" id="Job_NO" value="<?php newJobNo(); ?>" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Customer</label>
          <div class="col-sm-10">
            <input type="text" class="form-control form-control-sm" name="Customer" id="Customer" required >
          </div>
          <div class="col-sm-12" id="Customer_Autofill"></div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Comments</label>
          <div class="col-sm-10">
            <input type="text" id="Comments" class="form-control form-control-sm">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Additional Notes</label>
          <div class="col-sm-10">
            <textarea id="Notes" class="form-control form-control-sm"></textarea>
          </div>
        </div>
      </form>
      <div class="row justify-content-between align-items-center pb-2">
        <div class="col" id="nje1"></div>
        <div class="col-md-auto">
          <a class="btn btn-primary text-white" id="njnb1" onClick="Step1toOverview()">Next</a>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <h5>Step 2 - Add Products...</h5>
      <div class="form-group row">
        <div class="col-sm-12">
          <table class="table table-hover table-sm njs2t" id="ProductList">
            <thead>
              <th>Products</th>
              <th>Qty</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <form autocomplete="off">
        <div class="form-group row">
          <div class="col-sm-8">
            <input id="Product" name="Product" class="form-control form-control-sm" placeholder="Product" required >
          </div>
          <div class="col-sm-2">
            <input type="text" class="form-control form-control-sm" id="Qty" placeholder="Qty" required >
          </div>
          <div class="col-sm-2">
            <button type="button" class="btn btn-primary btn-sm" id="AddtoList" onclick="Step2AddProduct()">Add to List</button>
          </div>
        </div>
        <div class="form-group row">
          <div class="col pt-2">
            <button type="button" class="btn btn-danger btn-sm" id="RemoveLastItem" onclick="Step2RemoveProduct()">Remove Last Item</button>
          </div>
        </div>
        <div class="form-group row">
          <div class="col" id="Products_Autofill"></div>
        </div>
      </form>
      <div class="row justify-content-between align-items-center pb-2">
        <div class="col-md-auto">
          <a class="btn btn-secondary text-white" id="njbb1" onclick="prevSlide()">Back</a>
        </div>
        <div class="col" id="nje2"></div>
        <div class="col-md-auto">
          <a class="btn btn-primary text-white" id="njnb2" onclick="Step2CheckProducts()">Next</a>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <h5>Step 3 - Overview</h5>
      <div class="form-group row">
        <div class="col-sm-12" id="Step1"></div>
      </div>
      <div class="form-group row">
        <div class="col-sm-12" id="Step2">
          <table class="table table-hover table-sm njs3t2" id="ProductList">
            <thead>
              <th>Products</th>
              <th>Qty</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row justify-content-between align-items-center pb-2">
        <div class="col">
          <a class="btn btn-secondary text-white" id="njbb2" onclick="prevSlide()">Back</a>
        </div>
        <div class="col-md-auto">
          <a class="btn btn-primary text-white" id="njfb1" onclick="addJob()">Finish</a>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="/js/mods.js"></script>
  </div>
</div>
