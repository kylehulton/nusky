<?php require_once('core/system.php');
$cn = htmlspecialchars($_POST['cn']);
$sa = htmlspecialchars($_POST['sa']);
$t = htmlspecialchars($_POST['t']);
$r = htmlspecialchars($_POST['r']);
$pc = htmlspecialchars($_POST['pc']);
$tn = htmlspecialchars($_POST['tn']);
$nc = new Database();
$nc->query('INSERT INTO customers (CustomerName, streetAddress, town, region, postalCode, tel_no) VALUES (:cn, :sa, :t, :r, :pc, :tn)');
$nc->bind(':cn', $cn);
$nc->bind(':sa', $sa);
$nc->bind(':t', $t);
$nc->bind(':r', $r);
$nc->bind(':pc', $pc);
$nc->bind(':tn', $tn);
$nc->execute();
?>
