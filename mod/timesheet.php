<?php

// Project John

/*
 Current Timesheets are a blank table where employee writes in values applicable to ones work.
 For Example;
    Mike gets into work at 7:15 - This isn't noted on current timesheets but this is the start of Mike's Day
    Mike starts working on GE Boards 20x4373G testing them. He assigns a work order number to his time in the first column
    2826 is the Work Order
    Mike will then perhaps do an hour on Optics
    OPTICS isn't a work order but has a costing to it; As does PROD.

        First Hurdle here is that not all work is customer based and can be internal.

    Break times are hard set on the current timesheets as 30 minutes except for Friday where it is 15 minutes. These get scribbled out at times as not every day is perfect and allows for breaks.

        Easy fix, don't hard write in breaks.

    Mike then has to add up his hours and minutes of the day and write them into the bottom of the daytable.

        Not really a problem to leave employees to do this but It can be made to calc on their behalf.

Second Example;
    Kyle is doing Ryder Boards
    Kyle needs to do 4,000 a month for the Ryder H's SDL3055/B
    It's not clear when 4,000 for May is complete and 4,000 for June begins.
    On the rear of the Timesheets there is a list of Orders that wants completeing

        This isn't being updated on a regular basis, apparently it was handled by a man called John;

    When Kyle is filling out his Timesheet, the same way Mike is doing. Kyle doesn't know what his Works order number is.

        Second Hurdle, getting the data into sky of the progress of work so that the Timesheet can be updated regularly or is concurrent to user input.

*/
require_once('core/system.php');
?>
<h4 class="display-5">They say time is the fire in which we burn.</h4>
<p class="lead">It's like a predator; it's stalking you. Oh, you can try and outrun it with doctors, medicines, new technologies. But in the end, time is going to hunt you down... and make the kill.</p>
