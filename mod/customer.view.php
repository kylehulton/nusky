<?php require_once('core/system.php');
if (empty($_GET['ci'])) {
  $ci = FALSE;
} else {
  $ci = $_GET['ci'];
} // Update to PDO
$lc = $db->prepare('SELECT * FROM customers WHERE customer_id=:ci');
$lc->execute(array('ci' => $ci));
while($c = $lc->fetch(PDO::FETCH_ASSOC)){
  $cn = $c['CustomerName'];
  $a = $c['streetAddress'].", ".$c['town'].", ".$c['region'].", ".$c['postalCode'];
  $tn = $c['tel_no'];
} ?>
<div class="row justify-content-between align-items-center pb-2">
  <div class="col">
    <button class="btn btn-primary btn-sm" onclick="CustomersShow()">Back to Customers...</button>
  </div>
  <div class="col-md-auto">
    <button class="btn btn-warning" onclick="CustomerEdit(<?php echo '\''.$ci.'\', \''.$cn.'\''; ?>)">Edit</button>
  </div>
</div>
<div class="row justify-content-between align-items-center">
  <div class="col"><h4><?php echo $cn; ?></h4></div>
  <div class="col-md-auto"><?php echo $a.'<br />'.$tn; ?></div>
</div>
<table class="table-sm table-hover">
  <thead>
    <tr>
      <td><h4>Jobs</h4></td>
    </tr>
  </thead>
  <tbody>
<?php
$lcj = $db->prepare('SELECT * FROM jobs WHERE Customer=:cn');
$lcj->execute(array('cn' => $cn));
$lcj_r = $lcj->rowCount();
if ($lcj_r > 0){
  while($cj = $lcj->fetch(PDO::FETCH_ASSOC)){
    $jn = $cj['Job_NO'];?>
    <tr>
      <td><?php echo $jn; ?></td>
      <td><button class="badge acrylic badge-info btn" onclick="JobsView('<?php echo $jn; ?>')">View</button></td>
    </tr>
<?php }
}else{
  echo 'No Results Found.';
} ?>
  </tbody>
</table>
