<?php require_once("core/system.php");
$aup = new Database();
$query = 'UPDATE login SET ';
$cu = htmlspecialchars($_POST['cu']);
$nu = htmlspecialchars($_POST['nu']);
$fn = htmlspecialchars($_POST['fn']);
$sn = htmlspecialchars($_POST['sn']);
$dn = htmlspecialchars($_POST['dn']);
$dob = htmlspecialchars($_POST['dob']);
$em = htmlspecialchars($_POST['em']);
$ei = htmlspecialchars($_POST['ei']);
if (empty($_POST['cu'])) {
  header('HTTP/1.0 400');
  exit();
}
if (empty($_POST['nu'])) {
  header('HTTP/1.0 400');
  exit();
} else {
  $query .= 'username=:nu';
}
if (empty($_POST['fn'])) {
  header('HTTP/1.0 400');
  exit();
} else {
  $query .= ', firstname=:fn';
}
$query .= ', surname=:sn';
if (empty($_POST['dn'])) {
  header('HTTP/1.0 400');
  exit();
} else {
  $query .= ', displayname=:dn';
}
$query .= ', dob=:dob';
if (empty($dob)) {
  $dob = "0000/00/00";
}
$query .= ', email=:em, employee_id=:ei';
if (!empty($_POST['l'])) {
  $l = $_POST['l'];
  if ($l == "true") {
    $status = "Disabled";
  } else {
    $status = "Active";
  }
  $query .= ', status=:status';
} else {
  header('HTTP/1.0 400');
  exit();
}
if (!empty($_POST['m'])) {
  $m = $_POST['m'];
  if ($m == "true") {
    $mustchange = "1";
  } else {
    $mustchange = "0";
  }
  $query .= ', mustchange=:mustchange';
} else {
  header('HTTP/1.0 400');
  exit();
}
$query .= ' WHERE username=:cu';
$aup->query($query);
$aup->bind(':nu', $nu);
$aup->bind(':fn', $fn);
$aup->bind(':sn', $sn);
$aup->bind(':dn', $dn);
$aup->bind(':dob', $dob);
$aup->bind(':em', $em);
$aup->bind(':ei', $ei);
$aup->bind(':status', $status);
$aup->bind(':mustchange', $mustchange);
$aup->bind(':cu', $cu);
$aup->execute();
?>
