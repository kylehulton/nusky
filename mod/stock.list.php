<?php require_once("core/system.php");
if(empty($_GET['bom'])){ $bom = FALSE; } else { $bom = $_GET['bom']; }
if(empty($_GET['p'])){ $page = 1; } else { $page = $_GET['p']; }
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20; $start=($previous*$limit);
$ls = new database();
$ls->query('SELECT * FROM stock WHERE OVERALL_STOCK_LEVEL > 0 ORDER BY stock_id DESC LIMIT '.$start.', '.$limit);
$ls->execute();
$s = $ls->resultset();
$sr = new database();
$sr->query('SELECT COUNT(*) as TheCount from stock WHERE OVERALL_STOCK_LEVEL > 0');
$sr->execute();
$rows = $sr->resultset();
foreach($rows as $r){
  $TheCount = $r['TheCount'];
}
$lastpage = ceil($TheCount/$limit);
$secondtolast = $lastpage - 1;
$thirdtolast = $lastpage - 2;
$call = "listStock";
PrepPagination();
?>
<h4 class="display-5">Recent Stock Items <small>(In Stock)</small></h4>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Stock Code</th>
      <th>Description</th>
      <th>Stock Level</th>
    </tr>
  </thead>
  <tbody>
<?php
$a = new stock;
$a -> list($s);
print_r('</tbody>
</table>
<div class="row justify-content-center">
  <nav aria-label="Pages for Stock">');
  Pagination();
print_r('</nav>
</div>'); ?>
