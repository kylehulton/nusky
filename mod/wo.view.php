<?php require_once("core/system.php");
if(empty($_GET['wo'])){ $wo = FALSE; } else { $wo = $_GET['wo']; }
print_r('<button class="btn btn-primary btn-sm" onclick="WorksShow()">Back to Works Commencing...</button>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>WO #</th>
      <th>Customer</th>
      <th>Qty</th>
      <th>Product</th>
    </tr>
  </thead>
  <tbody>');
  $GetWorks = new Database();
  $GetWorks->query('SELECT * FROM wo WHERE wo_no=:wo ORDER BY wo_id DESC LIMIT '.$Limit_A.', '.$Limit_B);
  $GetWorks->bind(':wo', $wo);
  $GetWorks->execute();
  $lwo = $GetWorks->fetchAll();
  foreach ($lwo as $c){
    $wo = $c['wo_no'];
    $cn = $c['CustomerName'];
    $qty = $c['qty_o'];
    $fn = $c['FriendlyName'];
    echo '<tr>
    <td>'.$wo.'</td>
    <td>'.$cn.'</td>
    <td>'.$qty.'</td>
    <td>'.$fn.'</td>
    </tr>'; }
print_r("</tbody>
</table>"); ?>
<div class="progress">
  <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
  <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
  <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
</div>
