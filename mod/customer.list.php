<?php require_once('core/system.php');
if (empty($_GET['p'])) {
  $page = 1;
} else {
  $page = $_GET['p'];
}
if (empty($_GET['info'])) {
  $info = FALSE;
} else {
  $info = $_GET['info'];
}
$info = str_split($info, 7);
if ($info[0] == "Created") {
  ?><div class="alert alert-success" role="alert">
    Customer Created!
  </div><?php }
if ($info[0] == "Deleted") { ?>
  <div class="alert alert-warning" role="alert">
    Customer Deleted!
  </div><?php }
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20;
$start=($previous*$limit);
$lc = new database();
$lc->query('SELECT * FROM customers ORDER BY CustomerName LIMIT '.$start.', '.$limit);
$lc->execute();
$c = $lc->resultset();
$cr = new database();
$cr->query('SELECT COUNT(*) as TheCount from Customers');
$cr->execute();
$rows = $cr->resultset();
foreach($rows as $r){
  $TheCount = $r['TheCount'];
}
$lastpage = ceil($TheCount/$limit);
$secondtolast = $lastpage - 1;
$thirdtolast = $lastpage - 2;
$call = "CustomersShow";
PrepPagination();
?>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Customers</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
<?php
foreach($c as $c){
  $ci = $c['customer_id'];
  $cn = $c['CustomerName'];
  echo '<tr ondblclick="CustomerView(\''.$ci.'\', \''.$cn.'\')">
    <td>'.$cn.'</td>
    <td>
      <button id="Actions" type="button" class="badge acrylic badge-neutral btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
        <h6 class="dropdown-header">Actions for '.$cn.'</h6>
        <button class="btn dropdown-item" onclick="CustomerView(\''.$ci.'\', \''.$cn.'\')"><b>View</b></button>
        <button class="btn dropdown-item" onClick="CustomerEdit(\''.$ci.'\', \''.$cn.'\')">Edit</button>
        <button class="btn dropdown-item" onclick="CustomerDelete(\''.$ci.'\', \''.$cn.'\')">Delete</button>
      </div>
    </td>
  </tr>'; }
print_r('</tbody>
</table>
<div class="row justify-content-center">
  <nav aria-label="Pages for Customers">');
  Pagination();
print_r('</nav>
</div>'); ?>
