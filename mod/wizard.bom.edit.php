<?php require_once("core/system.php");
if(empty($_GET['bom'])){ header('HTTP/1.1 405 Looks like it\'s a MikeSQL error!'); exit(); } else { $bom = $_GET['bom']; }
if(empty($_GET['x'])){ $x = FALSE; } else { $x = $_GET['x']; }
$bom = htmlspecialchars($bom);
$db = new database();
$db->query("SELECT * FROM bom WHERE BOM=:bom");
$db->bind(':bom', $bom);
$db->execute();
$b = $db->resultset();
foreach ($b as $b){
  $sn = $b['BOM'];
  $fn = $b['FriendlyName'];
}
?>
<div class="row justify-content-between align-items-center pb-2">
  <div class="col">
    <h4 class="display-5"><span id="CName"><?php echo $fn; ?></span> (<span id="CCode"><?php echo $sn; ?></span>)</h4>
  </div>
  <div class="col-md-auto">
    <button class="btn btn-primary btn-sm" onclick="BOMSave()">Save</button>
  </div>
</div>
<form autocomplete="off">
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Stock Code</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" id="NCode" value="<?php echo $sn; ?>" required >
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" id="NName" value="<?php echo $fn; ?>" required >
    </div>
  </div>
</form>
<div class="alert alert-warning" role="alert">
  <div class="row justify-content-between align-items-center">
    <div class="col">
      <h5><b>*</b> Please Note : Changes to the BoM components are saved immediately <b>*</b></h5>
      <h6>Any changes above this notice must be saved using the save button.</h6>
    </div>
    <div class="col-md-auto">
      <button class="btn btn-primary btn-sm" onclick="BOMSave()">Save</button>
    </div>
  </div>
</div>
<?php
$db->query('SELECT bomi_id, BOM, stock_co, stock_desc, qty, item_no, comp_ref FROM bom_items WHERE BOM=:sn ORDER BY item_no');
$db->bind(':sn', $sn);
$db->execute();
$i = $db->resultset();
$ic = $db->rowCount();
if($x == "Change"){ ?><div class="alert alert-success" role="alert" id="ebe">Changes Saved!</div><?php } else { ?><div id="ebe"></div><?php } ?>
<!-- <div class="row justify-content-between align-items-center pb-2">
  <div class="col-md-auto">
    <button -->
    <?php //echo 'class="btn btn-primary btn-sm'; if($ic < 2){ echo ' disabled"'; } else { echo '" data-toggle="modal" data-target="#Modal" data-action="ReSeq"'; }
    ?>
    <!-- >Re-Sequence</button>
  </div>
</div> -->
<div id="ebi">
  <table class="table table-sm table-hover">
    <thead>
      <tr>
        <th>Item #</th>
        <th>Stock Code</th>
        <th>Description</th>
        <th>Qty</th>
        <th>Component Reference</th>
        <th>Actions</th>
      </tr>
    </thead>
  <tbody>
<?php
foreach($i as $i){
$id = $i['bomi_id'];
$in = $i['item_no'];
$sc = $i['stock_co'];
$sd = $i['stock_desc'];
$qty = $i['qty'];
$cr = $i['comp_ref'];
echo '<tr>
  <td>'.$in.'</td>
  <td>'.$sc.'</td>
  <td>'.$sd.'</td>
  <td>'.$qty.'</td>
  <td>'.$cr.'</td>
  <td><button class="badge badge-warning btn" data-toggle="modal" data-target="#Modal" data-action="ebEditComp" data-bomi="'.$id.'" data-itemno="'.$in.'" data-stockcode="'.$sc.'" data-desc="'.$sd.'" data-qty="'.$qty.'" data-compref="'.$cr.'">Edit</button> ';
  // if ($ic > 1){
  //   echo '<button id="Re-Seq" type="button" class="badge badge-info btn dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Re-Seq</button>
  //   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Re-Seq">';
  //   if ($in > 1){ echo '<button class="dropdown-item" >Move to Top</button>
  //       <button class="dropdown-item" >Move Up</button>'; }
  //   if ($in < $ic){ echo '<button class="dropdown-item" >Move Down</button>
  //       <button class="dropdown-item" >Move to Bottom</button>'; }
  //   echo '</div> '; }
  echo '<button class="badge badge-danger btn" data-toggle="modal" data-target="#Modal" data-action="ebDeleteComp" data-bomi="'.$id.'" data-itemno="'.$in.'" data-stockcode="'.$sc.'" data-desc="'.$sd.'" data-qty="'.$qty.'" data-compref="'.$cr.'">Delete</button></td>
</tr>';
}
?>
    </tbody>
  </table>
</div>
<form autocomplete="off">
  <div class="form-group row">
    <div class="col-sm-3">
      <input id="Stock" name="Stock" class="form-control form-control-sm" placeholder="Stock Code" required >
    </div>
    <div class="col-sm-3">
      <input id="Desc" name="Desc" class="form-control form-control-sm" placeholder="Description" required >
    </div>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm" id="Qty" placeholder="Qty" required >
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control form-control-sm" id="CompRef" placeholder="Comp Ref" required >
    </div>
    <div class="col-sm-2">
      <button type="button" class="btn btn-primary btn-sm" id="AddtoList" item_no="<?php echo $ic; ?>" onclick="ebAddComp()">Add to List</button>
    </div>
  </div>
  <div class="form-group row">
    <div class="col" id="Stock_Autofill"></div>
  </div>
</form>
<script type="text/javascript" src="/js/mods.js"></script>
