<?php require_once("core/system.php");
$u = htmlspecialchars($_POST['u']);
$p = sha1(htmlspecialchars($_POST['p']));
$m = htmlspecialchars($_POST['m']);
$l = htmlspecialchars($_POST['l']);
if ($m == "true") {
  $mustchange = "1";
} else {
  $mustchange = "0";
}
if ($l == "true") {
  $status = "Disabled";
} else {
  $status = "Active";
}
$aupr = new database();
$aupr->query('UPDATE login SET password=:password, mustchange=:mustchange, status=:status WHERE username=:username');
$aupr->bind(':username', $u);
$aupr->bind(':password', $p);
$aupr->bind(':mustchange', $mustchange);
$aupr->bind(':status', $status);
$aupr->execute();
?>
