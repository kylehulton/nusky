<?php require_once('core/system.php');
if(empty($_GET['job'])){ header('HTTP/1.1 405 Looks like it\'s a MikeSQL error!'); exit(); } else { $job = $_GET['job']; }
if(empty($_GET['x'])){ $x = FALSE; } else { $x = $_GET['x']; }
$Job_NO = htmlspecialchars($job);
$db = new database();
$db->query("SELECT * FROM jobs WHERE Job_NO=:jn");
$db->bind(':jn', $Job_NO);
$db->execute();
$j = $db->resultset();
foreach ($j as $j){
    $Job_NO = $j['Job_NO'];
    $Customer = $j['Customer'];
    $Comments = $j['Comments'];
    $Notes = $j['Notes'];
    $Status = $j['status'];
}
$db->query("SELECT * FROM job_items WHERE Job_NO=:jn");
$db->bind(':jn', $Job_NO);
$db->execute();
$ji = $db->resultset();
?><div class="row">
    <div class="col">
        <h4 class="display-5">Editing Job: <span id="job_no"><?php echo $Job_NO; ?></span></h4>
    </div>
    <div class="col-md-auto">
        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#Modal" data-action="SaveEdit">Save</button>
    </div>
</div>
<form autocomplete="off">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-10">
            <select class="form-control form-control-sm" id="Status">
                <option value="Active" <?php if($Status == "Active"){ echo "selected"; } ?> >Active</option>
                <option value="Hold" <?php if($Status == "Hold"){ echo "selected"; } ?> >On Hold</option>
                <option value="Completed" <?php if($Status == "Completed"){ echo "selected"; } ?> >Completed</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Customer</label>
        <div class="col-sm-10">
            <input type="text" class="form-control form-control-sm" name="Customer" id="Customer" value="<?php echo $Customer; ?>" required >
        </div>
        <div class="col-sm-12" id="Customer_Autofill"></div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Comments</label>
        <div class="col-sm-10">
            <input type="text" id="Comments" class="form-control form-control-sm" value="<?php echo $Comments; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Additional Notes</label>
        <div class="col-sm-10">
            <textarea id="Notes" class="form-control form-control-sm"><?php echo $Notes; ?></textarea>
        </div>
    </div>
</form>
<div class="alert alert-warning" role="alert">
    <div class="row justify-content-between align-items-center">
        <div class="col">
            <h5><b>*</b> Please Note : Changes to items are saved immediately <b>*</b></h5>
            <h6>Any changes above this notice must be saved using the save button.</h6>
        </div>
        <div class="col-md-auto">
            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#Modal" data-action="SaveEdit">Save</button>
        </div>
    </div>
</div>
<?php if($x == "Change"){ ?><div class="alert alert-success" role="alert" id="eaie">Changes Saved!</div><?php } else { ?><div id="eaie"></div><?php } ?>
<div id="ejs2"><table class="table table-sm table-hover" id="ejs2a">
    <thead>
        <tr>
            <th>Products</th>
            <th>Qty</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($ji as $ji){
            $jbi = $ji['JBI_ID'];
            $Product = $ji['Product'];
            $Qty = $ji['Qty'];
            print_r('<tr id="'.$jbi.'"><td id="exi1">'.$Product.'</td><td id="exi2">'.$Qty.'</td><td><button class="badge badge-warning btn" data-toggle="modal" data-target="#Modal" data-action="EditItem" data-productname="'.$Product.'" data-quantity="'.$Qty.'" data-jbi="'.$jbi.'" data-job_no="'.$Job_NO.'">Edit</button>
            <button class="badge badge-danger btn" data-toggle="modal" data-target="#Modal" data-action="DeleteItem" data-productname="'.$Product.'" data-quantity="'.$Qty.'" data-jbi="'.$jbi.'" data-job_no="'.$Job_NO.'">Delete</button></td></tr>');
        } ?></tbody>
    </table></div>
<div class="form-group row">
    <div class="col-sm-8">
        <input id="Product" name="Product" class="form-control form-control-sm" placeholder="Product" required >
    </div>
    <div class="col-sm-2">
        <input type="text" class="form-control form-control-sm" id="Qty" placeholder="Qty" required >
    </div>
    <div class="col-sm-2">
        <button type="button" class="btn btn-primary btn-sm" id="AddtoList" onclick="editAdditem()" >Add to List</button>
    </div>
</div>
<div class="form-group row">
    <div class="col" id="Products_Autofill"></div>
</div>
<script type="text/javascript" src="/js/mods.js"></script>
