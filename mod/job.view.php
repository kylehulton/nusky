<?php require_once("core/system.php");
if(empty($_GET['job'])){ header('HTTP/1.1 405 Looks like it\'s a MikeSQL error!'); exit(); } else { $job = $_GET['job']; }
$Job_NO = htmlspecialchars($job);
$db = new database();
$db->query("SELECT * FROM jobs WHERE Job_NO=:jn");
$db->bind(':jn', $Job_NO);
$db->execute();
$j = $db->resultset();
foreach ($j as $jd){
  $Job_NO = $jd['Job_NO'];
  $Customer = $jd['Customer'];
  $Comments = $jd['Comments'];
  $Notes = $jd['Notes'];
  $Status = $jd['status'];
} ?><div class="row justify-content-between align-items-center">
  <div class="col">
    <h4 class="display-5">Viewing Job: <?php echo $Job_NO; ?></h4>
    <h6>Status: <?php echo $Status; ?></h6>
  </div>
  <div class="col-md-auto">
    <button class="btn btn-warning btn-sm" onclick="JobsEdit('<?php echo $Job_NO; ?>')">Edit</button>
  </div>
</div>
<table class="table table-sm table-hover">
  <tbody>
    <tr>
      <th scope="row">Customer</th>
      <td><?php echo $Customer; ?></td>
    </tr>
    <tr>
      <th scope="row">Comments</th>
      <td><?php echo $Comments; ?></td>
    </tr>
    <tr>
      <th scope="row">Additional Notes</th>
      <td><?php echo $Notes; ?></td>
    </tr>
  </tbody>
</table>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Product Code</th>
      <th>Product Name</th>
      <th>Qty</th>
    </tr>
  </thead>
  <tbody><?php
$db->query("SELECT * FROM job_items WHERE Job_NO=:jn");
$db->bind(':jn', $Job_NO);
$db->execute();
$ji = $db->resultset();
foreach ($ji as $ji){
  $Product = $ji['Product'];
  $Qty = $ji['Qty'];
  $db->query("SELECT stock_desc FROM stock where stock_co=:jip");
  $db->bind(':jip', $Product);
  $db->execute();
  $si_r = $db->rowCount();
  $si = $db->resultset();
  foreach ($si as $si){
    $Product_Name = $si['stock_desc'];
  }
  if ($si_r == "0"){
    $Product_Name = "Code not on Stocks";
  }
  echo '<tr>
    <td>'.$Product.'</td>
    <td>'.$Product_Name.'</td>
    <td>'.$Qty.'</td>
  </tr>';
} ?>
  </tbody>
</table>
