<?php require_once('core/system.php');
if (empty($_GET['a'])){
  header('HTTP/1.1 400 No BOM');
  exit();
} else {
  $a = $_GET['a'];
  if (empty($total_cost)) {
    $total_cost = 0;
  }
}
$getBOM = new database();
$getBOM->query('SELECT BOM_ID, BOM FROM bom WHERE BOM=:bom');
$getBOM->bind(':bom', $a);
$getBOM->execute();
$b = $getBOM->fetchAll();
foreach ($b as $b) {
  $ID = $b['BOM_ID'];
  $BOM = $b['BOM'];
}
$getBOMItems = new database();
$getBOMItems->query('SELECT bomi_id, stock_co, qty FROM bom_items where BOM=:bom');
$getBOMItems->bind(':bom', $BOM);
$getBOMItems->execute();
$c = $getBOMItems->fetchAll();
$getStock = new database();
$updateBOMItems = new database();
foreach ($c as $c) {
  $item_id = $c['bomi_id'];
  $scode = $c['stock_co'];
  $qty = $c['qty'];
  $getStock->query('SELECT * FROM stock where stock_co=:scode');
  $getStock->bind(':scode', $scode);
  $getStock->execute();
  $d = $getStock->fetchAll();
  if (!$d) {
    $cost = 0;
  } else {
    foreach ($d as $d) {
      $cost = $d['BUYING_PRICE#1'];
      if (empty($cost)) {
        $cost = 0;
      }
    }
  }
  $sequence_cost = $qty * $cost;
  $updateBOMItems->query('UPDATE bom_items SET Cost=:sequence_cost, Cost_Single=:cost WHERE bomi_id=:item_id');
  $updateBOMItems->bind(':sequence_cost', $sequence_cost);
  $updateBOMItems->bind(':cost', $cost);
  $updateBOMItems->bind(':item_id', $item_id);
  $updateBOMItems->execute();
  $total_cost = $total_cost + $sequence_cost;
}
$updateBOM = new database();
$updateBOM->query('UPDATE bom SET Cost=:total_cost, date_costed=:date_costed, costed_by=:costed_by, costingversion=:costingversion WHERE bom_id=:id');
$updateBOM->bind(':total_cost', $total_cost);
$updateBOM->bind(':date_costed', $date_now);
$updateBOM->bind(':costed_by', $CU_Fullname);
$updateBOM->bind(':id', $ID);
$updateBOM->bind(':costingversion', $costingversion);
$updateBOM->execute();
?>
