<?php
require_once("core/system.php");
$page = 1;
$Limit_A=0; $Limit_B=10;
$Limit_A=($page-1)*$Limit_B;
if(empty($_GET['data'])){ $data = FALSE; } else { $data = rawurldecode($_GET['data']); }
if(empty($_GET['cat'])){ $cat = FALSE; } else { $cat = rawurldecode($_GET['cat']); }
$cat = htmlspecialchars($cat);
$data = strtolower(htmlspecialchars($data));
echo '<ul class="list-unstyled mt-2"><li>AutoComplete: </li>';
if (($cat == "Customers")){
    $gd = $db->prepare("SELECT * FROM customers WHERE LCASE(CustomerName) LIKE concat('%', :SearchParam, '%') LIMIT ".$Limit_A.", ".$Limit_B);
    $gd->execute(array('SearchParam' => $data));
    $r = $gd->rowCount();
    if ($r > 0){
        while($c = $gd->fetch(PDO::FETCH_ASSOC)){
            $id = $c['customer_id'];
            $n = $c['CustomerName'];
            $a = "(".$c['streetAddress'].", ".$c['town'].", ".$c['region'].", ".$c['postalCode'].")";
            echo '<li><a id="c_'.$id.'" class="badge badge-pill">'.$n.' '.$a.'</a>';
            echo "<script>
            $('#c_".$id."').click(function(){
                $('#Customer').val('".$n."');
                $('#Customer_Autofill').hide();
            });
            </script></li>";
        }
    }else{
        echo '<li>No Results Found.</li>';
    }
}elseif(($cat == "Products")){
    $gd = $db->prepare("SELECT * FROM BOM WHERE LCASE(FriendlyName) LIKE concat('%', :SearchParam, '%') OR LCASE(BOM) LIKE concat('%', :SearchParam, '%') ORDER BY BOM_ID DESC LIMIT ".$Limit_A.", ".$Limit_B);
    $gd->execute(array('SearchParam' => $data));
    $r = $gd->rowCount();
    if ($r > 0){
        while($p = $gd->fetch(PDO::FETCH_ASSOC)){
            $n = $p['FriendlyName'];
            $id = $p['BOM_ID'];
            $bc = $p['BOM'];
            echo '<li><a id="p_'.$id.'" class="badge badge-pill">'.$n.' ('.$bc.')</a>';
            echo "<script>
            $('#p_".$id."').click(function(){
                $('#Product').val('".$bc."');
                $('#Products_Autofill').hide();
            });
            </script></li>";
        }
    }else{
        echo '<li>No Results Found.</li>';
    }
}elseif(($cat == "Stock")){
    $gd = $db->prepare("SELECT stock_id, stock_co, stock_desc FROM stock WHERE LCASE(stock_co) LIKE concat('%', :SearchParam, '%') ORDER BY stock_co DESC LIMIT ".$Limit_A.", ".$Limit_B);
    $gd->execute(array('SearchParam' => $data));
    $r = $gd->rowCount();
    if ($r > 0){
        while($s = $gd->fetch(PDO::FETCH_ASSOC)){
            $si = $s['stock_id'];
            $sc = $s['stock_co'];
            $sd = $s['stock_desc'];
            echo '<li><a id="s_'.$si.'" class="badge badge-pill">'.$sd.' ('.$sc.')</a>';
            echo "<script>
            $('#s_".$si."').click(function(){
                $('#Stock').val('".$sc."');
                $('#Desc').val('".$sd."');
                $('#Stock_Autofill').hide();
            });
            </script></li>";
        }
    }else{
        echo '<li>No Results Found.</li>';
    }
}else{
    echo '<li>No Results Found.</li>';
}
echo '</ul>';
?>
