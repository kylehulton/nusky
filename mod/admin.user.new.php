<?php require_once("core/system.php");
$Username = strtolower(strip_tags($_POST["u"]));
$FirstName = ucwords(strip_tags($_POST["f"]));
$Surname = ucwords(strip_tags($_POST["s"]));
$DisplayName = ucwords(strip_tags($_POST["d"]));
$Email = ucwords(strip_tags($_POST["e"]));
$Password = sha1(strip_tags($_POST["p"]));
$NewUser = new database();
$NewUser->query('INSERT INTO login (username, firstname, surname, displayname, email, password) VALUES (:Username, :FirstName, :Surname, :DisplayName, :Email, :Password)');
$NewUser->bind(':Username', $Username);
$NewUser->bind(':FirstName', $FirstName);
$NewUser->bind(':Surname', $Surname);
$NewUser->bind(':DisplayName', $DisplayName);
$NewUser->bind(':Email', $Email);
$NewUser->bind(':Password', $Password);
$NewUser->execute();
?>
