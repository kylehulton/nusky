<?php $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
if(empty($_GET['nu'])){ header('HTTP/1.1 405 Looks like it\'s a MikeSQL error!'); exit(); } else { $job = $_GET['nu']; }
$Job_NO = htmlspecialchars($job);
$db = new database();
$db->query("SELECT * FROM job_items WHERE Job_NO=:nu");
$db->bind(':nu', $Job_NO);
$db->execute();
$ji = $db->resultset();
?>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Products</th>
      <th>Qty</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
      <?php
      foreach ($ji as $ji){
          $jbi = $ji['JBI_ID'];
          $Product = $ji['Product'];
          $Qty = $ji['Qty'];
          print_r('<tr id="'.$jbi.'"><td id="exi1">'.$Product.'</td><td id="exi2">'.$Qty.'</td><td><button class="badge badge-warning btn" data-toggle="modal" data-target="#Modal" data-action="EditItem" data-productname="'.$Product.'" data-quantity="'.$Qty.'" data-jbi="'.$jbi.'" data-job_no="'.$Job_NO.'">Edit</button>
          <button class="badge badge-danger btn" data-toggle="modal" data-target="#Modal" data-action="DeleteItem" data-productname="'.$Product.'" data-quantity="'.$Qty.'" data-jbi="'.$jbi.'" data-job_no="'.$Job_NO.'">Delete</button></td></tr>');
      } ?>
  </tbody>
</table>
