<?php require_once('core/system.php');
if (empty($_GET['ci'])) {
  header('HTTP/1.1 405 Looks like it\'s a MikeSQL error!');
  exit();
} else {
  $cust = $_GET['ci'];
  $cust = htmlspecialchars($cust);
}
$ec = new database();
$ec->query("SELECT * FROM customers WHERE customer_id=:cust");
$ec->bind(':cust', $cust);
$ec->execute();
$c = $ec->resultset();
foreach ($c as $c){
  $cn = $c['CustomerName'];
  $sa = $c['streetAddress'];
  $t = $c['town'];
  $r = $c['region'];
  $pc = $c['postalCode'];
  $tn = $c['tel_no'];
} ?>
<form autocomplete="off">
  <div class="form-group row">
    <label class="col-sm-2 col-form-label" for="CustomerName">Customer Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" id="CustomerName" placeholder="Required" value="<?php echo $cn; ?>" required >
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label" for="StreetAddress">Street Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" id="StreetAddress" placeholder="Required; See Note Below" value="<?php echo $sa; ?>" required>
    </div>
    <div class="col">
      <small class="form-text text-muted">This is normally a <b>Number</b> or the <b>Name</b> of the building and the <b>first line of address</b>, i.e. "<b>Spimin House, Beacon Road</b>" or <b>10 Downing Street</b>.</small>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label" for="Town">Town</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" id="Town" placeholder="Required; i.e. Poulton-Le-Fylde" value="<?php echo $t; ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label" for="Region">Region</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" id="Region" placeholder="Required; i.e. Lancashire" value="<?php echo $r; ?>"required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label" for="PostalCode">Postal Code</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" id="PostalCode" placeholder="Required" value="<?php echo $pc; ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label" for="TelNo">Telephone Number</label>
    <div class="col-sm-10">
      <input type="tel" class="form-control form-control-sm" name="TelNo" id="TelNo" placeholder="Required" value="<?php echo $tn; ?>" required>
    </div>
  </div>
</form>
<div class="row justify-content-between align-items-center pb-2">
  <div class="col" id="CustomerECE1"></div>
  <div class="col-md-auto">
    <button class="btn btn-primary" onclick="CustomerUpdate('<?php echo $cust; ?>')">Save</button>
  </div>
</div>
