<?php require_once("core/system.php");
if (empty($_POST['a'])) {
  header('HTTP/1.1 400 No BOM');
  exit();
} else {
  $a = $_POST['a'];
}
if (empty($_POST['b'])) {
  $b = 1;
} else {
  $b = $_POST['b'];
}
$getBOM = new database();
$getBOM->query('SELECT * FROM bom WHERE BOM=:bom');
$getBOM->bind(':bom', $a);
$getBOM->execute();
$c = $getBOM->fetchAll();
foreach ($c as $c) {
  $ID = $c['BOM_ID'];
  $BOM = $c['BOM'];
  $BOM_Desc = $c['FriendlyName'];
  $BOM_Cost = number_format((float)$c['Cost'], 2, '.', '');
  $BOM_Total_Cost = $BOM_Cost * $b;
  $BOM_Total_Cost = number_format((float)$BOM_Total_Cost, 2, '.', '');
  $BOM_CustRef = $c['cust_ref'];
  if (empty($BOM_CustRef)) { $BOM_CustRef = "N/A"; }
  $BOM_Date_Entered = date('d-m-Y', strtotime($c['date_entered']));
  if (empty($BOM_Date_Entered) or ($BOM_Date_Entered == "01-01-1970")) { $BOM_Date_Entered = "N/A"; }
  $BOM_Entered_By = $c['entered_by'];
  if (empty($BOM_Entered_By)) { $BOM_Entered_By = "N/A"; }
  $BOM_Date_Modified = date('d-m-Y', strtotime($c['date_modified']));
  if (empty($BOM_Date_Modified) or ($BOM_Date_Modified == "01-01-1970")) { $BOM_Date_Modified = "N/A"; }
  $BOM_Modified_By = $c['modified_by'];
  if (empty($BOM_Modified_By)) { $BOM_Modified_By = "N/A"; }
  $BOM_Date_Costed = date('d-m-Y', strtotime($c['date_costed']));
  if (empty($BOM_Date_Costed) or ($BOM_Date_Costed == "01-01-1970")) { $BOM_Date_Costed = "N/A"; }
  $BOM_Costed_By = $c['costed_by'];
  if (empty($BOM_Costed_By)) { $BOM_Costed_By = "N/A"; }
}
$getBOMItems = new database();
$getBOMItems->query('SELECT * FROM bom_items where BOM=:bom ORDER BY item_no ASC');
$getBOMItems->bind(':bom', $BOM);
$getBOMItems->execute();
$d = $getBOMItems->fetchAll();
print_r('<div class="row">
  <div class="col">
    <h3>Bill of Materials Costing <small class="text-muted">'.$BOM.' -  '.$BOM_Desc.'</small></h3>
  </div>
  <div class="col-md-auto">
    <button class="btn acrylic btn-warning" onclick="BOMBatchCost(\''.$BOM.'\')">Batch Cost</button>
  </div>
</div>
<div class="row">
  <div class="col">
    <h4>Customer Ref <small>'.$BOM_CustRef.'</small></h4>
    <h4>Date Entered <small>'.$BOM_Date_Entered.' <sup> by '.$BOM_Entered_By.'</sup></small></h4>
    <h4>Last Modified <small>'.$BOM_Date_Modified.' <sup> by '.$BOM_Modified_By.'</sup></small></h4>
    <h4>Last Costed <small>'.$BOM_Date_Costed.' <sup> by '.$BOM_Costed_By.'</sup></small></h4>
  </div>
  <div class="col-md-auto">
    <h4>Parts <small>&pound;'.$BOM_Cost.'</small></h4>
    <h4>Labour <small><sub>&pound;0.36 p/min</sub> <sup class="text-danger">Not Active</sup></small></h4>
    <h4>Per <small>'.$b.'</small></h4>
    <h4>Total <small>&pound;'.$BOM_Total_Cost.' <sup class="text-danger">(Not inc. Labour)</sup></small></h4>
  </div>
</div>
<hr />
<div class="row">
  <div class="col">
    <h4>Parts Breakdown</h4>
  </div>
</div>
<table class="table table-sm">
  <thead>
    <tr>
      <th>Stock Code</th>
      <th>Description</th>
      <th>Qty</th>
      <th>Cost Single</th>
      <th>Cost Per</th>
    </tr>
  </thead>
  <tbody>');
foreach ($d as $d) {
  $Item_ID = $d['bomi_id'];
  $Item_Name = $d['stock_co'];
  $Item_Desc = $d['stock_desc'];
  $Item_Qty = $d['qty'];
  $Item_No = $d['item_no'];
  $Item_CR = $d['comp_ref'];
  $Item_Cost = number_format_drop_zero_decimals($d['Cost'], 4);
  $Item_Cost_Single = number_format_drop_zero_decimals($d['Cost_Single'], 4);
  print_r('<tr>
    <td>'.$Item_Name.'</td>
    <td>'.$Item_Desc.'</td>
    <td>'.$Item_Qty.'</td>
    <td>&pound;'.$Item_Cost_Single.'</td>
    <td>&pound;'.$Item_Cost.'</td>
  </tr>');
}
print_r('</tbody>
</table>
<hr />
<div class="row">
  <div class="col"></div>
  <div class="col-md-auto text-md-right">
    <h4>Total Per <small>&pound;'.$BOM_Cost.'</small></h4>
    <h4>Qty <small>'.$b.'</small></h4>
    <h4>Total <small>&pound;'.$BOM_Total_Cost.' <sup class="text-danger">(Not inc. Labour)</sup></small></h4>
  </div>
</div>'); ?>
