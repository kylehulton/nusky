<?php
// Order Status	Desc
// 0	Incomplete Order
// 1	Print Separate Order Copy
// 2	Print Order
// 4	Waiting Delivery
// 6	Waiting Invoice
// 8	Completed Order
// 10	Cancelled Order
// 12	On Hold Order
// 20	Incomplete Return
// 22	Print Return
// 24	Waiting Return
// 26	Waiting Credit Note
// 28	Completed Return
// 30	Cancelled Return
// 32	On Hold Return
// Order Type	Desc
// 1	Order
// 0	Ghost
// -1	Return
require_once("core/system.php");
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20;
$start=($previous*$limit);
$lpo = new database();
$lpo->query('SELECT * FROM po ORDER BY po_id desc LIMIT '.$start.', '.$limit);
$lpo->execute();
$po = $lpo->resultset();
$lpo->query('SELECT COUNT(*) as TheCount from po');
$lpo->execute();
$rows = $lpo->resultset();
foreach($rows as $r){
  $TheCount = $r['TheCount'];
}
$lastpage = ceil($TheCount/$limit);
$secondtolast = $lastpage - 1;
$thirdtolast = $lastpage - 2;
$call = "PurchaseOrdersShow";
PrepPagination();
?>
<div class="row" id="PuchaseOrdersNavBar">
  <div class="col">
    <nav class="navbar navbar-expand-lg navbar-light manNav row">
      <ul class="navbar-nav">
        <li class="nav-item" id="PuchaseOrdersLink">
          <a class="nav-link" id="PuchaseOrdersLink" onclick="PurchaseOrdersShow()">All</a>
        </li>
        <li class="nav-item" id="PuchaseOrdersLinkWaitingDelivery">
          <a class="nav-link" id="PuchaseOrdersLinkWaitingDelivery" onclick="PurchaseOrdersShow()">Waiting Delivery</a>
        </li>
        <li class="nav-item" id="PuchaseOrdersLinkWaitingInvoice">
          <a class="nav-link" id="PuchaseOrdersLinkWaitingInvoice" onclick="PurchaseOrdersShow()">Waiting Invoice</a>
        </li>
        <li class="nav-item" id="PuchaseOrdersLinkOnHold">
          <a class="nav-link" id="PuchaseOrdersLinkOnHold" onclick="PurchaseOrdersShow()">On Hold</a>
        </li>
        <li class="nav-item" id="PuchaseOrdersLinkCompleted">
          <a class="nav-link" id="PuchaseOrdersLinkCompleted" onclick="PurchaseOrdersShow()">Completed</a>
        </li>
        <li class="nav-item" id="PuchaseOrdersLinkCancelled">
          <a class="nav-link" id="PuchaseOrdersLinkCancelled" onclick="PurchaseOrdersShow()">Cancelled</a>
        </li>
      </ul>
    </nav>
  </div>
</div>
<h4 class="display-5 pt-2">Recently created Purchase Orders</h4>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>PO #</th>
      <th>Supplier</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($po as $po) {
  $id = $po['po_id'];
  $on = $po['ORDER_NUMBER'];
  $an = $po['ACCOUNT_NAME'];
  $status = $po['ORDER_STATUS'];
  echo '    <tr ondblclick="PurchaseOrdersView(\''.$id.'\')">
      <td>'.$on.'</td>
      <td>'.$an.'</td>
      <td>
        <button id="Actions" type="button" class="badge badge-primary btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
          <h6 class="dropdown-header">Actions for '.$on.'</h6>
          <button class="btn dropdown-item" onclick="PurchaseOrdersView(\''.$id.'\')"><b>View</b></button>
          <button class="btn dropdown-item" onclick="PurchaseOrdersVardump(\''.$id.'\')">Vardump</button>
        </div>
      </td>
    </tr>'; }
    print_r(' </tbody>
    </table>
    <div class="row justify-content-center">
      <nav aria-label="Pages for Purchase Orders">');
      Pagination();
    print_r('</nav>
    </div>'); ?>
