<?php require_once('core/system.php');
if(empty($_GET['u'])){
  header('HTTP/1.1 400 No User');
  exit();
} else {
  $u = $_GET['u'];
}
$GetU = new Database();
$GetU->query('SELECT * FROM login WHERE username=:username');
$GetU->bind(':username', $u);
$GetU->execute();
$GotU = $GetU->fetchAll();
foreach ($GotU as $w) {
  $firstname = $w['firstname'];
  $surname = $w['surname'];
  $displayname = $w['displayname'];
  $email = $w['email'];
  $mustchange = $w['mustchange'];
  $status = $w['status'];
  $employee_id = $w['employee_id'];
  $dob = $w['dob'];
}
if ($mustchange == "1") {
  $mustchange = "checked";
} else {
  $mustchange = "";
}
if ($status == "Disabled") {
  $status = "checked";
} else {
  $status = "";
}
?>
<form autocomplete="off">
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmUPuser">Username</label>
    <div class="col-sm-8">
      <input type="text" id="AdmUPuser" name="AdmUPuser" class="form-control form-control-sm" value="<?php echo $u; ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmUPfn">First name</label>
    <div class="col-sm-8">
      <input type="text" id="AdmUPfn" name="AdmUPfn" class="form-control form-control-sm" value="<?php echo $firstname; ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmUPsn">Surname</label>
    <div class="col-sm-8">
      <input type="text" id="AdmUPsn" name="AdmUPsn" class="form-control form-control-sm" value="<?php echo $surname; ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmUPdn">Display Name</label>
    <div class="col-sm-8">
      <input type="text" id="AdmUPdn" name="AdmUPdn" class="form-control form-control-sm" value="<?php echo $displayname; ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmUPem">E-Mail</label>
    <div class="col-sm-8">
      <input type="text" id="AdmUPem" name="AdmUPem" class="form-control form-control-sm" value="<?php echo $email; ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmUPdob">Date of Birth</label>
    <div class="col-sm-8">
      <input type="date" id="AdmUPdob" name="AdmUPdob" class="form-control form-control-sm" value="<?php echo $dob; ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmUPei">Employee ID</label>
    <div class="col-sm-8">
      <input type="text" id="AdmUPei" name="AdmUPei" class="form-control form-control-sm" value="<?php echo $employee_id; ?>">
    </div>
  </div>
  <div class="form-group row">
    <div class="col"></div>
    <div class="col-md-auto">
      <div class="form-check form-check-inline">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="AdmUPs" <?php echo $status; ?>>
          <label class="custom-control-label" for="AdmUPs">Account Disabled</label>
        </div>
      </div>
      <div class="form-check form-check-inline">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="AdmUPmc" <?php echo $mustchange; ?>>
          <label class="custom-control-label" for="AdmUPmc">User must change password on next logon</label>
        </div>
      </div>
    </div>
  </div>
</form>
<div class="form-group row">
  <div class="col" id="AdmUPstatus"></div>
  <div class="col-md-auto">
    <button class="btn btn-primary btn" onclick="AdmUserUpdate()">OK</button>
    <button class="btn btn-outline-secondary btn" onclick="AdmUserManShow()">Cancel</button>
  </div>
</div>
