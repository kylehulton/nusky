<?php require_once("core/system.php");
if (empty($_GET['a'])){
  header('HTTP/1.1 400 No Purchase Order');
  exit();
} else {
  $a = $_GET['a'];
}
$vpo = new database();
$vpo->query('SELECT * FROM po WHERE po_id=:id');
$vpo->bind(':id', $a);
$vpo->execute();
$po = $vpo->fetchAll();
echo '<table class="table table-sm"><tbody>';
foreach ($po as $po) {
  $id = $po['po_id'];
  $on = $po['ORDER_NUMBER'];
  $vpo_items = new database();
  $vpo_items->query('SELECT * FROM po_items WHERE po_id=:id');
  $vpo_items->bind(':id', $id);
  $vpo_items->execute();
  $poi = $vpo_items->fetchAll();
  echo '<tr><th scope="row" colspan="2" class="table-dark">Purchase Order <span id="PurchaseOrderNumber">'.$on.'</span></th></tr>';
  foreach ($po as $columnName=>$data) {
    echo '<tr><th scope="row">'.$columnName.'</th><td>'.$data.'</td></tr>';
  }
  foreach ($poi as $columnName=>$data) {
    $itemno = $columnName+1;
    echo '<tr><th scope="row" colspan="2" class="table-dark">Item '.$itemno.'</th></tr>';
    foreach($data as $columnName=>$data){
      echo '<tr><th scope="row">'.$columnName.'</th><td>'.$data.'</td></tr>';
    }
  }
}
echo "</tbody></table>";
?>
