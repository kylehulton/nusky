<?php require_once("core/system.php");
if(empty($_GET['bom'])){ header('HTTP/1.1 405 Looks like it\'s a MikeSQL error!'); exit(); } else { $bom = $_GET['bom']; }
$bom = htmlspecialchars($bom);
$db = new database();
$db->query("SELECT * FROM bom WHERE BOM=:bom");
$db->bind(':bom', $bom);
$db->execute();
$b = $db->resultset();
foreach ($b as $b) {
  $sn = $b['BOM'];
  $fn = $b['FriendlyName'];
  $cost = $b['Cost'];
  $dwg = $b['DWG_NO'];
}
print_r('
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th colspan="4">
        <div class="row justify-content-between align-items-center">
          <div class="col">
            <h4 class="display-5">BoM for '.$fn.' ('.$sn.')</h4>
            <h6>'.$dwg.'</h6>
          </div>
          <div class="col-md-auto">
            <button class="btn btn-warning btn-sm" onclick="BOMEdit(\''.$bom.'\')">Edit</button>
          </div>
        </div>
      </th>
    </tr>
    <tr>
      <th>Stock Code</th>
      <th>Description</th>
      <th>Qty</th>
      <th>Component Reference</th>
    </tr>
  </thead>
<tbody>');
$db->query('SELECT bomi_id, BOM, stock_co, stock_desc, qty, item_no, comp_ref FROM bom_items WHERE BOM=:sn ORDER BY item_no');
$db->bind(':sn', $sn);
$db->execute();
$i = $db->resultset();
foreach($i as $i){
  $id = $i['bomi_id'];
  $in = $i['item_no'];
  $sc = $i['stock_co'];
  $sd = $i['stock_desc'];
  $qty = $i['qty'];
  $cr = $i['comp_ref'];
  echo '<tr>
    <td>'.$sc.'</td>
    <td>'.$sd.'</td>
    <td>'.$qty.'</td>
    <td>'.$cr.'</td>
  </tr>'; }
print_r("</tbody>
</table>"); ?>
