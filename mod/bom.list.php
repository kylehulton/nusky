<?php require_once("core/system.php");
if(empty($_GET['p'])){ $page = 1; } else { $page = $_GET['p']; }
if(empty($_GET['info'])){ $info = FALSE; } else { $info = $_GET['info']; }
if(empty($_GET['bom'])){ $bom = FALSE; } else { $bom = $_GET['bom']; }
if($info == "Created"){ ?><div class="alert alert-success" role="alert">BoM for <?php echo $bom; ?> Created!</div> <?php }
if($info == "Deleted"){ ?><div class="alert alert-warning" role="alert">BoM for <?php echo $bom; ?> Deleted!</div> <?php }
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20; $start=($previous*$limit);
$lb = new database();
$lb->query('SELECT * FROM bom ORDER BY BOM_ID DESC LIMIT '.$start.', '.$limit);
$lb->execute();
$b = $lb->resultset();
$cb = new database();
$cb->query('SELECT COUNT(*) as TheCount from bom');
$cb->execute();
$rows = $cb->resultset();
foreach($rows as $r){
  $TheCount = $r['TheCount'];
}
$lastpage = ceil($TheCount/$limit);
$secondtolast = $lastpage - 1;
$thirdtolast = $lastpage - 2;
$call = "BOMShow";
PrepPagination();
?>
<h4 class="display-5">List of BoMs</h4>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>BoM #</th>
      <th>Product</th>
      <th>Actions</th>
    </tr>
  </thead>
<tbody>
<?php
  $a = new bom();
  $a -> list($b);
  print_r(' </tbody>
  </table>
  <div class="row justify-content-center">
    <nav aria-label="Pages for BoMs">');
    Pagination();
  print_r('</nav>
  </div>'); ?>
