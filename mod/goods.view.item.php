<?php $page = 1; $Limit_A=0; $Limit_B=25; $Limit_A=($page-1)*$Limit_B; $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
if(empty($_GET['gdii'])){ $gdii = FALSE; } else { $gdii = $_GET['gdii']; }
print_r('<table class="table table-sm table-hover">
    <thead>
        <tr>
            <th>Stock Code</th>
            <th>Description</th>
            <th>Stock Level</th>
        </tr>
    </thead>
    <tbody>');
$lgi = $db->prepare('SELECT * FROM goods_in_items WHERE gdii_id=:gdii');
$lgi->execute(array('gdii' => $gdii));
while($c = $lgi->fetch(PDO::FETCH_ASSOC)){
    $sc = $c['stock_co'];
    $sd = $c['stock_desc'];
    $qty = $c['qty_now'];
    $pert = $c['qty_now']/$c['qty_orig'];
    echo '<tr>
        <td>'.$sc.'</td>
        <td>'.$sd.'</td>
        <td>Only '.$qty.' Left! <meter value="'.$pert.'"></meter></td>
    </tr>'; }
print_r("</tbody>
</table>"); ?>