<?php require_once("core/system.php");
$oln = htmlspecialchars($_POST['on']);
$cn = htmlspecialchars($_POST['cn']);
$sa = htmlspecialchars($_POST['sa']);
$to = htmlspecialchars($_POST['to']);
$re = htmlspecialchars($_POST['re']);
$pc = htmlspecialchars($_POST['pc']);
$tn = htmlspecialchars($_POST['tn']);
$uc = new database();
$uc->query('UPDATE customers SET CustomerName=:cn, streetAddress=:sa, town=:to, region=:re, postalCode=:pc, tel_no=:tn WHERE CustomerName=:oln');
$uc->bind(':oln', $oln);
$uc->bind(':cn', $cn);
$uc->bind(':sa', $sa);
$uc->bind(':to', $to);
$uc->bind(':re', $re);
$uc->bind(':pc', $pc);
$uc->bind(':tn', $tn);
$uc->execute();
?>
