<?php require_once("core/system.php");
if (empty($_GET['a'])){
  header('HTTP/1.1 400 No Purchase Order');
  exit();
} else {
  $a = $_GET['a'];
}
$vpo = new database();
$vpo->query('SELECT * FROM po WHERE po_id=:id');
$vpo->bind(':id', $a);
$vpo->execute();
$po = $vpo->fetchAll();
$gsi = new database();
foreach ($po as $po) {
  $id = $po['po_id'];
  $on = $po['ORDER_NUMBER'];
  $ano = $po['ACCOUNT_NUMBER'];
  $ann = $po['ACCOUNT_NAME'];
  $asn = $po['SHORT_NAME'];
  $aon = $po['THEIR_REFERENCE'];
  $od = $po['ORDER_DATE'];
  $os = $po['ORDER_STATUS'];
  switch($os) {
    case '0':
      $status = "Incomplete Order";
    break;
    case '1':
      $status = "Print Seperate Order Copy";
    break;
    case '2':
      $status = "Print Order";
    break;
    case '4':
      $status = "Waiting Delivery";
    break;
    case '6':
      $status = "Waiting Invoice";
    break;
    case '8':
      $status = "Completed Order";
    break;
    case '10':
      $status = "Cancelled Order";
    break;
    case '12':
      $status = "On Hold Order";
    break;
    case '20':
      $status = "Incomplete Return";
    break;
    case '22':
      $status = "Print Return";
    break;
    case '24':
      $status = "Waiting Return";
    break;
    case '26':
      $status = "Waiting Credit Note";
    break;
    case '28':
      $status = "Completed Return";
    break;
    case '30':
      $status = "Cancelled Return";
    break;
    case '32':
      $status = "On Hold Return";
    break;
  }
  $ot = $po['ORDER_TYPE'];
  switch($ot) {
    case '-1':
      $type = "Return";
    break;
    case '0':
      $type = "Ghost";
    break;
    case '1':
      $type = "Order";
    break;
  }
  $ov = $po['ORDER_VALUE'];
  $vpo_items = new database();
  $vpo_items->query('SELECT * FROM po_items WHERE po_id=:id');
  $vpo_items->bind(':id', $id);
  $vpo_items->execute();
  $poi = $vpo_items->fetchAll();
  echo '<div class="row justify-content-between align-items-center pb-2">
    <div class="col">
      <button class="btn btn-primary btn-sm" onclick="PurchaseOrdersShow()">Back to Purchase Orders...</button>
      <button class="btn btn-warning btn-sm" onclick="PurchaseOrdersEdit(\''.$id.'\')">Edit</button>
    </div>
    <div class="col-md-auto">
      <h5><b>Order Status </b>'.$status.'</h5>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <h5><b>Our Ref. </b><span id="PurchaseOrderNumber">'.$on.'</span></h5>
    </div>
    <div class="col-md-auto">
      <h5><b>Purchase Account </b><span id="TheirAccountNumber">'.$ano.'</span> <span id="TheirAccountName">'.$ann.'</span></h5>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <h5><b>Their Ref. </b><span id="TheirReferenceNumber">'.$aon.'</span></h5>
    </div>
    <div class="col">
      <h5><b>Order Date </b><span id="OrderDate">'.$od.'</span></h5>
    </div>
    <div class="col-md-auto">
      <h5><b>Order Value </b><span id="OrderValue">&pound;'.$ov.'</span></h5>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab">
      <div class="row">
        <div class="col">Item Number</div>
        <div class="col">Price</div>
        <div class="col">Quantity</div>
        <div class="col">Item Total</div>
        <div class="col-md-auto">Due Date</div>
      </div>
    </div>
  </div>
  <div id="accordion" role="tablist">';
  foreach ($poi as $poi) {
    $i_id = $poi['po_item_id'];
    $i_n = $poi['ITEM_NUMBER'];
    $i_p = $poi['INVOICE_PRICE'];
    $i_o = $poi['QUANTITY_ORDERED'];
    $i_st = $i_p * $i_o;
    $i_st = number_format((float)$i_st, 2, '.', '');
    $i_d = $poi['ITEM_DUE_DATE'];
    $s_id = $poi['ITEM_HEADER_REC'];
    $i_ld = $poi['DATE_LAST_DELIVERY'];
    echo '<div class="card">
    <a data-toggle="collapse" href="#'.$i_id.'" aria-expanded="true" aria-controls="'.$i_id.'">
      <div class="card-header" role="tab" id="'.$i_n.'">
        <div class="row">
          <div class="col">'.$i_n.'</div>
          <div class="col">&pound;'.$i_p.'</div>
          <div class="col">'.$i_o.'</div>
          <div class="col">&pound'.$i_st.'</div>
          <div class="col-md-auto">'.$i_d.'</div>
        </div>
      </div>
    </a>
    <div id="'.$i_id.'" class="collapse" role="tabpanel" aria-labelledby="'.$i_n.'" data-parent="#accordion">
      <div class="card-body">';
    $gsi->query('SELECT * FROM stock where stock_id=:s_id');
    $gsi->bind(':s_id', $s_id);
    $gsi->execute();
    $si_r = $gsi->rowCount();
    $si = $gsi->fetchAll();
    foreach ($si as $si) {
      $s_c = $si['stock_co'];
      $s_d = $si['stock_desc'];
      $s_ol = $si['DATE_LAST_ORDER'];
      $s_sl = $si['OVERALL_STOCK_LEVEL'];
      $s_os = $si['QTY_ON_ORDER'];
      $s_psl = $s_sl + $s_os;
      echo '<div class="row">
        <div class="col lead"><b>Stock Item</b> <a href="#" onclick="viewStock(\''.$s_id.'\')">'.$s_c.'</a></div>
        <div class="col-md-auto lead"><b>Description</b> '.$s_d.'</div>
      </div>
      <div class="row">
        <div class="col lead"><b>Stores Location</b> $sto $stores</div>
      </div>
      <div class="row">
        <div class="col lead"><b>Last Ordered</b> '.$s_ol.'</div>
        <div class="col-md-auto lead"><b>Last Delivered</b> '.$i_ld.'</div>
      </div>
      <div class="row">
        <div class="col lead"><b>In Stock</b> '.$s_sl.'</div>
        <div class="col-md-auto lead"><b>Potential Stock</b> '.$s_psl.'</div>
      </div>';
    }
    if ($si_r == "0") {
        echo '<span class="text-muted">Cannot get info, Not on Stock</span>';
    }
    echo '</div>
    </div>
  </div>';
  }
  echo '</div>';
}
?>
