<?php $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
if(empty($_POST['c'])){ $c = FALSE; } else { $c = $_POST['c']; }
if ($c == "Resistor"){
  $lstype = new database();
  $lstype->query('SELECT comp_type, COUNT(*) AS TheCount from stock where category=:cat Group By comp_type');
  $lstype->bind(":cat", $c);
  $lstype->execute();
  $comp_type = $lstype->resultset();
  ?>
  <form autocomplete="off">
    Component Types
    <select size="5" class="form-control form-control-sm" id="ComponentType">
      <?php foreach ($comp_type as $comp_type) {
        $name = $comp_type['comp_type'];
        $TheSecondCount = $comp_type['TheSecondCount'];
        if ($name == NULL ){ $name = "Not Specified"; }
        echo '<option value="'.$name.'">'.$name.' ('.$TheSecondCount.')</option>';
      }
      ?>
    </select>
  </form>
  <?php
}
