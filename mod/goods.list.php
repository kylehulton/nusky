<?php $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php"); require_once("$core_dir/pagination.php");
if(empty($_GET['p'])){ $page = 1; } else { $page = $_GET['p']; }
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20; $start=($previous*$limit);
$lg = new database();
$lg->query('SELECT * FROM goods_in ORDER BY gdi_id DESC LIMIT '.$start.', '.$limit);
$lg->execute();
$g = $lg->resultset();
$gr = new database();
$gr->query('SELECT COUNT(*) as TheCount from goods_in');
$gr->execute();
$rows = $gr->resultset();
foreach($rows as $r){
  $TheCount = $r['TheCount'];
}
$lastpage = ceil($TheCount/$limit);
$secondtolast = $lastpage - 1;
$thirdtolast = $lastpage - 2;
$call = "listGoods";
PrepPagination();
?>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>GRN</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
<?php
foreach($g as $g){
  $id = $g['gdi_id'];
  echo '<tr>
    <td>'.$id.'</td>
    <td><button class="badge badge-primary btn" onclick="ViewGoods(\''.$id.'\')">View</button></td>
  </tr>';
}
print_r('</tbody>
</table>
<div class="row justify-content-center">
  <nav aria-label="Pages for Goods Recieved">');
  Pagination();
print_r('</nav>
</div>'); ?>
