<?php require_once('core/system.php');
$u = htmlspecialchars($_POST['u']);
$status = 'Active';
$aueu = new database();
$aueu->query('UPDATE login SET status=:status WHERE username=:username');
$aueu->bind(':username', $u);
$aueu->bind(':status', $status);
$aueu->execute();
?>
