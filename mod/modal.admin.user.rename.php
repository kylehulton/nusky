<?php require_once('core/system.php');
if(empty($_GET['u'])){
  header('HTTP/1.1 400 No User');
  exit();
} else {
  $u = $_GET['u'];
}
$GetU = new Database();
$GetU->query('SELECT * FROM login WHERE username=:username');
$GetU->bind(':username', $u);
$GetU->execute();
$GotU = $GetU->fetchAll();
foreach ($GotU as $w) {
  $fn = $w['firstname'];
  $sn = $w['surname'];
  $dn = $w['displayname'];
}
?>
<form autocomplete="off">
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmRu">Username</label>
    <div class="col-sm-8">
      <input type="text" id="AdmRu" name="AdmRu" class="form-control form-control-sm" value="<?php echo $u; ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmRf">First Name</label>
    <div class="col-sm-8">
      <input type="text" id="AdmRf" name="AdmRf" class="form-control form-control-sm" value="<?php echo $fn; ?>"required>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmRs">Surname</label>
    <div class="col-sm-8">
      <input type="text" id="AdmRs" name="AdmRs" class="form-control form-control-sm" value="<?php echo $sn; ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label" for="AdmRd">Display Name</label>
    <div class="col-sm-8">
      <input type="text" id="AdmRd" name="AdmRd" class="form-control form-control-sm" value="<?php echo $dn; ?>" required>
    </div>
  </div>
</form>
