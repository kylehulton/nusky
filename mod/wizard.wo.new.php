<?php require_once("core/system.php");
if(empty($_GET['cat'])){ $cat = FALSE; } else { $cat = $_GET['cat']; }
function GetJobs(){
  $gj = new database();
  $gj->query('SELECT Job_NO FROM jobs WHERE status="Active" ORDER BY Job_NO DESC');
  $gj->execute();
  $j = $gj->resultset();
  echo "<option selected>Select Job...</option>";
  foreach ($j as $row){
    $Job_NO = $row['Job_NO'];
    echo "<option value=".$Job_NO.">".$Job_NO."</option>";
  }
}
if($cat == "Job"){
?>
<div id="innerWorksfromJobCreator" class="carousel slide" data-interval="false" data-wrap="false">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <form class="form-inline">
        <label class="mr-sm-2" for="inlineFormCustomSelect">From Job Number</label>
        <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
          <?php GetJobs(); ?>
        </select>
        <button type="submit" class="btn btn-info">AutoFill...</button>
      </form>
      <div class="row justify-content-between align-items-center pb-2">
        <div class="col" id="nwfje1"></div>
        <div class="col-md-auto">
          <a class="btn btn-primary text-white" id="nwfjnb1" onClick="nextSlide()">Next</a>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="row justify-content-between align-items-center pb-2">
        <div class="col" id="nwfje2">select products from job
          <button type="submit" class="btn btn-info">AutoFill...</button>
        </div>
        <div class="col-md-auto">
          <a class="btn btn-primary text-white" id="nwfjnb3" onClick="nextSlide()">Next</a>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="row justify-content-between align-items-center pb-2">
        <div class="col" id="nwfje3">Overview</div>
        <div class="col-md-auto">
          <a class="btn btn-primary text-white" id="nwfjfb" onClick="listWorks()">Finish</a>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
} elseif($cat == "Int") {
?>
<h4 class="display-5">New Internal Works Order</h4>
<?php
} else {
  exit("MikeSQL Error! (With an E)");
} ?>
<hr />
<p>Concept Form</p>
<form class="form-inline">
  <label for="jn">For Job</label>
  <input type="text" class="form-control-plaintext form-control-sm" id="jn">
  <label for="cn">Customer</label>
  <input type="text" class="form-control-plaintext form-control-sm" id="cn">
  <label for="pn">Product</label>
  <input type="text" class="form-control-plaintext form-control-sm" id="pn">
  <label for="qty">Qty</label>
  <input type="text" class="form-control-plaintext form-control-sm" id="qty">
  <label for="wo">W/O # <span id="wol"></span></label>
  <input type="text" class="form-control form-control-sm" id="wo">
  <label for="dd">Due Date</label>
  <input type="date" class="form-control form-control-sm" id="dd">
  <button type="submit" class="btn btn-primary">Add...</button>
</form>
