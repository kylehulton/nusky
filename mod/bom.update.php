<?php require_once('core/system.php');
$bom = htmlspecialchars($_POST['bom']);
$sc = htmlspecialchars($_POST['sc']);
$sd = htmlspecialchars($_POST['sd']);
$ub = new Database();
$ub->query('UPDATE bom SET BOM=:sc, FriendlyName=:sd, date_modified=:date_modified, modified_by=:modified_by WHERE BOM=:bom');
$ub->bind(':bom', $bom);
$ub->bind(':sc', $sc);
$ub->bind(':sd', $sd);
$ub->bind(':date_modified', $date_now);
$ub->bind(':modified_by', $CU_Fullname);
$ub->execute();
$ubi = new Database();
$ubi->query('UPDATE bom_items SET BOM=:sc WHERE BOM=:bom');
$ubi->bind(':bom', $bom);
$ubi->bind(':sc', $sc);
$ubi->execute();
?>
