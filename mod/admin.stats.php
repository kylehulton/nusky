<?php $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
$the_count = $db->prepare('SELECT (SELECT COUNT(*) FROM bom) as bomCount,
                                  (SELECT COUNT(*) from bom_items) as bom_itemsCount,
                                  (SELECT COUNT(*) from customers) as customerCount,
                                  (SELECT COUNT(*) from employees) as employeeCount,
                                  (SELECT COUNT(*) from goods_in) as goods_inCount,
                                  (SELECT COUNT(*) from goods_in_items) as items_inCount,
                                  (SELECT COUNT(*) from jobs) as jobsCount,
                                  (SELECT COUNT(*) from job_items) as job_itemCount,
                                  (SELECT COUNT(*) from log) as logCount,
                                  (SELECT COUNT(*) from login) as loginCount,
                                  (SELECT COUNT(*) from po) as poCount,
                                  (SELECT COUNT(*) from po_items) as po_itemsCount,
                                  (SELECT COUNT(*) from scratchpad) as scratchpadCount,
                                  (SELECT COUNT(*) from stock) as stockCount,
                                  (SELECT COUNT(*) from stores) as storesCount,
                                  (SELECT COUNT(*) from stores_locations) as stores_locCount,
                                  (SELECT COUNT(*) from userFeedback) as userFeedbackCount,
                                  (SELECT COUNT(*) from wo) as woCount');
$the_count->execute();
echo '<table class="table table-hover table-sm">
    <thead>
        <th colspan="2">Stats for Nerds</th>
    </thead>
    <tbody>';
while($ahahah = $the_count->fetch(PDO::FETCH_ASSOC)){
    $bom = $ahahah['bomCount'];
    $bom_items = $ahahah['bom_itemsCount'];
    $customers = $ahahah['customerCount'];
    $employees = $ahahah['employeeCount'];
    $goods_in = $ahahah['goods_inCount'];
    $goods_in_items = $ahahah['items_inCount'];
    $jobs = $ahahah['jobsCount'];
    $job_item = $ahahah['job_itemCount'];
    $log = $ahahah['logCount'];
    $login = $ahahah['loginCount'];
    $po = $ahahah['poCount'];
    $po_items = $ahahah['po_itemsCount'];
    $scratchpad = $ahahah['scratchpadCount'];
    $stock = $ahahah['stockCount'];
    $stores = $ahahah['storesCount'];
    $stores_loc = $ahahah['stores_locCount'];
    $userFeedback = $ahahah['userFeedbackCount'];
    $wo = $ahahah['woCount'];
    $ultimate_total = $bom + $bom_items + $customers + $employees + $goods_in + $goods_in_items + $jobs + $job_item + $log + $login + $po + $po_items + $scratchpad + $stock + $stores + $stores_loc + $userFeedback + $wo;
    echo '<tr><th scope="row">Total No# of BoMs:</th><td>'.$bom.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Items in Sequence:</th><td>'.$bom_items.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Customers:</th><td>'.$customers.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Employees:</th><td>'.$employees.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Deliveries:</th><td>'.$goods_in.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Products with those Deliveries:</th><td>'.$goods_in_items.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Jobs:</th><td>'.$jobs.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Products on those jobs:</th><td>'.$job_item.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Entries in the system log:</th><td>'.$log.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Users on this system:</th><td>'.$login.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Purchase Orders raised:</th><td>'.$po.'</td></tr>';
    echo '<tr><th scrop="row">Total No# of Items on those Orders:</th><td>'.$po_items.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Scratchpads created:</th><td>'.$scratchpad.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Products that have been through stock:</th><td>'.$stock.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Places where Items are Stored:</th><td>'.$stores_loc.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Storage spaces in those areas:</th><td>'.$stores.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Feedback from Users:</th><td>'.$userFeedback.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Works Orders raised:</th><td>'.$wo.'</td></tr>';
    echo '<tr><th scope="row">Total No# of Records, Everywhere:</th><td><b>'.$ultimate_total.'</b></td></tr>';
}
echo '</tbody>
</table>'; ?>
