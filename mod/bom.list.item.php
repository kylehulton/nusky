<?php $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
if(empty($_GET['bom'])){ header('HTTP/1.1 405 Looks like it\'s a MikeSQL error!'); exit(); } else { $bom = $_GET['bom']; }
$lbi = new database();
$lbi->query('SELECT bomi_id, BOM, stock_co, stock_desc, qty, item_no, comp_ref FROM bom_items WHERE BOM=:bom ORDER BY item_no');
$lbi->bind(':bom', $bom);
$lbi->execute();
$i = $lbi->resultset();
$ic = $lbi->rowCount();
?>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Item #</th>
      <th>Stock Code</th>
      <th>Description</th>
      <th>Qty</th>
      <th>Component Reference</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
<?php
foreach($i as $i){
  $id = $i['bomi_id'];
  $in = $i['item_no'];
  $sc = $i['stock_co'];
  $sd = $i['stock_desc'];
  $qty = $i['qty'];
  $cr = $i['comp_ref'];
  echo '<tr>
    <td>'.$in.'</td>
    <td>'.$sc.'</td>
    <td>'.$sd.'</td>
    <td>'.$qty.'</td>
    <td>'.$cr.'</td>
    <td><button class="badge badge-warning btn" data-toggle="modal" data-target="#Modal" data-action="ebEditComp" data-bomi="'.$id.'" data-itemno="'.$in.'" data-stockcode="'.$sc.'" data-desc="'.$sd.'" data-qty="'.$qty.'" data-compref="'.$cr.'">Edit</button> ';
    if ($ic > 1){
      echo '<button id="Re-Seq" type="button" class="badge badge-info btn dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Re-Seq</button>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Re-Seq">';
    if ($in > 1){ echo '<button class="dropdown-item" >Move to Top</button>
        <button class="dropdown-item" >Move Up</button>'; }
    if ($in < $ic){ echo '<button class="dropdown-item" >Move Down</button>
        <button class="dropdown-item" >Move to Bottom</button>'; }
    echo '</div> '; }
    echo '<button class="badge badge-danger btn" data-toggle="modal" data-target="#Modal" data-action="ebDeleteComp" data-bomi="'.$id.'" data-itemno="'.$in.'" data-stockcode="'.$sc.'" data-desc="'.$sd.'" data-qty="'.$qty.'" data-compref="'.$cr.'">Delete</button></td>
  </tr>'; }
?>
  </tbody>
</table>
