<?php require_once("core/system.php");
if(empty($_GET['p'])){ $page = 1; } else { $page = $_GET['p']; }
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20; $start=($previous*$limit);
$lu = new database();
$lu->query('SELECT displayname, username, status from login where username NOT LIKE "administrator" LIMIT '.$start.', '.$limit);
$lu->execute();
$u = $lu->resultset();
$cu = new database();
$cu->query('SELECT COUNT(*) as TheCount from login');
$cu->execute();
$rows = $cu->resultset();
foreach ($rows as $r) {
  $TheCount = $r['TheCount'];
}
$TheCount = $TheCount - 1; // Omit Administrator
$lastpage = ceil($TheCount/$limit);
$secondtolast = $lastpage - 1;
$thirdtolast = $lastpage - 2;
$call = "AdmUserList";
PrepPagination();
?>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Display Name</th>
      <th>Username</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($u as $u) {
  $dn = $u['displayname'];
  $un = $u['username'];
  $st = $u['status'];
  echo '<tr>
    <td>'.$dn.'</td>
    <td>'.$un.'</td>
    <td>
      <button id="Actions" type="button" class="badge acrylic badge-neutral btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
        <h6 class="dropdown-header">Actions for '.$dn.'</h6>';
  if ($st == "Active"){
    echo '<button class="btn dropdown-item" onclick="AdmUserDisable(\''.$un.'\')">Disable</button>';
  }
  if ($st == "Disabled"){
    echo '<button class="btn dropdown-item" onclick="AdmUserEnable(\''.$un.'\')">Enable</button>';
  }
  echo '<button class="btn dropdown-item" onclick="AdmUserPassReset(\''.$un.'\')">Reset Password</button>
        <div class="dropdown-divider"></div>
        <button class="btn dropdown-item" onclick="AdmUserDelete(\''.$un.'\')">Delete</button>
        <button class="btn dropdown-item" onclick="AdmUserRename(\''.$un.'\')">Rename</button>
        <div class="dropdown-divider"></div>
        <button class="btn dropdown-item" onclick="AdmUserProperties(\''.$un.'\')"><b>Properties</b></button>
      </div>
    </td>
  </tr>';
}
print_r('</tbody>
</table>
<div class="row justify-content-center">
  <nav aria-label="Pages for Users">');
  Pagination();
print_r('</nav>
</div>'); ?>
