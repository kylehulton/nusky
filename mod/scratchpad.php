<?php
require_once('core/system.php');
?>
<nav class="breadcrumb">
  <div class="row align-items-center">
    <div class="col-md-auto">
    <?php
    $sp = $db->prepare('SELECT * FROM scratchpad WHERE username=:u');
    $sp->execute(array('u' => $CU_Username));
    $r = $sp->rowCount();
    if ($r > 0){
      echo '<button class="btn btn-primary" id="SaveScratchpad">Save</button>';
    }else{
      echo '<button class="btn btn-primary" id="NewScratchpad">New</button>';
    }
    ?>
    </div>
  </div>
</nav>
<?php
if ($r > 0){
  echo '<textarea class="form-control container" id="Scratchpad"></textarea>';
}else{ 
?>
<h4 class="display-4">What is Scratchpad?</h4>
<p class="lead">Scratchpad is a persistant scrap of paper that will always be here for you to make notes on.</p>
<p class="lead">Once you make create one, that's it!</p>
<p class="lead">You'll never see this message again and this whole page will just become a notepad for you to scribble things down.</p>
<p class="lead">Enjoy!</p>
<?php
}
?>
