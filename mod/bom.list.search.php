<?php require_once("core/system.php");
if(empty($_GET['q'])){ $q = FALSE; } else { $q = rawurldecode($_GET['q']); }
$q = htmlspecialchars($q);
print_r('<h4 class="display-5">Searching First '.$Limit_B.' Results for "'.$q.'"</h4>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>BoM #</th>
      <th>Product</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>');
$q = strtolower($q);
$bs = new database();
$bs->query('SELECT * FROM bom WHERE LCASE(BOM) LIKE concat("%", :SearchParam, "%") OR LCASE(FriendlyName) LIKE concat("%", :SearchParam, "%") ORDER BY BOM_ID DESC LIMIT '.$Limit_A.', '.$Limit_B);
$bs->bind(":SearchParam", $q);
$bs->execute();
$b = $bs->resultset();
$rb = $bs->rowCount();
if ($rb > 0){
  $a = new bom();
  $a -> list($b);
} else {
  echo '<tr><td>No Results Found.</td></tr>';
}
print_r("</tbody>
</table>"); ?>
