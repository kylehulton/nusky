<?php require_once("core/system.php");
if(empty($_GET['p'])){ $page = 1; } else { $page = $_GET['p']; }
if(empty($_GET['cat'])){ $cat = FALSE; } else { $cat = $_GET['cat']; }
if(empty($_GET['info'])){ $info = FALSE; } else { $info = $_GET['info']; }
$info = str_split($info, 7);
if($info[0] == "Created"){ ?><div class="alert alert-success" role="alert">Job <?php echo $info[1]; ?> Created! (Nicely Done)</div> <?php }
if($info[0] == "Deleted"){ ?><div class="alert alert-warning" role="alert">Job <?php echo $info[1]; ?> Deleted! (Nicely Done)</div> <?php }
if($info[0] == "MarkonH"){ ?><div class="alert alert-warning" role="alert">Job <?php echo $info[1]; ?> Marked on Hold! (Nicely Done)</div> <?php }
if($info[0] == "MarkasA"){ ?><div class="alert alert-success" role="alert">Job <?php echo $info[1]; ?> Marked as Active! (Nicely Done)</div> <?php }
if($info[0] == "MarkasC"){ ?><div class="alert alert-info" role="alert">Job <?php echo $info[1]; ?> Marked as Complete! (Nicely Done)</div> <?php }
$breakup = 3;
$previous = $page - 1;
$next = $page + 1;
$limit=20; $start=($previous*$limit);
$call = "listJobs";
$lj = new database();
$cj = new database();
?>
<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th colspan="4">
        <?php
        if($cat == "Completed"){ echo '<h5>Completed Jobs</h5>';
        }elseif($cat == "Held"){ echo '<h5>Jobs On Hold</h5>';
        }else{ echo '<h5>Active Jobs</h5>'; }
        ?>
      </th>
    </tr>
    <tr>
      <th>Job No #</th>
      <th>Customer</th>
      <th>Comments</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
<?php
if($cat == "Completed"){
  $lj->query('SELECT * FROM jobs WHERE status="Completed" ORDER BY Job_NO DESC LIMIT '.$start.', '.$limit);
  $lj->execute();
  $j = $lj->resultset();
  $cj->query('SELECT COUNT(*) as TheCount FROM jobs WHERE status="Completed"');
  $cj->execute();
  $rows = $cj->resultset();
  foreach ($rows as $r) {
    $TheCount = $r['TheCount'];
  }
  $lastpage = ceil($TheCount/$limit);
  $secondtolast = $lastpage - 1;
  $thirdtolast = $lastpage - 2;
  PrepPagination();
  if ($TheCount > 0){
    foreach ($j as $j){
      $Job_NO = $j['Job_NO'];
      $Customer = $j['Customer'];
      $Comments = $j['Comments'];
      print_r('<tr>
        <th class="align-middle" scope="row">'.$Job_NO.'</th>
        <td class="align-middle">'.$Customer.'</td>
        <td class="align-middle">'.$Comments.'</td>
        <td class="align-middle">
          <button id="Actions" class="badge acrylic badge-neutral btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
            <div class="row">
              <div class="col-6">
                <h6 class="dropdown-header">Actions for '.$Job_NO.'</h6>
                <button class="dropdown-item" onClick="JobsView(\''.$Job_NO.'\')"><b>View</b></button>
                <button class="dropdown-item"" onClick="JobsEdit(\''.$Job_NO.'\')">Edit</button>
                <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Delete" data-job_no="'.$Job_NO.'">Delete</button>
              </div>
              <div class="col-6">
                <h6 class="dropdown-header"><small>Mark As...</small></h6>
                <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Active" data-job_no="'.$Job_NO.'" >Active</button>
                <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Hold" data-job_no="'.$Job_NO.'" >On Hold</button>
              </div>
            </div>
          </div>
        </td>
      </tr>');
    }
  }else{
    echo '<tr>
    <th colspan="4">No Results Found</th>
    </tr>';
  }
}elseif($cat == "Held"){
  $lj->query('SELECT * FROM jobs WHERE status="Hold" ORDER BY Job_NO DESC LIMIT '.$start.', '.$limit);
  $lj->execute();
  $j = $lj->resultset();
  $cj->query('SELECT COUNT(*) as TheCount FROM jobs WHERE status="Hold"');
  $cj->execute();
  $rows = $cj->resultset();
  foreach ($rows as $r) {
    $TheCount = $r['TheCount'];
  }
  $lastpage = ceil($TheCount/$limit);
  $secondtolast = $lastpage - 1;
  $thirdtolast = $lastpage - 2;
  PrepPagination();
  if ($TheCount > 0){
    foreach ($j as $j){
      $Job_NO = $j['Job_NO'];
      $Customer = $j['Customer'];
      $Comments = $j['Comments'];
      print_r('<tr>
        <th class="align-middle" scope="row">'.$Job_NO.'</th>
        <td class="align-middle">'.$Customer.'</td>
        <td class="align-middle">'.$Comments.'</td>
        <td class="align-middle">
          <button id="Actions" class="badge acrylic badge-neutral btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
            <div class="row">
              <div class="col-6">
                <h6 class="dropdown-header">Actions for '.$Job_NO.'</h6>
                <button class="dropdown-item" onClick="JobsView(\''.$Job_NO.'\')"><b>View</b></button>
                <button class="dropdown-item"" onClick="JobsEdit(\''.$Job_NO.'\')">Edit</button>
                <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Delete" data-job_no="'.$Job_NO.'">Delete</button>
                </div>
                <div class="col-6">
                  <h6 class="dropdown-header"><small>Mark As...</small></h6>
                  <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Active" data-job_no="'.$Job_NO.'" >Active</button>
                  <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Complete" data-job_no="'.$Job_NO.'" >Complete</button>
                </div>
            </div>
          </div>
        </td>
      </tr>');
    }
  }else{
    echo '<tr>
    <th colspan="4">No Results Found</th>
    </tr>';
  }
}else{
  $lj->query('SELECT * FROM jobs WHERE status="Active" ORDER BY Job_NO DESC LIMIT '.$start.', '.$limit);
  $lj->execute();
  $j = $lj->resultset();
  $cj->query('SELECT COUNT(*) as TheCount FROM jobs WHERE status="Active"');
  $cj->execute();
  $rows = $cj->resultset();
  foreach ($rows as $r) {
    $TheCount = $r['TheCount'];
  }
  $lastpage = ceil($TheCount/$limit);
  $secondtolast = $lastpage - 1;
  $thirdtolast = $lastpage - 2;
  PrepPagination();
  if ($TheCount > 0){
    foreach ($j as $j){
      $Job_NO = $j['Job_NO'];
      $Customer = $j['Customer'];
      $Comments = $j['Comments'];
      print_r('<tr>
        <th class="align-middle" scope="row">'.$Job_NO.'</th>
        <td class="align-middle">'.$Customer.'</td>
        <td class="align-middle">'.$Comments.'</td>
        <td class="align-middle">
          <button id="Actions" class="badge acrylic badge-neutral btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
            <div class="row">
              <div class="col-6">
                <h6 class="dropdown-header">Actions for '.$Job_NO.'</h6>
                <button class="dropdown-item" onClick="JobsView(\''.$Job_NO.'\')"><b>View</b></button>
                <button class="dropdown-item"" onClick="JobsEdit(\''.$Job_NO.'\')">Edit</button>
                <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Delete" data-job_no="'.$Job_NO.'">Delete</button>
                </div>
                <div class="col-6">
                  <h6 class="dropdown-header"><small>Mark As...</small></h6>
                  <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Hold" data-job_no="'.$Job_NO.'" >On Hold</button>
                  <button class="dropdown-item" data-toggle="modal" data-target="#Modal" data-action="Complete" data-job_no="'.$Job_NO.'" >Completed</button>
                </div>
              </div>
            </div>
        </td>
      </tr>');
    }
  }else{
    echo '<tr>
    <th colspan="4">No Results Found</th>
    </tr>';
  }
}
print_r('</tbody>
</table>
<div class="row justify-content-center">
  <nav aria-label="Pages for Stock">');
  Pagination();
print_r('</nav>
</div>'); ?>
