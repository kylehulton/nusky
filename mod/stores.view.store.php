<?php require_once("core/system.php");
if (empty($_GET['a'])){
  header('HTTP/1.1 400 No Box');
  exit();
} else {
  $a = $_GET['a'];
}
if (empty($_GET['b'])){
  header('HTTP/1.1 400 No Location');
  exit();
} else {
  $b = $_GET['b'];
}
$getBox = new database();
$getBox->query('SELECT * FROM stores WHERE stores_name=:name and stores_location=:at');
$getBox->bind(':name', $a);
$getBox->bind(':at', $b);
$getBox->execute();
$c = $getBox->fetchAll();
foreach ($c as $c) {
  $Box_id = $c['idstores'];
  $Box_Name = $c['stores_name'];
  $Box_At = $c['stores_location'];
}
$openBox = new database();
$openBox->query('SELECT * FROM goods_in_items WHERE stock_loc=:name and store_loc=:at and qty_now > 0');
$openBox->bind(':name', $Box_Name);
$openBox->bind(':at', $Box_At);
$openBox->execute();
$d = $openBox->fetchAll();
print_r('<div class="row">
  <div class="col"><h3>Opening Bin <small class="text-muted">'.$Box_Name.' in '.$Box_At.'</small></h3></div>
</div>
<ul class="ms-List">');
foreach ($d as $d) {
  $Item_Name = $d['stock_co'];
  $Item_Desc = $d['stock_desc'];
  $Item_Qty = $d['qty_now'];
  print_r('<li class="ms-ListItem" tabindex="0">
    <span class="ms-ListItem-primaryText">'.$Item_Name.'</span>
    <span class="ms-ListItem-secondaryText">'.$Item_Desc.'</span>
    <span class="ms-ListItem-metaText">'.$Item_Qty.'</span>
    <div class="ms-ListItem-actions">
      <div class="ms-ListItem-action">
        <i class="ms-Icon ms-Icon--Delete"></i>
      </div>
      <div class="ms-ListItem-action">
        <i class="ms-Icon ms-Icon--Flag"></i>
      </div>
    </div>
  </li>');
}
echo '</ul>';
