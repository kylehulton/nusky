<?php require_once("core/system.php");
$cu = htmlspecialchars($_POST['cu']);
$nu = htmlspecialchars($_POST['nu']);
$fn = htmlspecialchars($_POST['fn']);
$sn = htmlspecialchars($_POST['sn']);
$dn = htmlspecialchars($_POST['dn']);
$aur = new Database();
$aur->query('UPDATE login SET username=:nu, firstname=:fn, surname=:sn, displayname=:dn WHERE username=:cu');
$aur->bind(':nu', $nu);
$aur->bind(':fn', $fn);
$aur->bind(':sn', $sn);
$aur->bind(':dn', $dn);
$aur->bind(':cu', $cu);
$aur->execute();
?>
