<?php require_once("core/system.php");
if (empty($_GET['a'])){
  header('HTTP/1.1 400 No BOM');
  exit();
} else {
  $a = $_GET['a'];
}
?>
<div class="row">
  <div class="col">
    <h3 id="BOM"><small class="text-muted"><?php echo $a; ?></small></h3>
  </div>
  <div class="col">
    <input class="form-control acrylic" id="Qty" placeholder="Enter Quantity" />
  </div>
</div>
