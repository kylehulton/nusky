<?php require_once("core/system.php");
if(empty($_POST['c'])){ $c = FALSE; } else { $c = $_POST['c']; }
$lscat = new database();
$lscat->query('SELECT category, COUNT(*) AS TheCount from stock Group By category');
$lscat->execute();
$cat = $lscat->resultset();
?>
<h5 class="pt-2">Filters</h5>
<form autocomplete="off">
  Categories
  <select size="5" class="form-control form-control-sm acrylic max" id="Category" onchange="filterCat()">
    <?php foreach ($cat as $cat) {
      $name = $cat['category'];
      $TheCount = $cat['TheCount'];
      if ($name == NULL ){ $name = "Uncategorised"; }
      echo '<option value="'.$name.'"';
      if ($name == $c){ echo 'selected'; }
      echo ' >'.$name.' ('.$TheCount.')</option>';
    }
    ?>
  </select>
</form>
<?php
if ($c !== FALSE){
  $lstype = new database();
  $lstype->query('SELECT comp_type, COUNT(*) AS TheSecondCount from stock where category=:cat Group By comp_type');
  $lstype->bind(":cat", $c);
  $lstype->execute();
  $comp_type = $lstype->resultset();
  ?>
  <form autocomplete="off">
    Component Types
    <select size="5" class="form-control form-control-sm acrylic max" id="ComponentType">
      <?php foreach ($comp_type as $comp_type) {
        $name = $comp_type['comp_type'];
        $TheSecondCount = $comp_type['TheSecondCount'];
        if ($name == NULL ){ $name = "Not Specified"; }
        echo '<option value="'.$name.'">'.$name.' ('.$TheSecondCount.')</option>';
      }
      ?>
    </select>
  </form>
  <?php
}
?>
