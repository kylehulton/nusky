<?php require_once("core/system.php");
if (empty($_SESSION['Username'])) {
  header('HTTP/1.1 403 Not Logged In');
  exit();
}
if (empty($_POST['pass'])) {
  header('HTTP/1.1 400 No Password');
  exit();
} else {
  $oldpass = sha1($_POST['pass']);
  $newpass = sha1($_POST['npass']);
  $confirmpass = sha1($_POST['cpass']);
}
$getpass = new database();
$getpass->query('SELECT password FROM login WHERE username=:username');
$getpass->bind(':username', $_SESSION['Username']);
$getpass->execute();
$passresult = $getpass->fetchAll();
foreach ($passresult as $r){
  $pass = $r['password'];
}
if ($oldpass != $pass){
  echo "WrongPass";
  exit();
} else {
  $updatepass = new database();
  $updatepass->query('UPDATE login SET password=:password WHERE username=:username');
  $updatepass->bind(':password', $newpass);
  $updatepass->bind(':username', $_SESSION['Username']);
  $updatepass->execute();
  echo "UpdatePass";
  exit();
}
