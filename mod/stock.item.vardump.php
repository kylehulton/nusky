<?php $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
if(empty($_GET['id'])){ $id = FALSE; } else { $id = $_GET['id']; }
$lsi = $db->prepare('SELECT * FROM stock WHERE stock_id=:id');
$lsi->execute(array('id' => $id));
echo '<table class="table table-hover table-sm">
  <thead><tr><th>VarDump for Stock id: '.$id.'</th></tr></thead>
  <tbody>';
while($c = $lsi->fetch(PDO::FETCH_ASSOC)){
  foreach($c as $columnName=>$data){
    echo '<tr><th scope="row">'.$columnName.'</th><td>'.$data.'</td></tr>';
  }
}
echo '</tbody>
</table>'; ?>
