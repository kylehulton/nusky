<?php $page = 1; $Limit_A=0; $Limit_B=25; $Limit_A=($page-1)*$Limit_B; $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
if(empty($_GET['gdi'])){ $gdi = FALSE; } else { $gdi = $_GET['gdi']; }
print_r('<button class="btn btn-primary btn-sm" onclick="listGoods(\'1\', \'In\')">Back to Deliveries...</button>
<table class="table table-sm table-hover">
    <thead>
        <tr>
            <th>Stock Code</th>
            <th>Description</th>
            <th>Location</th>
            <th>Qty</th>
            <th>Stock Level</th>
        </tr>
    </thead>
    <tbody>');
$lgi = $db->prepare('SELECT * FROM goods_in_items WHERE gdi_id=:gdi ORDER BY gdi_id');
$lgi->execute(array('gdi' => $gdi));
while($c = $lgi->fetch(PDO::FETCH_ASSOC)){
    $sc = $c['stock_co'];
    $sd = $c['stock_desc'];
    $sl = $c['stock_loc'];
    $scqty = $c['qty_now'];
    $soqty = $c['qty_in'];
    $sqty_d = ($scqty / $soqty);
    $sqty_p = ($sqty_d * 100);
    echo '<tr>
        <td>'.$sc.'</td>
        <td>'.$sd.'</td>
        <td>'.$sl.'</td>
        <td>'.$scqty.'/'.$soqty.'</td>
        <td><div class="progress">
        <div class="progress-bar" role="progressbar" style="width: '.$sqty_p.'%;" aria-valuenow="'.$sqty_p.'" aria-valuemin="0" aria-valuemax="100"></div>
        </div></td>
    </tr>'; }
print_r("</tbody>
</table>"); ?>
