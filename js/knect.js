var version = '0.14.1165.1802.14.Lion_prerelease';
// 0.14 - Stores
// 0.15 - either Purchasing or Suppliers or GRNs
// 0.16 - See 0.15
// 0.17 - See 0.15
// 0.18 - Traversing modules i.e. view customer -> view their jobs (Common Viewport)
// 0.19 - Full Ajax Reform
// 1.00 - Full PHP Class Reform
var title = document.title;
var BOMitem_no = 1;
var prog_name = "nuSky";
var AdmNUdisplayname;
var AdmUserStatusDisabled;

function nextSlide() {
  $('.carousel').carousel('next');
}
function prevSlide() {
  $('.carousel').carousel('prev');
}
function toggleThisCollapse(a) {
  $('#' + a).collapse('toggle');
}
function showModal() {
  $('#Modal').modal('show');
}
function hideModal() {
  $('#Modal').modal('hide');
}
function isModalOpen() {
  if ($('body').hasClass('modal-open')){
    return true;
  } else {
    return false;
  }
}
function modalSoftError() {
  $('#MYB').attr('class', 'd-none');
  $('#MNB').attr('class', 'btn btn-warning');
  $('#MNB').attr('onclick', 'hideModal()');
  $('#MNB').text('Continue');
}
function modalAsLoader() {
  $('body').attr('style', 'cursor: progress;');
  $('#modal-dialog').attr('class', 'modal-dialog modal-dialog-centered');
  $('#modal-header').attr('class', 'd-none');
  $('#modal-body').attr('class', '');
  $('#modal-body').html('<div class="loader">\
    <div class="circle"></div>\
    <div class="circle"></div>\
    <div class="circle"></div>\
    <div class="circle"></div>\
    <div class="circle"></div>\
  </div>');
  $('#modal-footer').attr('class', '');
  $('#modal-footer').html('');
}
function modalReset() {
  $('body').attr('style', '');
  $('#modal-dialog').attr('class', 'modal-dialog');
  $('#modal-header').attr('class', 'modal-header');
  $('#modal-body').attr('class', 'modal-body');
  $('#modal-footer').attr('class', 'modal-footer');
  $('#modal-footer').html('<div id="ModalStatus"></div>\
  <button class="btn btn-danger" id="MYB">Yes</button>\
  <button class="btn btn-secondary" id="MNB">No</button>');
}
function NavigationReset() {
  $('#sidebar > a').attr('class', 'nav-link');
  $('#Status').text('');
  $('#Omnibox').val('');
  $('#Omnibox').attr('class', 'form-control acrylic dark-max');
  $('#Omnibox').attr('placeholder', 'Cannot Search right now...');
  $('#Omnibox').prop('disabled', true);
}
function soft400() {
  $('#Modal').modal('show');
  $('.modal-dialog').attr('class', 'modal-dialog modal-lg');
  $('#ModalLabel').text('Bad Request');
  $('.modal-body').html('<p class="lead">The request cold not be processed by the server due to invalid syntax.</p>');
  modalSoftError();
}
function soft404() {
  $('#Modal').modal('show');
  $('.modal-dialog').attr('class', 'modal-dialog modal-lg');
  $('#ModalLabel').text('Page cannot be displayed');
  $('.modal-body').html('<p class="lead">Funny 404 reference could not be found.</p>');
  modalSoftError();
}
function soft418() {
  $('#Modal').modal('show');
  $('.modal-dialog').attr('class', 'modal-dialog modal-lg');
  $('#ModalLabel').text('I\'m a Tea Pot');
  $('.modal-body').html('<p class="lead">The server is a teapot, it cannot brew coffee.</p>');
  modalSoftError();
}
function ValidateAlphanumeric(username) {
  var pattern = /^[a-zA-Z0-9_-]/;
  if ( pattern.test(username) ) {
    return true;
  } else {
    return false;
  }
}
function ValidateEmail(email) {
  var pattern = /[a-z0-9]+[_a-z0-9\.-]*[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/ig;
  if ( pattern.test(email) ) {
    return true;
  } else {
    return false;
  }
}
function ComparePasswords(pass, cpass) {
  if ( pass != cpass ) {
    return false;
  } else {
    return true;
  }
}
function ValidateContainsAdmin(username) {
  var pattern = /(?:admin)|(sa)|(?:system)|(?:spimin)|(?:nusky)|(?:manager)/i;
  if ( pattern.test(username) ) {
    return true;
  } else {
    return false;
  }
}
// Admin Tools
function AdmReset() {
  $('#Status').text('');
  $('#Status').attr('class', 'col mx-4');
  $('#Status').attr('role', '');
  $('li#AdmLinkHome').attr('class', 'nav-item');
  $('li#AdmLinkUserMan').attr('class', 'nav-item');
  $('a#AdmLinkUserManNew').attr('class', 'nav-link');
  $('a#AdmLinkUserManNew').attr('onclick', 'AdmNU()');
}
function AdmStatsShow() {
  $.ajax({
    method: 'GET',
    url: './mod/admin.stats.php',
    cache: false,
    success: function (data) {
      AdmReset();
      $('li#AdmLinkHome').attr('class', 'nav-item active');
      $('#AdmViewport').html(data);
    }
  });
}
function AdmUserList() {
  $.ajax({
    method: 'GET',
    url: './mod/admin.user.list.php',
    cache: false,
    success: function (data) {
      $('#AdmUserManViewport').html(data);
    }
  });
}
function AdmNU() {
  $.ajax({
    method: 'GET',
    url: './mod/wizard.admin.user.new.html',
    cache: false,
    success: function (data) {
      $('#AdmUserManViewport').html(data);
    },
    complete: function () {
      $('a#AdmLinkUserManNew').attr('class', 'nav-link active');
      $('a#AdmLinkUserManNew').attr('onclick', '');
      $('#Status').text('New User Wizard');
    },
    error: function () {
      soft404();
    }
  });
}
// Step 1 Validation for New User Wizard
function AdmNUS1V() {
  var AdmNUusername = $('#Username').val();
  var AdmNUfirstname = $('#FirstName').val();
  var AdmNUsurname = $('#Surname').val();
  var AdmNUemail = $('#Email').val();
  if ( AdmNUsurname.length < 1 ) {
    AdmNUdisplayname = AdmNUfirstname;
  } else {
    AdmNUdisplayname = AdmNUfirstname + ' ' + AdmNUsurname;
  }
  $.ajax({
    method: 'POST',
    url: './mod/admin.user.check.php',
    data: 'q=' + AdmNUusername,
    cache: false,
    success: function (data) {
      // Check that User doesn't already exist, that the username is entered and the first name is entered
      var AdmNUuserexist = data;
      if ( ValidateContainsAdmin(AdmNUusername) == true ) {
        $('#AdmNUE1').text('You cannot use that username!');
        $('#AdmNUE1').attr('class', 'col alert alert-danger mx-4');
        $('#AdmNUE1').attr('role', 'alert');
      } else if ( AdmNUuserexist > 0 ) {
        $('#AdmNUE1').text('The user "' + AdmNUusername + '" already exists. Please use another.');
        $('#AdmNUE1').attr('class', 'col alert alert-danger mx-4');
        $('#AdmNUE1').attr('role', 'alert');
      } else if ( AdmNUusername.length < 1 ) {
        $('#AdmNUE1').text('You haven\'t entered a username.');
        $('#AdmNUE1').attr('class', 'col alert alert-warning mx-4');
        $('#AdmNUE1').attr('role', 'alert');
      } else if ( ValidateAlphanumeric(AdmNUusername) == false ) {
        $('#AdmNUE1').text('You cannot use special characters in the username. Please use only letters and/or numbers.');
        $('#AdmNUE1').attr('class', 'col alert alert-danger mx-4');
        $('#AdmNUE1').attr('role', 'alert');
      } else if ( AdmNUfirstname.length < 1 ) {
        $('#AdmNUE1').text('You haven\'t entered a first name.');
        $('#AdmNUE1').attr('class', 'col alert alert-warning mx-4');
        $('#AdmNUE1').attr('role', 'alert');
      } else if ( AdmNUemail.length > 0 && ValidateEmail(AdmNUemail) == false ) {
        $('#AdmNUE1').text('You haven\'t entered a valid email address.');
        $('#AdmNUE1').attr('class', 'col alert alert-warning mx-4');
        $('#AdmNUE1').attr('role', 'alert');
      } else {
        $('#AdmNUE1').text('');
        $('#AdmNUE1').attr('class', 'col');
        $('#AdmNUE1').attr('role', '');
        // Now lets output the values to overview
        var output = '<table class="table table-sm">\
          <tbody>\
            <tr>\
              <th scope="row">Username</th>\
              <td id="AdmNUusername">' + AdmNUusername + '</td>\
            </tr>\
            <tr>\
              <th scope="row">Display Name</th>\
              <td id="AdmNUdisplayname">' + AdmNUdisplayname + '</td>\
            </tr>\
          </tbody>\
        </table>';
        $('#AdmNUS1V').html(output);
        nextSlide();
      }
    }
  });
}
// Step 2 Validation for New User Wizard
function AdmNUS2V() {
  var AdmNUpassword = $('#Password').val();
  var AdmNUconfirmpassword = $('#ConfirmPassword').val();
  if ( ComparePasswords(AdmNUpassword, AdmNUconfirmpassword) == false ) {
    $('#AdmNUE2').text('The passwords you have entered do not match.');
    $('#AdmNUE2').attr('class', 'col alert alert-danger mx-4');
    $('#AdmNUE2').attr('role', 'alert');
  } else if ( AdmNUpassword.length < 1 ) {
    $('#AdmNUE2').text('The password cannot be blank.');
    $('#AdmNUE2').attr('class', 'col alert alert-warning mx-4');
    $('#AdmNUE2').attr('role', 'alert');
  } else if ( AdmNUpassword.length < 4 ) {
    $('#AdmNUE2').text('The password entered is too short. Minimum length is 4');
    $('#AdmNUE2').attr('class', 'col alert alert-warning mx-4');
    $('#AdmNUE2').attr('role', 'alert');
  } else {
    $('#AdmNUE2').text('');
    $('#AdmNUE2').attr('class', 'col');
    $('#AdmNUE2').attr('role', '');
    nextSlide();
  }
}
// Add the User
function AdmNUAddUser() {
  // Collect the User Details and Password
  var AdmNUusername = $('#Username').val();
  var AdmNUfirstname = $('#FirstName').val();
  var AdmNUsurname = $('#Surname').val();
  if ( AdmNUsurname.length < 1 ) {
    AdmNUdisplayname = AdmNUfirstname;
  } else {
    AdmNUdisplayname = AdmNUfirstname + ' ' + AdmNUsurname;
  }
  var AdmNUemail = $('#Email').val();
  var AdmNUpassword = $('#Password').val();
  $.ajax({
    method: 'POST',
    url: './mod/admin.user.new.php',
    data: 'u=' + AdmNUusername + '&f=' + AdmNUfirstname + '&s=' + AdmNUsurname + '&d=' + AdmNUdisplayname + '&e=' + AdmNUemail + '&p=' + AdmNUpassword,
    cache: false,
    success: function () {
      AdmUserList();
      AdmReset();
      $('li#AdmLinkUserMan').attr('class', 'nav-item active');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
      $('#Status').attr('role', 'alert');
      $('#Status').html('<small>The user "' + AdmNUusername + '" has been created.</small>');
    }
  });
}
// Disable User
function AdmUserDisable(user) {
  $.ajax({
    method: 'POST',
    url: './mod/admin.user.disable.php',
    data: 'u=' + user,
    cache: false,
    success: function () {
      $('#Status').html('<small>' + user + ' has been disabled.</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
      $('#Status').attr('role', 'alert');
      AdmUserList();
    },
    error: function () {
      $('#Status').html('<small>There was an error disabling ' + user + '</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
      $('#Status').attr('role', 'alert');
    }
  });
}
// Enable User
function AdmUserEnable(user) {
  $.ajax({
    method: 'POST',
    url: './mod/admin.user.enable.php',
    data: 'u=' + user,
    cache: false,
    success: function () {
      $('#Status').html('<small>' + user + ' has been enabled.</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
      $('#Status').attr('role', 'alert');
      AdmUserList();
    },
    error: function () {
      $('#Status').html('<small>There was an error enabling ' + user + '</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
      $('#Status').attr('role', 'alert');
    }
  });
}
// User Password Reset
function AdmUserPassReset(user) {
  $.ajax({
    method: 'POST',
    url: './mod/admin.user.check.status.php',
    data: 'u=' + user,
    cache: false,
    success: function (data) {
      var AdmUserstatus = data;
      if ( AdmUserstatus == "Disabled" ) {
        AdmUserStatusDisabled = "checked";
      } else {
        AdmUserStatusDisabled;
      }
      showModal();
      $('.modal-dialog').attr('class', 'modal-dialog');
      $('#ModalLabel').html('Reset Password - <span id="AdmUPRUser">' + user + '</span>');
      var output = '<form autocomplete="off">\
        <div class="form-group row">\
          <label class="col-sm-4 col-form-label" for="AdmUPRNew">New password</label>\
          <div class="col-sm-8">\
            <input type="password" id="AdmUPRNew" name="AdmUPRNew" class="form-control form-control-sm" required>\
          </div>\
        </div>\
        <div class="form-group row">\
          <label class="col-sm-4 col-form-label" for="AdmUPRConfirm">Confirm password</label>\
          <div class="col-sm-8">\
            <input type="password" id="AdmUPRConfirm" name="AdmUPRConfirm" class="form-control form-control-sm" required>\
          </div>\
        </div>\
        <div class="form-group row">\
          <div class="col">\
            <div class="form-check">\
              <div class="custom-control custom-checkbox">\
                <input type="checkbox" class="custom-control-input" id="AdmUPRMustChange">\
                <label class="custom-control-label" for="AdmUPRMustChange">User must change password at next logon</label>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div class="form-group row">\
          <div class="col">User lockout status: ' + AdmUserstatus + '</div>\
        </div>\
        <div class="form-group row">\
          <div class="col">\
            <div class="form-check">\
              <div class="custom-control custom-checkbox">\
                <input type="checkbox" class="custom-control-input" id="AdmUPRuserstatus" ' + AdmUserStatusDisabled + '>\
                <label class="custom-control-label" for="AdmUPRuserstatus">Lock user\'s account</label>\
              </div>\
            </div>\
          </div>\
        </div>\
      </form>';
      $('.modal-body').html(output);
      $('#MYB').attr('class', 'btn btn-primary');
      $('#MYB').text('OK');
      $('#MYB').attr('onclick', 'AdmEUpassreset("' + user + '")');
      $('#MNB').attr('class', 'btn btn-outline-secondary');
      $('#MNB').text('Cancel');
      $('#MNB').attr('onClick', 'hideModal()');
    },
    error: function () {
      $('#Status').html('<small>Error loading Reset Password module</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
      $('#Status').attr('role', 'alert');
    }
  });
}
function AdmEUpassreset(){
  var user = $('#AdmUPRUser').text();
  var pass = $('#AdmUPRNew').val();
  var cpass = $('#AdmUPRConfirm').val();
  var mustchange;
  if ($('#AdmUPRMustChange').prop('checked')) {
    mustchange = true;
  } else {
    mustchange = false;
  }
  var lockout;
  if ($('#AdmUPRuserstatus').prop('checked')) {
    lockout = true;
  } else {
    lockout = false;
  }
  if ( ComparePasswords(pass, cpass) == false ) {
    $('#ModalStatus').html('<small>The passwords must match. Please re-type them.</small>');
    $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
    $('#ModalStatus').attr('role', 'alert');
  } else if ( pass.length < 1 ) {
    $('#ModalStatus').html('<small>The password cannot be blank.</small>');
    $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
    $('#ModalStatus').attr('role', 'alert');
  } else if ( pass.length < 4 ) {
    $('#ModalStatus').html('<small>The passwords are too short. Please re-type them.</small>');
    $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
    $('#ModalStatus').attr('role', 'alert');
  } else {
    $.ajax({
      method: 'POST',
      url: './mod/admin.user.passreset.php',
      data: 'u=' + user + '&p=' + pass + '&m=' + mustchange + '&l=' + lockout,
      cache: false,
      success: function () {
        AdmUserList();
        hideModal();
        $('#AdmStatusUPR').html('');
        $('#AdmStatusUPR').attr('class', '');
        $('#AdmStatusUPR').attr('role', '');
      },
      complete: function () {
        $('#Status').html('<small>The password for ' + user + ' has been changed.</small>');
        $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
        $('#Status').attr('role', 'alert');
      },
      error: function () {
        $('#ModalStatus').html('<small>There was an error updating the password for ' + user + '</small>');
        $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
        $('#ModalStatus').attr('role', 'alert');
      }
    });
  }
}
// Delete User
function AdmUserDelete(user) {
  if ( isModalOpen() == true ) {
    $.ajax({
      method: 'POST',
      url: './mod/admin.user.delete.php',
      data: 'u=' + user,
      cache: false,
      success: function () {
        hideModal();
        AdmUserList();
      },
      complete: function () {
        $('#Status').html('<small>' + user + ' has been deleted.</small>');
        $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
        $('#Status').attr('role', 'alert');
      },
      error: function () {
        $('#Status').html('<small>There was an error deleteing ' + user + '</small>');
        $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
        $('#Status').attr('role', 'alert');
      }
    });
  } else {
    showModal();
    $('.modal-dialog').attr('class', 'modal-dialog');
    $('#ModalLabel').html('Delete - <span id="AdmDc">' + user + '</span>');
    $('.modal-body').text('Are you sure you want to delete ' + user);
    $('#MYB').attr('class', 'btn btn-primary');
    $('#MYB').text('OK');
    $('#MYB').attr('onclick', 'AdmUserDelete("' + user + '")');
    $('#MNB').attr('class', 'btn btn-outline-secondary');
    $('#MNB').text('Cancel');
    $('#MNB').attr('onClick', 'hideModal()');
  }
}
// Rename User
function AdmUserRename(user) {
  $.ajax({
    method: 'GET',
    url: './mod/modal.admin.user.rename.php',
    data: 'u=' + user,
    cache: false,
    success: function (data) {
      showModal();
      $('.modal-dialog').attr('class', 'modal-dialog');
      $('#ModalLabel').html('Renaming - <span id="AdmRc">' + user + '</span>');
      $('.modal-body').html(data);
      $('#MYB').attr('class', 'btn btn-primary');
      $('#MYB').text('OK');
      $('#MYB').attr('onclick', 'AdmEUrename()');
      $('#MNB').attr('class', 'btn btn-outline-secondary');
      $('#MNB').text('Cancel');
      $('#MNB').attr('onClick', 'hideModal()');
    },
    error: function () {
      $('#Status').html('<small>Error loading rename user module</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
      $('#Status').attr('role', 'alert');
    }
  });
}
function AdmEUrename() {
  var cuser = $('#AdmRc').text();
  var user = $('#AdmRu').val();
  var firstname = $('#AdmRf').val();
  var surname = $('#AdmRs').val();
  var displayname = $('#AdmRd').val();
  $.ajax({
    method: 'POST',
    url: './mod/admin.user.rename.php',
    data: 'cu=' + cuser + '&nu=' + user + '&fn=' + firstname + '&sn=' + surname + '&dn=' + displayname,
    cache: false,
    success: function () {
      hideModal();
      $('#Status').html('<small>' + user + ' has been renamed.</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
      $('#Status').attr('role', 'alert');
      AdmUserList();
    },
    error: function () {
      $('#Status').html('<small>There was an error renaming ' + user + '</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
      $('#Status').attr('role', 'alert');
    }
  });
}
// User Properties
function AdmUserProperties(a) {
  $.ajax({
    method: 'GET',
    url: './mod/admin.user.view.php',
    data: 'u=' + a,
    cache: false,
    success: function (data) {
      $('#AdmUserManViewport').html(data);
    },
    complete: function () {
      $('#Status').html('Viewing properties for <span id="AdmUPcuser">' + a + '</span>');
      $('#Status').attr('class', 'col mx-4');
      $('#Status').attr('role', '');
    },
    error: function () {
      $('#Status').html('<small>There was an error loading properties for ' + a + '</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
      $('#Status').attr('role', 'alert');
    }
  });
}
function AdmUserUpdate() {
  var cuser = $('#AdmUPcuser').text();
  var user = $('#AdmUPuser').val();
  var firstname = $('#AdmUPfn').val();
  var surname = $('#AdmUPsn').val();
  var displayname = $('#AdmUPdn').val();
  var email = $('#AdmUPem').val();
  var dob = $('#AdmUPdob').val();
  var employee_id = $('#AdmUPei').val();
  var status;
  if ($('#AdmUPs').prop('checked')) {
    status = true;
  } else {
    status = false;
  }
  var mustchange;
  if ($('#AdmUPmc').prop('checked')) {
    mustchange = true;
  } else {
    mustchange = false;
  }
  if ( isModalOpen() == true ) {
    $.ajax({
      method: 'POST',
      url: './mod/admin.user.update.php',
      data: 'cu=' + cuser + '&nu=' + user + '&fn=' + firstname + '&sn=' + surname + '&dn=' + displayname + '&em=' + email + '&dob=' + dob + '&ei=' + employee_id + '&l=' + status + '&m=' + mustchange,
      cache: false,
      success: function () {
        hideModal();
        AdmUserList();
      },
      complete: function () {
        $('#Status').html('<small>Properties updated for ' + user + '</small>');
        $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
        $('#Status').attr('role', 'alert');
      },
      error: function () {
        $('#Status').html('<small>There was an error updating properties for ' + cuser + '</small>');
        $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
        $('#Status').attr('role', 'alert');
      }
    });
  } else {
    showModal();
    $('.modal-dialog').attr('class', 'modal-dialog');
    $('#ModalLabel').html('Make Changes - <span id="AdmUPMUser">' + cuser + '</span>');
    var output = 'Are you sure you want to make the following changes to this user?';
    $('.modal-body').text(output);
    $('#MYB').attr('class', 'btn btn-primary');
    $('#MYB').text('OK');
    $('#MYB').attr('onclick', 'AdmUserUpdate()');
    $('#MNB').attr('class', 'btn btn-outline-secondary');
    $('#MNB').text('Cancel');
    $('#MNB').attr('onClick', 'hideModal()');
  }
}
// Debug Info
function AdmUserVarDump(user) {
  $.ajax({
    method: 'GET',
    url: './mod/admin.user.vardump.php',
    data: 'u=' + user,
    cache: false,
    success: function (data) {
      $('#Status').text('Debug Info for ' + user);
      $('#Status').attr('class', 'col mx-4');
      $('#Status').attr('role', '');
      $('#AdmUserManViewport').html(data);
    },
    error: function () {
      $('#Status').html('<small>There was an error loading Debug Info for ' + user + '</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-danger');
      $('#Status').attr('role', 'alert');
    }
  });
}
function AdmUserManShow() {
  $.ajax({
    method: 'GET',
    url: './mod/admin.user.man.html',
    cache: false,
    success: function (data) {
      hideModal();
      AdmUserList();
      AdmReset();
      $('li#AdmLinkUserMan').attr('class', 'nav-item active');
      $('#AdmViewport').html(data);
    }
  });
}
function AdmLoad() {
  NavigationReset();
  $('#Page-Title').text('Admin Tools');
  $.ajax({
    method: 'GET',
    url: './mod/admin.man.html',
    cache: false,
    success: function (data) {
      $('div#Gogglebox').html(data);
      AdmStatsShow();
    }
  });
}
// Bill of Materials
function BOMReset(){
  $('#Status').html('');
  $('#BOMNew').text('New BOM');
  $('#BOMNew').attr('class', 'nav-link');
  $('#BOMNew').attr('onclick', 'BOMNew()');
  $('#BOMShow').text('List BoMs');
  $('#BOMShow').attr('class', 'nav-link');
  $('#BOMShow').attr('onclick', 'BOMShow()');
}
function BOMNBS1V(){
  var Stock = $('#BOMStock').val();
  var Description = $('#BOMDescription').val();
  $('#Step1').html('<table class="table table-hover table-sm nbo1"><tbody><tr><th scope="row">Stock Number</th><td id="nbo1a">' + Stock + '</td></tr><tr><th scope="row">Description</th><td id="nbo1b">' + Description + '</td></tr></tbody></table>');
  if ( Stock.length < 1 ) {
    $('#BOMNBE1').attr('class', 'col alert alert-danger mx-4');
    $('#BOMNBE1').attr('role', 'alert');
    $('#BOMNBE1').text('You need to enter a Stock Number!');
  } else if ( Description.length < 1 ) {
    $('#BOMNBE1').attr('class', 'col alert alert-danger mx-4');
    $('#BOMNBE1').attr('role', 'alert');
    $('#BOMNBE1').text('You need to enter a Description!');
  } else {
    $('#BOMNBE1').attr('class', 'col');
    $('#BOMNBE1').attr('role', '');
    $('#BOMNBE1').text('');
    $('#BOMNBS2T').text(Description + ' (' + Stock + ')');
    nextSlide();
  }
}
function nbs2AddComp(){
  var Stock = $('#Stock').val();
  var nb2c = $('#Desc').val();
  var nb2d = $('#Qty').val();
  var nb2e = $('#CompRef').val();
  var nb2b1;
  var nb2c1;
  var nb2d1;
  if (Stock.length < 1) {
    nb2b1 = false;
  } else {
    nb2b1 = true;
  }
  if (nb2c.length < 1) {
    nb2c1 = false;
  } else {
    nb2c1 = true;
  }
  if (nb2d.length < 1) {
    nb2d1 = false;
  } else {
    nb2d1 = true;
  }
  if (nb2e.length < 1) {
    nb2e = "N/A";
  }
  if ( nb2b1 == false ) {
    $('#nbe2').attr('class', 'col alert alert-danger');
    $('#nbe2').attr('role', 'alert');
    $('#nbe2').text('You haven\'t entered a Stock Code!');
  } else if ( nb2c1 == false ) {
    $('#nbe2').attr('class', 'col alert alert-danger');
    $('#nbe2').attr('role', 'alert');
    $('#nbe2').text('You haven\'t entered a Description for that Stock Code!');
  } else if ( nb2d1 == false ) {
    $('#nbe2').attr('class', 'col alert alert-danger');
    $('#nbe2').attr('role', 'alert');
    $('#nbe2').text('You haven\'t entered a Quantity!');
  } else {
    var nb2a = BOMitem_no++;
    if ( nb2a <= 0 ) {
      nb2a = "1";
    }
    if ( nb2e === "N/A") {
      $('div#nbe2').attr('class', 'col alert alert-warning');
      $('div#nbe2').attr('role', 'alert');
      $('div#nbe2').text('Item Added! But because a Component Reference wasn\'t entered, N/A has been entered for you.');
    } else {
      $('div#nbe2').attr('class', 'col alert alert-success');
      $('div#nbe2').attr('role', 'alert');
      $('div#nbe2').text('Item Added!');
    }
    $('#ComponentList > tbody:last-child').append('<tr><td>' + nb2a + '</td><td>' + Stock + '</td><td>' + nb2c + '</td><td>' + nb2d + '</td><td>' + nb2e + '</td></tr>');
    $('div#nbe2').attr('class', 'col alert alert-success');
    $('div#nbe2').attr('role', 'alert');
    $('div#nbe2').text('Item Added!');
    $('#Stock').val('');
    $('#Desc').val('');
    $('#Qty').val('');
    $('#CompRef').val('');
  }
}
function nbs2RemoveLastComp(){
  $('#ComponentList > tbody > tr:last-of-type').remove();
  var nb2a = BOMitem_no--;
  $('div#nbe2').attr('class', 'col alert alert-warning');
  $('div#nbe2').attr('role', 'alert');
  if ( nb2a <= 1 ) {
    $('div#nbe2').text('You cannot remove what is not there!');
  } else {
    $('div#nbe2').text('Removed last entered Component!');
  }
}
function nbs2Check() {
  var rowCount = $('.nbt > tbody > tr').length;
  if (rowCount === 0){
    $('div#nbe2').attr('class', 'col alert alert-danger');
    $('div#nbe2').attr('role', 'alert');
    $('div#nbe2').text('You haven\'t added any Components to this BoM!');
  } else {
    $('div#nbe2').attr('class', 'col');
    $('div#nbe2').attr('role', '');
    $('div#nbe2').text('');
    nextSlide();
  }
}
function listBoMItems(a) {
  $.ajax({
    method: 'GET',
    url: './mod/bom.list.item.php?bom=' + a,
    cache: false,
    success: function (data) {
      $('#ebi').html(data);
    }
  });
}
function BOMAdd() {
  var nbo1 = new Array($('#nbo1a').text(), $('#nbo1b').text());
  var nbo2 = new Array();
  $.ajax({
    method: 'POST',
    url: './mod/bom.new.php',
    data: 'sc=' + nbo1[0] + '&sd=' + nbo1[1],
    success: function (data) {
      $('table.nbo2 > tbody > tr').each(function() {
        var eachRow = [];
        var tableData = $(this).find('td');
        if ( tableData.length > 0 ) {
          tableData.each(function() {
            eachRow.push($(this).text());
          });
          nbo2.push(eachRow);
        }
        $.ajax({
          method: 'POST',
          url: './mod/bom.new.item.php',
          data: 'bom=' + nbo1[0] + '&in=' + eachRow[0] + '&sc=' + eachRow[1] + '&sd=' + eachRow[2] + '&qty=' + eachRow[3] + '&cr=' + eachRow[4],
          success: function () {
          }
        });
      });
      BOMShow('1', 'Created', nbo1[0]);
    }
  });
}
function ebAddComp() {
  var eba = $('#CCode').text();
  var Stock = $('#Stock').val();
  var ebc = $('#Desc').val();
  var ebd = $('#Qty').val();
  var ebe = $('#CompRef').val();
  var ebf = $('#AddtoList').attr('item_no');
  if (Stock.length < 1) {
    var ebb1 = false;
  } else {
    var ebb1 = true;
  }
  if (ebc.length < 1) {
    var ebc1 = false;
  } else {
    var ebc1 = true;
  }
  if (ebd.length < 1) {
    var ebd1 = false;
  } else {
    var ebd1 = true;
  }
  if (ebe.length < 1) {
    var ebe = "N/A";
  }
  if ( ebb1 === false ) {
    $('div#ebe').attr('class', 'col alert alert-danger');
    $('div#ebe').attr('role', 'alert');
    $('div#ebe').text('You haven\'t entered a Stock Code!');
  } else if ( ebc1 === false ) {
    $('div#ebe').attr('class', 'col alert alert-danger');
    $('div#ebe').attr('role', 'alert');
    $('div#ebe').text('You haven\'t entered a Description!');
  } else if ( ebd1 === false ) {
    $('div#ebe').attr('class', 'col alert alert-danger');
    $('div#ebe').attr('role', 'alert');
    $('div#ebe').text('You haven\'t entered a Quantity!');
  } else {
    ebf++;
    $('#Stock').val('');
    $('#Desc').val('');
    $('#Qty').val('');
    $('#CompRef').val('');
    $('#AddtoList').attr('item_no', ebf);
    $.ajax({
      method: 'POST',
      url: '/mod/bom.new.item.php',
      data: 'bom=' + eba + '&sc=' + Stock + '&sd=' + ebc + '&qty=' + ebd + '&cr=' + ebe + '&in=' + ebf,
      success: function () {
        listBoMItems(eba);
        $('div#ebe').attr('class', 'col alert alert-success');
        $('div#ebe').attr('role', 'alert');
        $('div#ebe').text('Added ' + ebd + 'x ' + ebc + ' to current BoM.');
      }
    });
  }
}
function ebUpdateComp(bomi) {
  var ebc = $('#CCode').text();
  var ebcin = $('#ebcin').val();
  var ebcsc = encodeURIComponent($('#ebcsc').val());
  var ebcsd = $('#ebcsd').val();
  var ebcq = $('#ebcq').val();
  var ebccr = $('#ebccr').val();
  $.ajax({
    method: 'POST',
    url: './mod/bom.update.item.php',
    data: 'id=' + bomi + '&b=' + ebc + '&in=' + ebcin + '&sc=' + ebcsc + '&sd=' + ebcsd + '&qty=' + ebcq + '&cr=' + ebccr,
    success: function () {
      $('#Modal').modal('hide');
      listBoMItems(ebc);
      $('div#ebe').attr('class', 'col alert alert-success');
      $('div#ebe').attr('role', 'alert');
      $('div#ebe').text('Updated Component ' + ebcq + 'x ' + ebcsd + ' to current BoM');
    }
  });
}
function ebDeleteComp(bomi) {
  var ebc = $('#CCode').text();
  var ebdq = $('#ebdq').text();
  var ebdd = $('#ebdd').text();
  $.ajax({
    method: 'POST',
    url: './mod/bom.delete.item.php',
    data: 'id=' + bomi,
    success: function () {
      $('#Modal').modal('hide');
      listBoMItems(ebc);
      $('div#ebe').attr('class', 'col alert alert-warning');
      $('div#ebe').attr('role', 'alert');
      $('div#ebe').text('Deleted Component ' + ebdq + 'x ' + ebdd + ' from current BoM');
    }
  });
}
function BOMNew() {
  $.ajax({
    method: 'GET',
    url: '/mod/wizard.bom.new.html',
    cache: false,
    success: function (data) {
      $('#BoMViewport').html(data);
    },
    complete: function () {
      BOMReset();
      $('#BOMNew').attr('class', 'nav-link active');
      $('#BOMNew').attr('onclick', '');
      $('#Status').text('New BOM Wizard');
    },
    error: function () {
      soft404();
    }
  });
}
function BOMDuplicate(a) {
  var b = $('input#DuplicateTo').val();
  if ( isModalOpen() == true ) {
    // Check if BOM Exists
    $.ajax({
      method: 'POST',
      url: './mod/bom.check.exist.php',
      data: 'a=' + b,
      cache: false,
      success: function (data) {
        var c = data;
        if ( c > 0 ) {
          $('#ModalStatus').html('<small>The BOM Code entered already exists. Please enter another.</small>');
          $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
        } else if ( b.length < 1 ) {
          $('#ModalStatus').html('<small>You must enter a new Bill of Materials</small>');
          $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
        } else {
          // Duplicate the BoM
          $.ajax({
            method: 'POST',
            url: './mod/bom.new.duplicate.php',
            data: 'a=' + a + '&b=' + b,
            cache: false,
            success: function () {
              modalAsLoader();
              $('#modal-footer').attr('class', 'preloader-dialog');
              $('#modal-footer').html('<p class="lead">Duplicating BoM ' + a + ' to  ' + b + '...</p>');
            },
            complete: function () {
              BOMEdit(b);
              modalReset();
              hideModal();
            }
          });
        }
      }
    });
  } else {
    showModal();
    var output = '<form autocomplete="off">\
      <div class="row">\
        <div class="col">\
          <input id="DuplicateTo" name="DuplicateTo" class="form-control form-control-sm" placeholder="New BoM" required>\
          <small id="DuplicateToHelp" class="form-text">\
            You must enter a new BoM code before you can duplicate.\
          </small>\
        </div>\
      </div>\
    </form>';
    $('.modal-dialog').attr('class', 'modal-dialog');
    $('.modal-title').text('Duplicate from BOM (' + a + ')');
    $('.modal-body').html(output);
    $('#MYB').attr('class', 'btn btn-primary');
    $('#MYB').text('Duplicate');
    $('#MYB').attr('onClick', 'BOMDuplicate(\'' + a + '\')');
    $('#MNB').attr('class', 'btn btn-secondary');
    $('#MNB').text('Cancel');
    $('#MNB').attr('onClick', 'hideModal()');
  }
}
function BOMView(a) {
  $.ajax({
    method: 'GET',
    url: '/mod/bom.view.php',
    data: 'bom=' + a,
    cache: false,
    success: function(data){
      $('#BoMViewport').html(data);
    },
    complete: function () {
      BOMReset();
    },
    error: function () {
      soft400();
    }
  });
}
function BOMViewCostBatch() {
  var a = $('h3#BOM').text();
  var b = $('input#Qty').val();
  $.ajax({
    method: 'POST',
    url: '/mod/bom.costing.view.php',
    data: 'a=' + a + '&b=' + b,
    cache: false,
    success: function (data) {
      $('#BoMViewport').html(data);
    },
    complete: function () {
      hideModal();
      BOMReset();
      $('#Status').text('Viewing Costing by Batch');
    },
    error: function () {
      soft400();
    }
  });
}
function BOMBatchCost(a) {
  $.ajax({
    method: 'GET',
    url: '/mod/modal.bom.costing.batch.php',
    data: 'a=' + a,
    cache: false,
    success: function (data) {
      showModal();
      $('#ModalLabel').text('Batch Costing');
      $('#modal-body').html(data);
      $('#MYB').text('Go!');
      $('#MYB').attr('onclick', 'BOMViewCostBatch()');
      $('#MYB').attr('class', 'btn btn-primary');
      $('#MNB').attr('class', 'd-none');
    },
    error: function () {
      soft400();
    }
  });
}
function BOMViewCost(a) {
  $.ajax({
    method: 'POST',
    url: '/mod/bom.costing.view.php',
    data: 'a=' + a,
    cache: false,
    success: function (data) {
      $('#BoMViewport').html(data);
    },
    complete: function () {
      modalReset();
      $('#Modal').modal('hide');
      hideModal();
      BOMReset();
      $('#Status').text('Viewing Costing');
    },
    error: function () {
      soft400();
    }
  });
}
function BOMCost(a) {
  $.ajax({
    method: 'GET',
    url: '/mod/bom.costing.new.php',
    data: 'a=' + a,
    cache: false,
    beforeSend: function () {
      modalAsLoader();
      showModal();
      $('#modal-footer').attr('class', 'preloader-dialog');
      $('#modal-footer').html('<p class="lead">Preparing BoM Costing...</p>');
    },
    success: function () {
      $('body').attr('style', '');
      BOMViewCost(a);
    },
    complete: function () {
      BOMReset();
    },
    error: function () {
      modalReset();
      soft400();
    }
  });
}
function BOMEdit(a) {
  $.ajax({
    method: 'GET',
    url: '/mod/wizard.bom.edit.php',
    data: 'bom=' + a,
    cache: false,
    success: function (data) {
      $('#BoMViewport').html(data);
    },
    complete: function () {
      BOMReset();
      $('#Status').text('Editing BOM');
    },
    error: function () {
      soft400();
    }
  });
}
function BOMSave() {
  var a = $('#CCode').text();
  var b = $('#CName').text();
  var c = $('#NCode').val();
  var d = $('#NName').val();
  if ( isModalOpen() == true ) {
    if (c.length < 1) {
      $('#ModalStatus').text('Stock Code is Empty!');
    } else if (d.length < 1) {
      $('#ModalStatus').text('Description is Empty!');
    } else {
      $.ajax({
        method: 'POST',
        url: '/mod/bom.update.php',
        data: 'bom=' + a + '&sc=' + c + '&sd=' + d,
        success: function () {
          hideModal();
          BOMEdit(c);
        },
        error: function () {
          soft400();
        }
      });
    }
  } else {
    showModal();
    var output = '<p>Are you sure you want to make the following Changes?</p>\
    <h5>Current Info</h5>\
    <table class="table table-sm">\
      <thead>\
        <tr>\
          <th>Stock Code</th>\
          <th>Description</th>\
        </tr>\
      </thead>\
      <tbody>\
        <tr>\
          <td>' + a + '</td>\
          <td>' + b + '</td>\
        </tr>\
      </tbody>\
    </table>\
    <h5>New Information</h5>\
    <table class="table table-sm">\
      <thead>\
        <tr>\
          <th>Stock Code</th>\
          <th>Description</th>\
        </tr>\
      </thead>\
      <tbody>\
        <tr>\
          <td>' + c + '</td>\
          <td>' + d + '</td>\
        </tr>\
        </tbody>\
      </table>';
    $('.modal-dialog').attr('class', 'modal-dialog modal-lg');
    $('.modal-title').text('Save Changes');
    $('.modal-body').html(output);
    $('#MYB').attr('class', 'btn btn-warning');
    $('#MYB').text('Save');
    $('#MYB').attr('onClick', 'BOMSave()');
    $('#MNB').attr('class', 'btn btn-outline-secondary');
    $('#MNB').text('Cancel');
    $('#MNB').attr('onClick', 'hideModal()');
  }
}
function BOMShow(page, info, info_desc){
  if (page == undefined) { var page = 1; }
  $.ajax({
    method: 'GET',
    url: './mod/bom.list.php?p=' + page + '&info=' + info + '&bom=' + info_desc,
    cache: false,
    success: function (data) {
      $('#BoMViewport').html(data);
    },
    complete: function () {
      BOMReset();
      $('#BOMShow').text('Refresh BoMs');
      $('#BOMShow').attr('class', 'nav-link active');
    },
    error: function () {
      soft400();
    }
  });
}
function BOMDelete(a) {
  if ( isModalOpen() == true ) {
    $.ajax({
      method: 'POST',
      url: '/mod/bom.delete.php',
      data: 'bom=' + a,
      success: function () {
        hideModal();
        BOMShow();
      }
    });
  } else {
    showModal();
    var output = '<p>Are you sure you want to Delete the following BoM?</p>\
    <b>' + a + '</b>';
    $('.modal-dialog').attr('class', 'modal-dialog');
    $('.modal-title').text('Delete BOM (' + a + ')');
    $('.modal-body').html(output);
    $('#MYB').attr('class', 'btn btn-danger');
    $('#MYB').text('Delete');
    $('#MYB').attr('onClick', 'BOMDelete(\'' + a + '\')');
    $('#MNB').attr('class', 'btn btn-outline-secondary');
    $('#MNB').text('Cancel');
    $('#MNB').attr('onClick', 'hideModal()');
  }
}
function BOMLoad() {
  $.ajax({
    method: 'GET',
    url: '/mod/bom.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      BOMShow();
      NavigationReset();
      $('a#BOMs').attr('class', 'nav-link active');
      $('#Page-Title').text('Bill of Materials');
      $('#Omnibox').attr('class', 'form-control acrylic low');
      $('#Omnibox').attr('placeholder', 'Search with Code or Description');
      $('#Omnibox').prop('disabled', false);
    },
    error: function () {
      soft404();
    }
  });
}
// Customers
function CustomersReset() {
  $('#Status').text('');
  $('#Status').attr('class', 'col mx-4');
  $('#Status').attr('role', '');
  $('#CustomerNew').text('New Customer');
  $('#CustomerNew').attr('class', 'nav-link');
  $('#CustomerNew').attr('onclick', 'CustomerNew()');
  $('#CustomersShow').text('List Customers');
  $('#CustomersShow').attr('class', 'nav-link');
  $('#CustomersShow').attr('onclick', 'CustomersShow()');
}
function CustomerNew() {
  $.ajax({
    method: 'GET',
    url: './mod/wizard.customer.new.html',
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
      CustomersReset();
    },
    complete: function () {
      $('#CustomerNew').attr('class', 'nav-link active');
      $('#CustomerNew').attr('onclick', '');
      $('#Status').text('New Customer Wizard');
    },
    error: function () {
      soft404();
    }
  });
}
function CustomerNCS1V() {
  var a = $('#CustomerName').val();
  var b = $('#StreetAddress').val();
  var c = $('#Town').val();
  var d = $('#Region').val();
  var e = $('#PostalCode').val();
  var f = $('#TelNo').val();
  if ( a.length < 1 ) {
    $('#CustomerNCE1').text('Customer Name cannot be blank.');
    $('#CustomerNCE1').attr('class', 'col alert alert-danger mx-4');
    $('#CustomerNCE1').attr('role', 'alert');
  } else if ( b.length < 1 ) {
    $('#CustomerNCE1').text('Street Address cannot be blank.');
    $('#CustomerNCE1').attr('class', 'col alert alert-danger mx-4');
    $('#CustomerNCE1').attr('role', 'alert');
  } else if ( c.length < 1 ) {
    $('#CustomerNCE1').text('Town cannot be blank.');
    $('#CustomerNCE1').attr('class', 'col alert alert-danger mx-4');
    $('#CustomerNCE1').attr('role', 'alert');
  } else if ( d.length < 1 ) {
    $('#CustomerNCE1').text('Region cannot be blank.');
    $('#CustomerNCE1').attr('class', 'col alert alert-danger mx-4');
    $('#CustomerNCE1').attr('role', 'alert');
  } else if ( e.length < 1 ) {
    $('#CustomerNCE1').text('Postal Code cannot be blank.');
    $('#CustomerNCE1').attr('class', 'col alert alert-danger mx-4');
    $('#CustomerNCE1').attr('role', 'alert');
  } else if ( f.length < 1 ) {
    $('#CustomerNCE1').text('Telephone Number cannot be blank.');
    $('#CustomerNCE1').attr('class', 'col alert alert-danger mx-4');
    $('#CustomerNCE1').attr('role', 'alert');
  } else {
    var output = '<table class="table table-sm">\
      <tbody>\
        <tr>\
          <th>Customer Name</th>\
          <td>' + a + '</td>\
        </tr>\
        <tr>\
          <th>Address</th>\
          <td>' + b + ', ' + c + ', ' + d + ', ' + e + '</td>\
        </tr>\
        <tr>\
          <th>Telephone Number</th>\
          <td>' + f + '</td>\
        </tr>\
      </tbody>\
    </table>';
    $('#CustomerNCS1V').html(output);
    nextSlide();
  }
}
function CustomerAdd() {
  var a = $('#CustomerName').val();
  var b = $('#StreetAddress').val();
  var c = $('#Town').val();
  var d = $('#Region').val();
  var e = $('#PostalCode').val();
  var f = $('#TelNo').val();
  $.ajax({
    method: 'POST',
    url: './mod/customer.new.php',
    data: 'cn=' + a + '&sa=' + b + '&t=' + c + '&r=' + d + '&pc=' + e + '&tn=' + f,
    success: function () {
      CustomersShow();
    },
    complete: function () {
      $('#Status').html('<small>' + a + ' has been deleted.</small>');
      $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
      $('#Status').attr('role', 'alert');
    },
    error: function () {
      soft400();
    }
  });
}
function CustomerDelete(a, b) {
  if ( isModalOpen() == true ) {
    $.ajax({
      method: 'POST',
      url: './mod/customer.delete.php',
      data: 'ci=' + a,
      cache: false,
      success: function () {
        hideModal();
        CustomersShow();
      },
      complete: function () {
        $('#Status').html('<small>' + b + ' has been deleted.</small>');
        $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-success');
        $('#Status').attr('role', 'alert');
      },
      error: function () {
        soft400();
      }
    });
  } else {
    showModal();
    $('.modal-dialog').attr('class', 'modal-dialog');
    $('#ModalLabel').html('Delete - <span id="CustomerDc">' + b + '</span>');
    $('.modal-body').text('Are you sure you want to delete ' + b);
    $('#MYB').attr('class', 'btn btn-primary');
    $('#MYB').text('OK');
    $('#MYB').attr('onclick', 'CustomerDelete("' + a + '", "' + b + '")');
    $('#MNB').attr('class', 'btn btn-outline-secondary');
    $('#MNB').text('Cancel');
  }
}
function CustomerEdit(a, b) {
  $.ajax({
    method: 'GET',
    url: './mod/wizard.customer.edit.php',
    data: 'ci=' + a,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
      CustomersReset();
    },
    complete: function () {
      $('#Status').html('Editing customer <span id="Customercname">' + b + '</span>');
      $('#Status').attr('class', 'col mx-4');
      $('#Status').attr('role', '');
    },
    error: function () {
      soft404();
    }
  });
}
function CustomerUpdate(h) {
  var g = $('#Customercname').text();
  var a = $('#CustomerName').val();
  var b = $('#StreetAddress').val();
  var c = $('#Town').val();
  var d = $('#Region').val();
  var e = $('#PostalCode').val();
  var f = $('#TelNo').val();
  // Validation
  if ( isModalOpen() == true ) {
    $.ajax({
      method: 'POST',
      url: './mod/customer.update.php',
      data: 'on=' + g + '&cn=' + a + '&sa=' + b + '&to=' + c + '&re=' + d + '&pc=' + e + '&tn=' + f,
      success: function () {
        hideModal();
      },
      complete: function () {
        CustomersShow();
      },
      error: function () {
        soft400();
      }
    });
  } else {
    var output = 'Are you sure you want to save the following changes to ' + g + '?\
    <table class="table table-sm">\
      <thead>\
        <tr>\
          <th>Preview</th>\
        </tr>\
      </thead>\
      <tbody>\
        <tr>\
          <th>Customer Name</th>\
          <td>' + a + '</td>\
        </tr>\
        <tr>\
          <th>Street Address</th>\
          <td>' + b + '</td>\
        </tr>\
        <tr>\
          <th>Town</th>\
          <td>' + c + '</td>\
        </tr>\
        <tr>\
          <th>Region</th>\
          <td>' + d + '</td>\
        </tr>\
        <tr>\
          <th>Postal Code</th>\
          <td>' + e + '</td>\
        </tr>\
        <tr>\
          <th>Telephone Number</th>\
          <td>' + f + '</td>\
        </tr>\
      </tbody>\
    </table>';
    showModal();
    $('.modal-dialog').attr('class', 'modal-dialog modal-lg');
    $('#ModalLabel').html('Make Changes - <span id="CustomerDc">' + g + '</span>');
    $('.modal-body').html(output);
    $('#MYB').attr('class', 'btn btn-primary');
    $('#MYB').text('OK');
    $('#MYB').attr('onclick', 'CustomerUpdate("' + h + '")');
    $('#MNB').attr('class', 'btn btn-outline-secondary');
    $('#MNB').text('Cancel');
  }
}
function CustomerView(a, b) {
  $.ajax({
    method: 'GET',
    url: './mod/customer.view.php',
    data: 'ci=' + a,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
      CustomersReset();
    },
    complete: function () {
      $('#Status').html('Viewing customer <span id="Customercname">' + b + '</span>');
      $('#Status').attr('class', 'col mx-4');
      $('#Status').attr('role', '');
    },
    error: function () {
      soft404();
    }
  });
}
function CustomersShow(page, info) {
  if (page == undefined) {
    page = 1;
  }
  $.ajax({
    method: 'GET',
    url: './mod/customer.list.php',
    data: 'info=' + info + '&p=' + page,
    cache: false,
    success: function (data) {
      CustomersReset();
      $('#Viewport').html(data);
      $('#CustomersShow').text('Refresh Customers');
      $('#CustomersShow').attr('class', 'nav-link active');
    }
  });
}
function CustomersLoad() {
  NavigationReset();
  $('a#Cust').attr('class', 'nav-link active');
  $('#Page-Title').text('Customers');
  $.ajax({
    method: 'GET',
    url: './mod/customers.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      CustomersShow();
    },
    error: function () {
      soft404();
    }
  });
}
// Goods
function GoodsReset() {
  $('#NewGoodsIn').text('Incoming');
  $('#NewGoodsIn').attr('class', 'nav-link disabled');
  //$('#NewGoodsIn').attr('onclick', 'newGoodsIn()');
  $('#NewGoodsOut').text('Outgoing');
  $('#NewGoodsOut').attr('class', 'nav-link disabled');
  //$('#NewGoodsOut').attr('onclick', 'newGoodsOut()');
  $('#ListGoodsIn').text('Incoming');
  $('#ListGoodsIn').attr('class', 'nav-link');
  $('#ListGoodsIn').attr('onclick', 'listGoods(\'1\', \'In\')');
  $('#ListGoodsOut').text('Outgoing');
  $('#ListGoodsOut').attr('class', 'nav-link disabled');
  //$('#ListGoodsOut').attr('onclick', 'listGoods(\'1\', \'Out\')');
}
function listGoods(page, cat){
  if (page == undefined) { var page = 1; }
  $.ajax({
    method: 'GET',
    url: './mod/goods.list.php',
    data: 'p=' + page,
    cache: false,
    success: function(data){
      $('#Viewport').html(data);
    }
  });
  GoodsReset();
  if (cat === 'In') {
    $('#ListGoodsIn').text('Refresh Goods');
    $('#ListGoodsIn').attr('class', 'nav-link active');
  }
}
function ViewGoods(a) {
  $.ajax({
    method: 'GET',
    url: './mod/goods.view.php',
    data: 'gdi=' + a,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
      GoodsReset();
    },
    complete: function () {
      $('#Status').html('Viewing GRN <span id="GRNNumber">' + a + '</span>');
      $('#Status').attr('class', 'col mx-4');
      $('#Status').attr('role', '');
    },
    error: function () {
      soft400();
    }
  });
}
function GoodsLoad() {
  $.ajax({
    method: 'GET',
    url: './mod/goods.man.html',
    cache: false,
    success: function(data){
      $('#Gogglebox').html(data);
    },
    complete: function () {
      NavigationReset();
      listGoods('1', 'In');
      $('a#Goods').attr('class', 'nav-link active');
      $('#Page-Title').text('Deliveries');
    },
    error: function () {
      soft400();
    }
  });
}
// Jobs
function JobsReset() {
  $('#Status').text('');
  $('#Status').attr('class', 'col mx-4');
  $('#Status').attr('role', '');
  $('a#NewJob').text('New Job');
  $('a#NewJob').attr('class', 'nav-link');
  $('a#NewJob').attr('onclick', 'JobsNew()');
  $('a#ActiveJobs').text('Active Jobs');
  $('a#ActiveJobs').attr('class', 'nav-link');
  $('a#CompletedJobs').text('Completed Jobs');
  $('a#CompletedJobs').attr('class', 'nav-link');
  $('a#HeldJobs').text('Jobs On Hold');
  $('a#HeldJobs').attr('class', 'nav-link');
}
function resetButtons() {
  JobsReset();
}
function JobsShow(page, cat, info) {
  if (page == undefined) { var page = 1; }
  $.ajax({
    method: 'GET',
    url: './mod/job.list.php?p=' + page + '&cat=' + cat + '&info=' + info,
    cache: false,
    success: function (data) {
      $('div#JobsManViewport').html(data);
    }
  });
  JobsReset();
  if (cat === 'Active') {
    $('a#ActiveJobs').text('Refresh Jobs');
    $('a#ActiveJobs').attr('class', 'nav-link active');
  }
  if (cat === 'Completed') {
    $('a#CompletedJobs').text('Refresh Jobs');
    $('a#CompletedJobs').attr('class', 'nav-link active');
  }
  if (cat === 'Held') {
    $('a#HeldJobs').text('Refresh Jobs');
    $('a#HeldJobs').attr('class', 'nav-link active');
  }
}
function listJobItems(job) {
    $.ajax({
        method: 'GET',
        url: './mod/job.list.item.php',
        data: 'nu=' + job,
        cache: false,
        success: function (data) {
            $('div#ejs2').html(data);
        }
    });
}
function Step1toOverview() {
  var nj1a = $('#Job_NO').val();
  var nj1b = $('#Customer').val();
  var nj1c = $('#Comments').val();
  if ( nj1c.length < 1 ) {
    nj1c = "No Comments";
  }
  var nj1d = $('#Notes').val();
  if ( nj1d.length < 1 ) {
    nj1d = "No Notes";
  }
  if ($('#Customer').val().length > 0) {
    $('div#nje1').attr('class', 'col');
    $('div#nje1').attr('role', '');
    $('div#nje1').text('');
    var output = '<table class="table table-hover table-sm njs3t1">\
      <tbody>\
        <tr>\
          <th scope="row">Job No#</th>\
          <td id="njs3t1a">' + nj1a + '</td>\
        </tr>\
        <tr>\
          <th scope="row">Customer</th>\
          <td id="njs3t1b">' + nj1b + '</td>\
        </tr>\
        <tr>\
          <th scope="row">Comments</th>\
          <td id="njs3t1c">' + nj1c + '</td>\
        </tr>\
        <tr>\
          <th scope="row">Additional Notes</th>\
          <td id="njs3t1d">' + nj1d + '</td>\
        </tr>\
      </tbody>\
    </table>';
    $('div#Step1').html(output);
    nextSlide();
  } else {
    $('div#nje1').attr('class', 'col alert alert-danger mx-4');
    $('div#nje1').attr('role', 'alert');
    $('div#nje1').text('You haven\'t entered a Customer!');
  }
}
function Step2AddProduct() {
    var nj2a = $('#Product').val();
    var nj2b = $('#Qty').val();
    var nj2a1;
    var nj2b1;
    if (nj2a.length < 1) {
      nj2a1 = false;
    } else {
      nj2a1 = true;
    }
    if (nj2b.length < 1) {
      nj2b1 = false;
    } else {
      nj2b1 = true;
    }
    if ( nj2a1 === false ) {
        $('div#nje2').attr('class', 'col alert alert-danger');
        $('div#nje2').attr('role', 'alert');
        $('div#nje2').text('You haven\'t entered a Product!');
    } else if ( nj2b1 === false ) {
        $('div#nje2').attr('class', 'col alert alert-danger');
        $('div#nje2').attr('role', 'alert');
        $('div#nje2').text('You haven\'t entered a Quantity!');
    } else {
        $('#ProductList > tbody:last-child').append('<tr><td>' + nj2a + '</td><td>' + nj2b + '</td></tr>');
        $('div#nje2').attr('class', 'col');
        $('div#nje2').attr('role', '');
        $('div#nje2').text('');
        $('#Product').val(''); $('#Qty').val('');
    }
}
function Step2RemoveProduct() {
  $('#ProductList > tbody > tr:last-of-type').remove();
  $('div#nje2').attr('class', 'col alert alert-warning');
  $('div#nje2').attr('role', 'alert');
  $('div#nje2').text('Removed last entered product!');
}
function Step2CheckProducts() {
    var rowCount = $('.njs2t > tbody > tr').length;
    if (rowCount === 0){
        $('div#nje2').attr('class', 'col alert alert-danger');
        $('div#nje2').attr('role', 'alert');
        $('div#nje2').text('You haven\'t added any Products to this Job!');
    } else {
        $('div#nje2').attr('class', 'col');
        $('div#nje2').attr('role', '');
        $('div#nje2').text('');
        nextSlide();
    }
}
function JobsNew() {
  $.ajax({
    method: 'GET',
    url: './mod/wizard.job.new.php',
    cache: false,
    success: function (data) {
      $('div#JobsManViewport').html(data);
    }
  });
  JobsReset();
  $('a#NewJob').attr('class', 'nav-link active');
  $('a#NewJob').attr('onclick', '');
}
function addJob() {
  var njs3t1 = new Array($('#njs3t1a').text(), $('#njs3t1b').text(), $('#njs3t1c').text(), $('#njs3t1d').text());
  var njs3t2 = new Array();
  $.ajax({
    method: 'POST',
    url: './mod/job.new.php',
    data: 'nu=' + njs3t1[0] + '&cu=' + njs3t1[1] + '&co=' + njs3t1[2] + '&no=' + njs3t1[3],
    success: function (data) {
      $('table.njs3t2 > tbody > tr').each(function() {
        var eachRow = [];
        var tableData = $(this).find('td');
        if (tableData.length > 0) {
          tableData.each(function() { eachRow.push($(this).text()); });
          njs3t2.push(eachRow);
        }
        $.ajax({
          method: 'POST',
          url: './mod/job.new.item.php',
          data: 'n=' + njs3t1[0] + '&p=' + eachRow[0] + '&q=' + eachRow[1],
          success: function () {
          }
        });
      });
      JobsShow('1', 'Active', 'Created' + njs3t1[0]);
    }
  });
}
function viewJob(jn) {
  $.ajax({
    method: 'GET',
    url: './mod/job.view.php',
    data: 'job=' + jn,
    cache: false,
    success: function (data) {
      $('div#JobsManViewport').html(data);
    }
  });
  JobsReset();
}
function JobsView(a) {
  $.ajax({
    method: 'GET',
    url: './mod/job.view.php',
    data: 'job=' + a,
    cache: false,
    success: function (data) {
      $('div#JobsManViewport').html(data);
      $('#Viewport').html(data);
    },
    complete: function () {
      $('nav').hide();
      setTimeout(function() {
          $('nav').show();
      }, 0);
    }
  });
  JobsReset();
}
function deleteJob(a) {
  $.ajax({
    method: 'POST',
    url: './mod/job.delete.php',
    data: 'nu=' + a,
    success: function () {
      hideModal();
      JobsShow('1', 'Active', 'Deleted' + a)
    }
  });
}
function holdJob(a) {
  $.ajax({
    method: 'POST',
    url: './mod/job.markas.php',
    data: 'x=Hold&nu=' + a,
    success: function () {
      hideModal();
      JobsShow('1', 'Held', 'MarkonH' + a)
    }
  });
}
function activateJob(a) {
  $.ajax({
    method: 'POST',
    url: './mod/job.markas.php',
    data: 'x=Active&nu=' + a,
    success: function () {
      hideModal();
      JobsShow('1', 'Active', 'MarkasA' + a)
    }
  });
}
function completeJob(a) {
  $.ajax({
    method: 'POST',
    url: './mod/job.markas.php',
    data: 'x=Complete&nu=' + a,
    success: function () {
      hideModal();
      JobsShow('1', 'Completed', 'MarkasC' + a)
    }
  });
}
function editJob(jn, info) {
  if ( info === "Yes! Fam!") {
      var x = "Change";
  } else { //Do Nothing
  }
  $.ajax({
    method: 'GET',
    url: './mod/wizard.job.edit.php',
    data: 'job=' + jn + '&x=' + x,
    cache: false,
    success: function (data) {
      $('div#JobsManViewport').html(data);
    }
  });
  resetButtons();
}
function JobsEdit(a, b) {
  var c;
  if ( b === "Yes! Fam!") {
    c = "Change";
  } else {
    c;
  }
  $.ajax({
    method: 'GET',
    url: './mod/wizard.job.edit.php',
    data: 'job=' + a + '&x=' + c,
    cache: false,
    success: function (data) {
      $('#JobsManViewport').html(data);
      $('#Viewport').html(data);
    },
    complete: function () {
      $('nav').hide();
      setTimeout(function() {
          $('nav').show();
      }, 0);
    }
  });
  JobsReset();
}
function editDeleteitem(a, b, c, d) {
  $.ajax({
    method: 'POST',
    url: './mod/job.delete.item.php',
    data: 'nu=' + a,
    success: function () {
      hideModal();
      listJobItems('w', b);
      $('div#eaie').attr('class', 'col alert alert-danger');
      $('div#eaie').attr('role', 'alert');
      $('div#eaie').text('Removed ' + c + 'x ' + d);
    }
  });
}
function editSaveChanges() {
  var ej0 = $('#job_no').text();
  var ej1 = $('#Status').val();
  var ej2 = $('#Customer').val();
  var ej3 = $('#Comments').val();
  if ( ej3.length < 1 ) {
    var ej3 = "No Comments";
  }
  var ej4 = $('#Notes').val();
  if ( ej4.length < 1 ) {
    var ej4 = "No Notes";
  }
  $.ajax({
    method: 'POST',
    url: './mod/job.update.php',
    data: 'nu=' + ej0 + '&st=' + ej1 + '&cu=' + ej2 + '&co=' + ej3 + '&no=' + ej4,
    success: function () {
      hideModal();
      editJob(ej0, "Yes! Fam!");
    }
  });
}
function editAdditem() {
  var ejai0 = $('#job_no').text();
  var ejai1 = $('#Product').val();
  var ejai2 = $('#Qty').val();
  var ejai1a;
  var ejai2a;
  if (ejai1.length < 1) {
    ejai1a = false;
  } else {
    ejai1a = true;
  }
  if (ejai2.length < 1) {
    ejai2a = false;
  } else {
    ejai2a = true;
  }
  if ( ejai1a === false ) {
    $('#eaie').attr('class', 'col alert alert-danger');
    $('#eaie').attr('role', 'alert');
    $('#eaie').text('You haven\'t entered a Product!');
  } else if ( ejai2a === false ) {
    $('#eaie').attr('class', 'col alert alert-danger');
    $('#eaie').attr('role', 'alert');
    $('#eaie').text('You haven\'t entered a Quantity!');
  } else {
    $('#Product').val('');
    $('#Qty').val('');
    $.ajax({
      method: 'POST',
      url: './mod/job.new.item.php',
      data: 'n=' + ejai0 + '&p=' + ejai1 + '&q=' + ejai2,
      success: function () {
        listJobItems('w', ejai0);
        $('div#eaie').attr('class', 'col alert alert-success');
        $('div#eaie').attr('role', 'alert');
        $('div#eaie').text('Added ' + ejai2 + 'x ' + ejai1 + ' to current Job.');
      }
    });
  }
}
function editExistingitem(a) {
  var b = $('#mei1').val();
  var c = $('#mei2').val();
  var d = $('#job_no').text();
  $.ajax({
    method: 'POST',
    url: './mod/job.update.item.php',
    data: 'nu=' + a + '&pr=' + b + '&qty=' + c,
    success: function () {
      hideModal();
      listJobItems(d);
      $('#eaie').attr('class', 'col alert alert-success');
      $('#eaie').attr('role', 'alert');
      $('#eaie').text('Updated Item to ' + c + 'x ' + b);
    }
  });
}
function JobsLoad() {
  $('a#Jobs').attr('class', 'nav-link active');
  $.ajax({
    method: 'GET',
    url: './mod/job.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      JobsShow('1', 'Active');
      NavigationReset();
      $('a#Jobs').attr('class', 'nav-link active');
      $('#Page-Title').text('Jobs');
    },
    error: function () {
      soft404();
    }
  });
}
// Purchasing
function PurchasingReset() {
  $('#Status').text('');
  $('#Status').attr('class', 'col mx-4');
  $('#Status').attr('role', '');
  $('#PurchaseOrdersNew').text('New Purchase Order');
  $('#PurchaseOrdersNew').attr('class', 'nav-link');
  $('#PurchaseOrdersNew').attr('onclick', 'PurchaseOrdersNew()');
  $('#PurchaseOrdersShow').text('Show Purchase Orders');
  $('#PurchaseOrdersShow').attr('class', 'nav-link');
  $('#PurchaseOrdersShow').attr('onclick', 'PurchaseOrdersShow()');
}
function PurchaseOrdersView(a) {
  $.ajax({
    method: 'GET',
    url: './mod/purchasing.order.view.php',
    data: 'a=' + a,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    complete: function () {
      var b = $('#PurchaseOrderNumber').text();
      $('#Status').text('Viewing Purchase Order ' + b);
    },
    error: function () {
      soft400();
    }
  });
}
function PurchaseOrdersNew() {
  $.ajax({
    method: 'GET',
    url: './mod/wizard.purchasing.order.new.html',
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
      PurchasingReset();
    },
    complete: function () {
      $('#Status').text('New Purchase Order');
      $('#PurchaseOrdersNew').attr('class', 'nav-link active');
      $('#PurchaseOrdersNew').attr('onclick', '');
    },
    error: function () {
      soft404();
    }
  });
}
function PurchaseOrdersShow(page) {
  if (page == undefined) {
    page = 1;
  }
  $.ajax({
    method: 'GET',
    url: '/mod/purchasing.order.list.php',
    data: 'page=' + page,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    complete: function () {
      PurchasingReset();
      $('#PurchaseOrdersShow').attr('class', 'nav-link active');
      $('#PurchaseOrdersShow').text('Refresh Purchase Orders');
    },
    error: function () {
      soft400();
    }
  });
}
function PurchaseOrdersVardump(a) {
  $.ajax({
    method: 'GET',
    url: './mod/purchasing.order.vardump.php',
    data: 'a=' + a,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
      PurchasingReset();
    },
    complete: function () {
      var b = $('#PurchaseOrderNumber').text();
      $('#Status').text('Viewing VarDump for Purchase Order ' + b);
    },
    error: function () {
      soft400();
    }
  });
}
function PurchasingLoad() {
  $.ajax({
    method: 'GET',
    url: './mod/purchasing.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      NavigationReset();
      PurchaseOrdersShow();
      $('a#Purchasing').attr('class', 'nav-link active');
      $('#Page-Title').text('Purchasing');
    },
    error: function () {
      soft404();
    }
  });
}
// Scratchpad
function ScratchpadLoad() {
  $.ajax({
    method: 'GET',
    url: './mod/scratchpad.php',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      NavigationReset();
      //      SuppliersShow();
      $('#Page-Title').text('Scratchpad');
    },
    error: function () {
      soft400();
    }
  });
}
// Settings
// function SettingsReset() {
// }
function SettingsAboutMe() {
  $.ajax({
    method: 'GET',
    url: '/mod/settings.aboutme.php',
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    error: function () {
      soft404();
    }
  });
}
function SettingsChangePassword() {
  showModal();
  $('#modal-dialog').attr('class', 'modal-dialog');
  $('#ModalLabel').html('Change Password');
  var output = '<form autocomplete="off">\
    <div class="form-group row">\
      <label class="col-sm-4 col-form-label" for="SettingsChangePasswordOld">Old password</label>\
      <div class="col-sm-8">\
        <input type="password" id="SettingsChangePasswordOld" name="SettingsChangePasswordOld" class="form-control form-control-sm" required>\
      </div>\
    </div>\
    <div class="form-group row">\
      <label class="col-sm-4 col-form-label" for="SettingsChangePasswordNew">New password</label>\
      <div class="col-sm-8">\
        <input type="password" id="SettingsChangePasswordNew" name="SettingsChangePasswordNew" class="form-control form-control-sm" required>\
      </div>\
    </div>\
    <div class="form-group row">\
      <label class="col-sm-4 col-form-label" for="SettingsChangePasswordConfirm">Confirm password</label>\
      <div class="col-sm-8">\
        <input type="password" id="SettingsChangePasswordConfirm" name="SettingsChangePasswordConfirm" class="form-control form-control-sm" required>\
      </div>\
    </div>\
  </form>';
  $('#modal-body').html(output);
  $('#MYB').attr('class', 'btn btn-primary');
  $('#MYB').text('OK');
  $('#MYB').attr('onclick', 'SettingsUpdatePassword()');
  $('#MNB').attr('class', 'btn btn-outline-secondary');
  $('#MNB').text('Cancel');
  $('#MNB').attr('onclick', 'hideModal()');
}
function SettingsUpdatePassword() {
  var pass = $('#SettingsChangePasswordOld').val();
  var npass = $('#SettingsChangePasswordNew').val();
  var cpass = $('#SettingsChangePasswordConfirm').val();
  if ( pass.length < 1 || npass.length < 1 || cpass.length < 1 ) {
    $('#ModalStatus').html('<small>Password cannot be blank</small>');
    $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
  } else if ( pass.length < 4 || npass.length < 4 || cpass.length < 4 ) {
    $('#ModalStatus').html('<small>Password cannot be too short</small>');
    $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
  } else if ( ComparePasswords(npass, cpass) == false ) {
    $('#ModalStatus').html('<small>New passwords mismatch</small>');
    $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
  } else if ( ComparePasswords(pass, npass) == true ) {
    $('#ModalStatus').html('<small>New password cannot match old password</small>');
    $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-danger');
  } else {
    $.ajax({
      method: 'POST',
      url: '/mod/settings.update.password.php',
      data: 'pass=' + pass + '&npass=' + npass + '&cpass=' + cpass,
      cache: false,
      success: function (data) {
        if (data == "WrongPass") {
          $('#ModalStatus').html('<small>The password entered is wrong</small>');
          $('#ModalStatus').attr('class', 'col my-0 px-2 py-1 alert alert-info');
        } else if (data == "UpdatePass") {
          $('#Status').html('<small>Password has been set</small>');
          $('#Status').attr('class', 'col my-0 mx-4 px-2 py-0 alert alert-info');
          hideModal();
        }
      },
      complete: function () {
      },
      error: function () {
        soft400();
      }
    });
  }
}
function SettingsLoad() {
  NavigationReset();
  $('#Page-Title').text('Settings');
  $.ajax({
    method: 'GET',
    url: '/mod/settings.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      SettingsAboutMe();
    },
    error: function () {
      soft404();
    }
  });
}
// Stock
function StockReset() {
  $('#stb1').text('Refresh Stock');
  $('#stb1').attr('class', 'nav-link active');
  $('#stb1').attr('onclick', 'listStock(\'1\')');
}
function listStock(page) {
  if (page == undefined) { var page = 1; }
  $.ajax({
    method: 'GET',
    url: '/mod/stock.list.php',
    cache: false,
    data: 'p=' + page,
    success: function (data) {
      $('#Viewport').html(data);
    }
  });
  StockReset();
}
function viewStock(id) {
  $.ajax({
    method: 'GET',
    url: '/mod/stock.item.view.php',
    data: 'id=' + id,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    }
  });
  $('#stb1').text('Back to Stock...');
  $('#stb1').attr('class', 'nav-link');
}
function viewStockVarDump(id) {
  $.ajax({
    method: 'GET',
    url: './mod/stock.item.vardump.php',
    data: 'id=' + id,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    }
  });
  $('#stb1').text('Back to Stock...');
  $('#stb1').attr('class', 'nav-link');
}
function getFilters() {
  $.ajax({
    method: 'GET',
    url: './mod/stock.filter.php',
    cache: false,
    success: function (data) {
      $('#filters').html(data);
    }
  });
}
function filterCat(){
  var cat = $('select#Category').val();
  $.ajax({
    method: 'POST',
    url: './mod/stock.filter.php',
    data: 'c=' + cat,
    cache: false,
    success: function (data) {
      $('#filters').html(data);
    }
  });
  $.ajax({
    method: 'GET',
    url: './mod/stock.list.search.php',
    data: 'cat=' + cat,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    }
  });
  $('#stb1').text('Reset Filters...');
  $('#stb1').attr('class','nav-link');
}
function StockLoad() {
  $.ajax({
    method: 'GET',
    url: './mod/stock.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
      StockReset();
    },
    complete: function () {
      NavigationReset();
      $('a#NavStock').attr('class', 'nav-link active');
      $('#Page-Title').text('Stock');
      listStock();
      getFilters();
      $('#Omnibox').attr('class', 'form-control acrylic low');
      $('#Omnibox').attr('placeholder', 'Search by Code/Description, Category or Marking...');
      $('#Omnibox').prop('disabled', false);
    },
    error: function () {
      soft400();
    }
  });
}
// Stores
function StoresOpenBox(a, b) {
  $.ajax({
    method: 'GET',
    url: './mod/stores.view.store.php',
    data: 'a=' + a + '&b=' + b,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    error: function () {
      soft404();
    }
  });
}
function StoresOpenLocation(a) {
  $.ajax({
    method: 'GET',
    url: './mod/stores.list.php',
    data: 'a=bins&b=' + a,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    complete: function () {
      $('#storesscroll').scrollspy({ target: '#stores-navscroll' });
    },
    error: function () {
      soft400();
    }
  });
}
function StoresLoad() {
  NavigationReset();
  $('a#Stores').attr('class', 'nav-link active');
  $('#Page-Title').text('Stores');
  $.ajax({
    method: 'GET',
    url: './mod/stores.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      $.ajax({
        method: 'GET',
        url: './mod/stores.list.php',
        cache: false,
        success: function (data) {
          $('#Viewport').html(data);
        },
        error: function () {
          soft400();
        }
      });
    },
    error: function () {
      soft400();
    }
  });
}
// Suppliers
function SuppliersView(Sup) {
  $.ajax({
    method: 'GET',
    url: './mod/suppliers.view.php',
    data: 'Sup=' + Sup,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    error: function () {
      soft404();
    }
  });
}
function SuppliersShow() {
  $.ajax({
    method: 'GET',
    url: './mod/suppliers.list.php',
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    error: function () {
      soft404();
    }
  });
}
function SuppliersLoad() {
  $.ajax({
    method: 'GET',
    url: './mod/suppliers.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      NavigationReset();
      SuppliersShow();
      $('a#Suppliers').attr('class', 'nav-link active');
      $('#Page-Title').text('Suppliers');
    },
    error: function () {
      soft404();
    }
  });
}
// Timesheet
function TimesheetLoad() {
  $.ajax({
    method: 'GET',
    url: './mod/timesheet.php',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      NavigationReset();
      //      SuppliersShow();
      $('#Page-Title').text('Timesheet');
    },
    error: function () {
      soft400();
    }
  });
}
// Works Orders
function WorksReset() {
  $('#Status').text('');
  $('#Status').attr('class', 'col mx-4');
  $('#Status').attr('role', '');
  $('#WorksNew').text('New Works Order');
  $('#WorksNew').attr('class', 'nav-link');
  $('#WorksNew').attr('onclick', 'WorksNew(\'Job\')');
}
function WorksNew(cat) {
  $.ajax({
    method: 'GET',
    url: '/mod/wizard.wo.new.php',
    data: 'cat=' + cat,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    complete: function () {
      $('#Status').text('New Works Order Wizard');
      $('#WorksNew').attr('class', 'nav-link active');
      $('#WorksNew').attr('onclick', '');
    },
    error: function () {
      soft400();
    }
  });
}
function WorksShow(a) {
  if (a == undefined) {
    a = 1;
  }
  $.ajax({
    method: 'GET',
    url: '/mod/wo.list.php',
    data: 'p=' + a,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    complete: function () {
      WorksReset();
    },
    error: function () {
      soft400();
    }
  });
}
function WorksView(wo) {
  $.ajax({
    method: 'GET',
    url: '/mod/wo.view.php',
    data: 'wo=' + wo,
    cache: false,
    success: function (data) {
      $('#Viewport').html(data);
    },
    error: function () {
      soft400();
    }
  });
}
function WorksLoad() {
  $.ajax({
    method: 'GET',
    url: '/mod/wo.man.html',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      NavigationReset();
      WorksShow();
      $('a#WO').attr('class', 'nav-link active');
      $('#Page-Title').text('Works Orders');
    },
    error: function () {
      soft404();
    }
  });
}
// Index
function DoStart() {
  $('#version').text(version);
  $.ajax({
    method: 'GET',
    url: '/core/welcome.php',
    cache: false,
    success: function (data) {
      $('#Gogglebox').html(data);
    },
    complete: function () {
      NavigationReset();
      $('a#Start').attr('class', 'nav-link active');
      $('#Page-Title').text('Start');
    },
    error: function () {
      soft404();
    }
  });
}
// Login
function DoLogin() {
  var a = $("#inputUser").val();
  var b = $("#inputPassword").val();
  $('#loginStatus').attr('role', 'alert');
  if ( a.length < 1 ) {
    $('#loginStatus').html('Username cannot be Blank!');
    $('#loginStatus').attr('class', 'alert alert-danger');
  } else if ( b.length < 1 ) {
    $('#loginStatus').html('Password cannot be Blank!');
    $('#loginStatus').attr('class', 'alert alert-danger');
  } else if ( b.length < 4 ) {
    $('#loginStatus').html('The password entered is too short!');
    $('#loginStatus').attr('class', 'alert alert-danger');
  } else {
    $.ajax({
      method: 'POST',
      url: '/core/logon.php',
      data: 'u=' + a + '&p=' + b,
      cache: false,
      success: function (data) {
        if ( data == 'Disabled' ) {
          $('#loginStatus').html('Your account has been disbaled. Please see your system administrator.');
          $('#loginStatus').attr('class', 'alert alert-danger');
        } else if ( data == 'No Match' ) {
          $('#loginStatus').html('The password is incorrect. Try again.');
          $('#loginStatus').attr('class', 'alert alert-danger');
        } else if ( data == 'No Exist' ) {
          $('#loginStatus').html('The user name or password is incorrect.');
          $('#loginStatus').attr('class', 'alert alert-danger');
        } else {
          $('#loginStatus').html('Login Successful!');
          $('#loginStatus').attr('class', 'alert alert-success');
          location.replace('/');
        }
      },
      error: function () {
        $('#loginStatus').html('Those details entered are not on record, try again.');
        $('#loginStatus').attr('class', 'alert alert-danger');
      }
    });
  }
}
// Logout
function DoLogout() {
  $.ajax({
    method: 'GET',
    url: '/core/logoff.php',
    cache: false,
    success: function () {
      // do nothing
    },
    complete: function () {
      location.replace('/');
    }
  });
}
// Listeners
function isLogin(title){
  var login = /(?:login)/i;
  if ( login.test(title) ) {
    return true;
  } else {
    return false;
  }
}
$(document).ready(function(e){
  if ( isLogin == true ) {
    $('#Login').click(function(){
      DoLogin();
    });
    $(document).keypress(function(event){
      var ascii = (event.keyCode ? event.keyCode : event.which);
      if ( ascii == '13' ) {
        DoLogin();
      }
    });
  }
  $('#Omnibox').keypress(function(event) {
    var ascii = (event.keyCode ? event.keyCode : event.which);
    if ( ascii == '13' ) {
      var Page_Title = $('#Page-Title').text();
      if ( Page_Title == 'Bill of Materials' ) {
        var a = '/mod/bom.list.search.php';
        var c = '#BoMViewport';
        var d = '#BOMShow';
        var e = 'List BoMs';
      } else if ( Page_Title == 'Stock' ) {
        var a = '/mod/stock.list.search.php';
        var c = '#Viewport';
        var d = '#stb1';
        var e = 'Back to Stock...';
      }
      if ($(this).val().length > 1) {
        var b = encodeURIComponent($(this).val());
        $.ajax({
          method: 'GET',
          url: a,
          data: 'q=' + b,
          cache: false,
          success: function (data) {
            $(c).html(data);
            $(d).text(e);
            $(d).attr('class', 'nav-link');
          },
          error: function () {
            soft400();
          }
        });
      }
    }
  })
});
// Legacy Modal Listeners
$('#Modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var action = button.data('action')
  var bomi = button.data('bomi')
  var itemno = button.data('itemno')
  var stockcode = button.data('stockcode')
  var description = button.data('desc')
  var qty = button.data('qty')
  var compref = button.data('compref')
  var job_no = button.data('job_no')
  var jbi = button.data('jbi')
  var productname = button.data('productname')
  var jqty = button.data('quantity')
  var modal = $(this)
  modal.find('#MYB').text('Yes');
  modal.find('#MYB').attr('class', 'btn btn-primary');
  modal.find('#MNB').text('No');
  modal.find('#MNB').attr('class', 'btn btn-secondary');
  modal.find('#MNB').attr('onclick', 'hideModal()');
  if ( action === "ebEditComp" ) {
    modal.find('.modal-title').text('Editing Component');
    modal.find('.modal-body').html('<h5>Current Component Info</h5><table class="table table-hover table-sm"><thead><tr><th>Item #</th><th>Stock Code</th><th>Description</th><th>Qty</th><th>Component Reference</th></tr></thead><tbody><tr><td>' + itemno + '</td><td>' + stockcode + '</td><td>' + description + '</td><td>' + qty + '</td><td>' + compref + '</td></tr></tbody></table><h5>Change to</h5><form autocomplete="off"><div class="form-group row"><div class="col-sm-1"><input id="ebcin" name="ebcin" class="form-control form-control-sm" placeholder="Item #" value="' + itemno + '" required></div><div class="col-sm-3"><input id="ebcsc" name="ebcsc" class="form-control form-control-sm" placeholder="Stock Code" value="' + stockcode + '" required></div><div class="col-sm-3"><input id="ebcsd" name="ebcsd" class="form-control form-control-sm" placeholder="Description" value="' + description + '" required></div><div class="col-sm-1"><input id="ebcq" name="ebcq" class="form-control form-control-sm" placeholder="Qty" value="' + qty + '" required></div><div class="col-sm-3"><input id="ebccr" name="ebccr" class="form-control form-control-sm" placeholder="Component Reference" value="' + compref + '"></div></div></form>');
    modal.find('.modal-dialog').attr('class', 'modal-dialog modal-xl');
    modal.find('#MYB').attr('onClick', 'ebUpdateComp(\'' + bomi + '\')');
    modal.find('#MYB').text('Save');
    modal.find('#MYB').attr('class', 'btn btn-warning');
    modal.find('#MNB').text('Cancel');
  } else if ( action === "ebDeleteComp" ) {
    modal.find('.modal-title').text('Delete Component');
    modal.find('.modal-body').html('<p>Are you sure you want to delete the following Component from this BoM?</p><h5>Current Component Info</h5><table class="table table-hover table-sm"><thead><tr><th>Item #</th><th>Stock Code</th><th>Description</th><th>Qty</th><th>Component Reference</th></tr></thead><tbody><tr><td>' + itemno + '</td><td>' + stockcode + '</td><td id="ebdd">' + description + '</td><td id="ebdq">' + qty + '</td><td>' + compref + '</td></tr></tbody></table>');
    modal.find('.modal-dialog').attr('class', 'modal-dialog modal-xl');
    modal.find('#MYB').attr('onClick', 'ebDeleteComp(\'' + bomi + '\')');
    modal.find('#MYB').text('Delete');
    modal.find('#MYB').attr('class', 'btn btn-danger');
    modal.find('#MNB').text('Cancel');
  } else if ( action === "Delete" ) {
    modal.find('.modal-title').text('Delete Job ' + job_no);
    modal.find('.modal-body').text('Are you sure you want to delete ' + job_no + '?');
    modal.find('#MYB').attr('onClick', 'deleteJob(\'' + job_no + '\')');
    modal.find('#MYB').attr('class', 'btn btn-danger');
  } else if ( action === "Complete" ) {
    modal.find('.modal-title').text('Mark Job ' + job_no + ' as Complete');
    modal.find('.modal-body').text('Are you sure you want to mark ' + job_no + ' as being complete?');
    modal.find('#MYB').attr('onClick', 'completeJob(\'' + job_no + '\')');
    modal.find('#MYB').attr('class', 'btn btn-info');
  } else if ( action === "Active" ) {
    modal.find('.modal-title').text('Mark Job ' + job_no + ' as Active...');
    modal.find('.modal-body').text('Are you sure you want to mark ' + job_no + ' as being active?');
    modal.find('#MYB').attr('onClick', 'activateJob(\'' + job_no + '\')');
    modal.find('#MYB').attr('class', 'btn btn-primary');
  } else if ( action === "Hold" ) {
    modal.find('.modal-title').text('Put Job ' + job_no + ' On Hold...');
    modal.find('.modal-body').text('Are you sure you want to mark ' + job_no + ' as being on hold?');
    modal.find('#MYB').attr('onClick', 'holdJob(\'' + job_no + '\')');
    modal.find('#MYB').attr('class', 'btn btn-warning');
  } else if ( action === "DeleteItem" ) {
    modal.find('.modal-title').text('Delete Product from Job ' + job_no);
    modal.find('.modal-body').html('Are you sure you want to delete the following item?<br /><table class="table table-hover table-sm"><thead><tr><th>Product</th><th>Qty</th></tr></thead><tbody><tr><td>' + productname + '</td><td>' + jqty + '</td></tr></tbody></table>');
    modal.find('.modal-dialog').attr('class', 'modal-dialog modal-lg');
    modal.find('#MYB').attr('onClick', 'editDeleteitem(\'' + jbi + '\', \'' + job_no + '\', \'' + jqty + '\', \'' + productname + '\')');
    modal.find('#MYB').attr('class', 'btn btn-danger');
  } else if ( action === "SaveEdit" ) {
    var ej0 = $('#job_no').text();
    var ej1 = $('#Status').val();
    var ej2 = $('#Customer').val();
    var ej3 = $('#Comments').val();
    if ( ej3.length < 1 ) {
      ej3 = "No Comments";
    }
    var ej4 = $('#Notes').val();
    if ( ej4.length < 1 ) {
      ej4 = "No Notes";
    }
    modal.find('.modal-title').text('Make Changes to Job ' + ej0);
    modal.find('.modal-body').html('Are you sure you want to save the following changes?<br /><table class="table table-hover table-sm"><thead><tr><th>Preview</th></tr></thead><tbody><tr><th scope="row">Status</th><td>' + ej1 + '</td></tr><tr><th scope="row">Customer</th><td>' + ej2 + '</td></tr><tr><th scope="row">Comments</th><td>' + ej3 + '</td></tr><tr><th scope="row">Additional Notes</th><td>' + ej4 + '</td></tr></tbody></table>');
    modal.find('.modal-dialog').attr('class', 'modal-dialog modal-lg');
    modal.find('#MYB').attr('onClick', 'editSaveChanges()');
    modal.find('#MYB').attr('class', 'btn btn-info');
  } else if ( action === "EditItem" ) {
    modal.find('.modal-title').text('Editing item');
    modal.find('.modal-body').html('<div class="form-group row"><div class="col-sm-10"><input class="form-control form-control-sm" id="mei1" type="text" value="' + productname + '" required></div><div class="col-sm-2"><input class="form-control form-control-sm" id="mei2" type="text" value="' + jqty + '" required></div></div>');
    modal.find('.modal-dialog').attr('class', 'modal-dialog modal-lg');
    modal.find('#MYB').attr('onClick', 'editExistingitem(\'' + jbi + '\')');
    modal.find('#MYB').text('Save');
    modal.find('#MYB').attr('class', 'btn btn-warning');
    modal.find('#MYB').text('Cancel');
  } else {
    modal.find('.modal-title').text('Feature hasn\'t arrived yet');
    modal.find('.modal-body').html('This feature has not left the birds nest, If the button is there. It\'ll be here soon!');
    modal.find('#MYB').attr('class', 'd-none');
    modal.find('#MNB').attr('class', 'btn btn-primary');
    modal.find('#MNB').text('Ok');
  }
});
