$(document).ready(function(e){
  $('#Customer').keyup(function(){
    if ($(this).val().length == 0) {
      $('#Customer_Autofill').hide();
    } else {
      $('#Customer_Autofill').show();
    }
    var text = encodeURIComponent($(this).val());
    $.ajax({
      method: 'GET',
      url: './mod/search.php',
      data: 'data=' + text + '&cat=' + "Customers",
      cache: false,
      success: function(data){
        $('#Customer_Autofill').html(data);
      }
    });
  })
  $('#Product').keyup(function(){
    if ($(this).val().length == 0) {
      $('#Products_Autofill').hide();
    } else {
      $('#Products_Autofill').show();
    }
    var text = encodeURIComponent($(this).val());
    $.ajax({
      method: 'GET',
      url: './mod/search.php',
      data: 'data=' + text + '&cat=' + "Products",
      cache: false,
      success: function(data){
        $('#Products_Autofill').html(data);
      }
    });
  })
  $('#Stock').keyup(function(){
    if ($(this).val().length == 0) {
      $('#Stock_Autofill').hide();
    } else {
      $('#Stock_Autofill').show();
    }
    var text = encodeURIComponent($(this).val());
    $.ajax({
      method: 'GET',
      url: './mod/search.php',
      data: 'data=' + text + '&cat=' + "Stock",
      cache: false,
      success: function(data){
        $('#Stock_Autofill').html(data);
      }
    });
  })
  $('#BomNBStock').keyup(function(){
    if ($(this).val().length == 0) {
      $('#Stock_Autofill').hide();
    } else {
      $('#Stock_Autofill').show();
    }
    var text = encodeURIComponent($(this).val());
    $.ajax({
      method: 'GET',
      url: './mod/search.php',
      data: 'data=' + text + '&cat=' + "Stock",
      cache: false,
      success: function(data){
        $('#Stock_Autofill').html(data);
      }
    });
  })
  $('#BomEBStock').keyup(function(){
    if ($(this).val().length == 0) {
      $('#Stock_Autofill').hide();
    } else {
      $('#Stock_Autofill').show();
    }
    var text = encodeURIComponent($(this).val());
    $.ajax({
      method: 'GET',
      url: './mod/search.php',
      data: 'data=' + text + '&cat=' + "Stock",
      cache: false,
      success: function(data){
        $('#Stock_Autofill').html(data);
      }
    });
  })
});
