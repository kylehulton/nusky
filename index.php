<?php require_once("core/system.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $prog_name; ?> | Start</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=1.0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=1.0)">
    <meta http-equiv="Site-Enter" content="blendTrans(Duration=1.0)">
    <meta http-equiv="Site-Exit" content="blendTrans(Duration=1.0)">
    <link rel="stylesheet" href="/css/knect.css" />
  </head>
  <body onload="DoStart()">
    <div class="modal fade" id="Modal" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog" id="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" id="modal-header">
            <h5 class="modal-title" id="ModalLabel"></h5>
            <button type="button" class="close close-light" data-dismiss="modal" aria-label="Close" id="modal-close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="modal-body">Loading Data...</div>
          <div class="modal-footer" id="modal-footer">
            <div id="ModalStatus"></div>
            <button class="btn btn-danger" id="MYB">Yes</button>
            <button class="btn btn-secondary" id="MNB">No</button>
          </div>
        </div>
      </div>
    </div>
    <div class="parallax neutral mvh-100">
      <header>
        <div class="ns-header acrylic dark">
          <div class="ns-header-left"><?php echo $prog_name; ?></div>
          <div class="ns-header-center">
            <div class="row">
              <div class="col-md-auto" id="Page-Title"></div>
              <div class="col mx-4" id="Status"></div>
            </div>
          </div>
          <div class="ns-header-right"><?php GetUserControls(); ?></div>
        </div>
      </header>
      <div class="ns-Wrapper mvh-100 acrylic neutral">
        <nav id="sidebar" role="navigation" aria-label="Navigation" class="ns-Nav nav flex-column nav-pills acrylic medium pt-3">
          <input class="form-control acrylic dark-medium" id="Omnibox" type="search">
          <a id="top"></a>
          <?php GetLinks(); ?>
        </nav>
        <div class="container-fluid acrylic pt-3 pb-3">
          <div class="ns-Content">
            <div id="Gogglebox">Loading...</div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="acrylic dark">
        <div class="ns-footer">
          <div class="ns-footer-left"><p>Powered by <a href="http://knect-it.co.uk/" target="_blank">knect</a><br /><sub id="version"></sub></p></div>
          <div class="ns-footer-center">
            <noscript>
              <div class="alert alert-dangeralert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <p><strong>Oh Shoot!</strong></p><br />
                <p>This page uses JavaScript. Either your browser doesn't support JavaScript, or you have turned it off.</p><br />
                <p>To view this page as it is meant to appear, please enable JavaScript or use a JavaScript-enabled browser.</p>
              </div>
            </noscript>
          </div>
          <div class="ns-footer-right">
            <img src="/lib/logo.png"><br />
          </div>
        </div>
      </footer>
      <!-- END of Footer -->
    </div>
    <script src="/js/popper.min.js"></script>
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/fabric.min.js"></script>
    <script src="/js/knect.js"></script>
  </body>
</html>
