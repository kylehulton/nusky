<?php function CheckIfUserDisabled($q) {
  $a = new database();
  $a->query('SELECT status FROM login WHERE username=:username');
  $a->bind(':username', $q);
  $a->execute();
  $b = $a->resultset();
  foreach ($b as $b){
    $c = $b['status'];
    return $c;
  }
}
if((isset($_SESSION['Logged_In'])) && ($_SESSION['Logged_In'] == TRUE)){
  $CU_Username = $_SESSION['Username'];
  $CU_First_Name = $_SESSION['First_Name'];
  $CU_Surname = $_SESSION['Surname'];
  $CU_Fullname = $_SESSION['displayname'];
  if ( CheckIfUserDisabled($_SESSION['Username']) != 'Active' ) {
    header('location: /login.html');
    exit();
  }
} else {
  header('location: /login.html');
  exit();
}
