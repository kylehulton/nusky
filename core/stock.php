<?php
class stock {
  function list ($s) {
    foreach ($s as $s) {
      $id = $s['stock_id'];
      $sc = $s['stock_co'];
      $sd = $s['stock_desc'];
      $sl = $s['OVERALL_STOCK_LEVEL'];
      if ($sl > 99) { $sl_w = 100; } else { $sl_w = $sl; }
      if ($sl > 50) { $sl_c = "positive"; } else if ($sl > 33) { $sl_c = "neutral"; } else if ($sl > 25) { $sl_c = "cautionary"; } else { $sl_c = "negative"; }
      $sto = new database();
      $sto->query('SELECT * FROM goods_in_items WHERE stock_co=:sc AND qty_now > 0');
      $sto->bind(':sc', $sc);
      $sto->execute();
      $stored = $sto->fetchAll();
      echo '<tr data-toggle="collapse" data-target="#'.$id.'" class="accordion-toggle" >
        <td>'.$sc.'</td>
        <td>'.$sd.'</td>
        <td><div class="progress acrylic dark-max">
          <div class="progress-bar acrylic '.$sl_c.'" role="progressbar" style="width: '.$sl_w.'%;" aria-valuenow="'.$sl.'" aria-valuemin="0" aria-valuemax="100"></div>
          <span>'.$sl.'</span>
          </div>
        </td>
      </tr>';
      if (!empty($stored)){
        echo '<tr>
          <td colspan="3" class="py-0 border-0">
            <div class="accordian-body collapse py-2" id="'.$id.'" data-parent=".table" >';
      }
      foreach ($stored as $stored) {
        $bin = $stored['stock_loc'];
        $stor = $stored['store_loc'];
        $qty = $stored['qty_now'];
        echo ' <button class="btn acrylic btn-neutral" onclick="StoresOpenBox(\''.$bin.'\',\''.$stor.'\')">'.$bin.' <span class="badge acrylic badge-neutral">'.$qty.'</span></button> ';
      }
      if (!empty($stored)){
        echo '</div>
          </td>
        </tr>';
      }
    }
  }
}
