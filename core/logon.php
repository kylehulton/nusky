<?php session_start(); $config_dir = "config"; $core_dir = "core"; require_once("$config_dir/database.php"); require_once("$core_dir/database.php");
if (isset($_POST['u']) && isset($_POST['p'])){
  $inputUsername = strtolower(strip_tags($_POST['u']));
  $inputPassword = sha1(strip_tags($_POST['p']));
  $chkUser = new database();
  $chkUser->query('SELECT username FROM login WHERE username=:username');
  $chkUser->bind(':username', $inputUsername);
  $chkUser->execute();
  $UserCount = $chkUser->rowCount();
  if ( $UserCount < 1 ) {
    $_SESSION['Logged_In'] = FALSE;
    exit('No Exist');
  } else {
    $GetUser = new database();
    $GetUser->query('SELECT username, password, firstname, surname, displayname, status FROM login WHERE username=:username');
    $GetUser->bind(':username', $inputUsername);
    $GetUser->execute();
    $info = $GetUser->resultset();
    foreach ( $info as $info ){
      if ( $inputPassword != $info['password'] ) {
        $_SESSION['Logged_In'] = FALSE;
        exit ('No Match');
      } else if ( $info['status'] == "Disabled") {
        $_SESSION['Logged_In'] = FALSE;
        exit('Disabled');
      } else {
        // password is right
        $_SESSION['Logged_In'] = TRUE;
        $_SESSION['Username'] = $info['username'];
        $_SESSION['First_Name'] = $info['firstname'];
        $_SESSION['Surname'] = $info['surname'];
        $_SESSION['displayname'] = $info['displayname'];
      }
    }
    exit();
  }
} else {
  header('HTTP/1.0 418');
}
