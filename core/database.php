<?php define ("Database_Type", $GLOBALS['Database_Type']);
define ("Database_Host", $GLOBALS['Database_Host']);
define ("Database_Name", $GLOBALS['Database_Name']);
define ("Database_User", $GLOBALS['Database_User']);
define ("Database_Password", $GLOBALS['Database_Password']);
$db = new PDO($GLOBALS['Database_Type'].':host='.$GLOBALS['Database_Host'].';dbname='.$GLOBALS['Database_Name'], $GLOBALS['Database_User'], $GLOBALS['Database_Password']);
class Database {
  private $Type = Database_Type;
  private $Hostname = Database_Host;
  private $User = Database_User;
  private $Password = Database_Password;
  private $Database_Name = Database_Name;
  private $Database_Instance;
  private $PDO_Error;
  public function __construct(){
      $Database_Source = $this->Type .':host='.$this->Hostname.';dbname='.$this->Database_Name;
      $PDO_Options = array(PDO::ATTR_EMULATE_PREPARES => 1, PDO::ATTR_PERSISTENT => 1, PDO::ERRMODE_EXCEPTION => 1);
      try{
          $this->Database_Instance = new PDO($Database_Source, $this->User, $this->Password, $PDO_Options);
      }
      catch(PDOException $e){
          $this->PDO_Error = $e->getMessage();
      }
  }
  private $stmt;
  public function query($query){
      $this->stmt = $this->Database_Instance->prepare($query);
  }
  public function bind($param, $value, $type = null){
      if (is_null($type)) {
          switch (true) {
              case is_int($value):
                  $type = PDO::PARAM_INT;
                  break;
              case is_bool($value):
                  $type = PDO::PARAM_BOOL;
                  break;
              case is_null($value):
                  $type = PDO::PARAM_NULL;
                  break;
              default:
                  $type = PDO::PARAM_STR;
          }
      }
      $this->stmt->bindValue($param, $value, $type);
  }
  public function execute(){
      return $this->stmt->execute();
  }
  public function resultset(){
      $this->execute();
      return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
  }
  public function single(){
      $this->execute();
      return $this->stmt->fetch(PDO::FETCH_ASSOC);
  }
  public function fetch(){
      $this->execute();
      return $this->stmt->fetch(PDO::FETCH_ASSOC);
  }
  public function fetchAll(){
      $this->execute();
      return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
  }
  public function rowCount(){
      return $this->stmt->rowCount();
  }
  public function lastInsertId(){
      return $this->Database_Instance->lastInsertId();
  }
  public function beginTransaction(){
      return $this->Database_Instance->beginTransaction();
  }
  public function endTransaction(){
      return $this->Database_Instance->commit();
  }
  public function cancelTransaction(){
      return $this->Database_Instance->rollBack();
  }
  public function debugDumpParams(){
      return $this->stmt->debugDumpParams();
  }
}
