<?php session_start(); $prog_name = "nuSky"; $config_dir = "config"; $core_dir = "core"; $modules_dir = "mod"; $servertime = date("d-m-Y H:i:s"); $UTCtime = gmdate("d-m-Y H:i:s"); $GMTtime = $UTCtime; date_default_timezone_set('Europe/London');
$default_time = date("d-m-Y H:i:s"); $build_time = date("YmdHis"); $debug_timer = microtime(true); $_SESSION['debug_timer'] = $debug_timer; $time = date("Hi"); $day = date("D"); $month = date("M"); $year = date("Y"); $date_now = date("Y-m-d");
$clientip = $_SERVER['REMOTE_ADDR']; $filename = htmlspecialchars($_SERVER["SCRIPT_NAME"]); $thispage = htmlspecialchars($_SERVER["REQUEST_URI"]); $num_links = 10;
if(empty($_GET['action'])){ $action = FALSE; } else { $action = $_GET['action']; } if(empty($_GET['data'])){ $data = FALSE; } else { $data = $_GET['data']; }
if(empty($_GET['dir'])){ $dir = FALSE; } else { $dir = $_GET['dir']; } if(empty($_GET['page'])){ $page = 1; } else { $page = $_GET['page']; }
$Limit_A=0; $Limit_B=25; $Limit_A=($page-1)*$Limit_B;
function number_format_drop_zero_decimals($n, $n_decimals) {
  return ((floor($n) == round($n, $n_decimals)) ? number_format($n) : number_format($n, $n_decimals));
} $links = array (
  array("Start","#",'<i class="ms-Icon ms-Icon--Tiles" aria-hidden="true"></i> Start',"Start","active","DoStart()"),
  array("Customers","#",'<i class="ms-Icon ms-Icon--People" aria-hidden="true"></i> Customers',"Cust","active","CustomersLoad()"),
  array("Jobs","#",'<i class="ms-Icon ms-Icon--Event" aria-hidden="true"></i> Jobs',"Jobs","active","JobsLoad()"),
  array("BoM","#",'<i class="ms-Icon ms-Icon--TextDocument" aria-hidden="true"></i> Bill of Materials',"BOMs","active","BOMLoad()"),
  array("Stock","#",'<i class="ms-Icon ms-Icon--Package" aria-hidden="true"></i> Stock',"NavStock","active","StockLoad()"),
  array("Stores","#",'<i class="ms-Icon ms-Icon--Packages" aria-hidden="true"></i> Stores',"Stores","active","StoresLoad()"),
  array("Deliveries","#",'<i class="ms-Icon ms-Icon--DeliveryTruck" aria-hidden="true"></i> Deliveries',"Goods","active","GoodsLoad()"),
  array("Works Orders","#",'<i class="ms-Icon ms-Icon--ActivateOrders" aria-hidden="true"></i> Works Orders',"WO","active","WorksLoad()"),
  array("Purchasing","#",'<i class="ms-Icon ms-Icon--ShoppingCart" aria-hidden="true"></i> Purchasing',"Purchasing","active","PurchasingLoad()"),
  array("Suppliers","#",'<i class="ms-Icon ms-Icon--ShopServer" aria-hidden="true"></i> Suppliers',"Suppliers","active","SuppliersLoad()")
// Title[0], Href[1], Friendly Name[2], ID[3], Active/Disabled[4], onClick[5], IsLink [6], IsTile[7], IsDropdown[8]
);
require_once("$config_dir/database.php");
require_once("$core_dir/database.php");
require_once("$core_dir/sessioninfo.php"); // Session to Vars
require_once("$core_dir/menus.php");
require_once("$core_dir/bom.php");
require_once("$core_dir/stock.php");
require_once("$core_dir/stores.php");
require_once("$core_dir/pagination.php");
$costingversion = "0.2";
