<?php
class bom {
  function modified($b) {
    global $CU_Fullname, $date_now;
    $ub = new database();
    $ub->query('UPDATE BOM SET date_modified=:date_modified, modified_by=:modified_by WHERE bom=:b');
    $ub->bind('date_modified', $date_now);
    $ub->bind('modified_by', $CU_Fullname);
    $ub->bind('b', $b);
    $ub->execute();
  }
  function list($b){
    global $costingversion;
    foreach ($b as $b) {
      $id = $b['BOM_ID'];
      $bom = $b['BOM'];
      $name = $b['FriendlyName'];
      $cost = $b['Cost'];
      $version = $b['costingversion'];
      if (empty($cost) || ($version != $costingversion)) {
        $str_cost = "Cost";
        $costing = false;
      } else {
        $str_cost = "Re-Cost";
        $costing = true;
      }
      if ($costing == true){
        $str_viewcost = '<button class="btn dropdown-item" onclick="BOMViewCost(\''.$bom.'\')">View Costing</button>';
        $str_batchcost = '<button class="btn dropdown-item" onclick="BOMBatchCost(\''.$bom.'\')">Batch Cost</button>';
      } else {
        $str_viewcost = '<button class="btn dropdown-item disabled">View Costing</button>';
        $str_batchcost = '<button class="btn dropdown-item disabled">Batch Cost</button>';
      }
      echo '<tr ondblclick="BOMView(\''.$bom.'\')">
      <td>'.$bom.'</td>
      <td>'.$name.'</td>
      <td>
        <button id="Actions" type="button" class="badge acrylic badge-neutral btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Actions">
          <h6 class="dropdown-header">Actions for '.$bom.'</h6>
          <div class="row">
            <div class="col-6">
              <button class="btn dropdown-item" onclick="BOMView(\''.$bom.'\')"><b>View</b></button>
              <button class="btn dropdown-item" onclick="BOMDuplicate(\''.$bom.'\')">Duplicate</button>
              <button class="btn dropdown-item" onclick="BOMEdit(\''.$bom.'\')">Edit</button>
              <button class="btn dropdown-item" onclick="BOMDelete(\''.$bom.'\')">Delete</button>
            </div>
            <div class="col-6">
              <button class="btn dropdown-item" onclick="BOMCost(\''.$bom.'\')">'.$str_cost.'</button>
              '.$str_viewcost.'
              '.$str_batchcost.'
            </div>
          </div>
        </div>
      </td>
    </tr>';
    }
  }
}
