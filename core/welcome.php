<?php require_once('core/system.php'); ?>
<div class="jumbotron jumbotron-fluid">
  <h1 class="display-4"><?php
    if (($time >= "0500") && ($time <= "0759")) { $greeting=1; }
    elseif (($time >= "0800") && ($time <= "0949") | ($time >= "1015") && ($time <= "1159")) { $greeting=2; }
    elseif (($time >= "0950") && ($time <= "0959") | ($time >= "1220") && ($time <= "1229") | ($time >= "1450") && ($time <= "1459") | ($time >= "1515") && ($time <= "1524")) { $greeting=9; }
    elseif (($time >= "1000") && ($time <= "1014") | ($time >= "1500") && ($time <= "1514")) { $greeting=3; }
    elseif (($time >= "1230") && ($time <= "1259")) { $greeting=4; }
    elseif (($time >= "1200") && ($time <= "1219") | ($time >= "1300") && ($time <= "1450") | ($time >= "1525") && ($time <= "1629")) { $greeting=5; }
    elseif (($time >= "1630") && ($time <= "1729")) { $greeting=6; }
    else { $greeting=0; }
    switch ($greeting) {
    case 0: printf("You're still here? It's over. Go Home. Go."); break;
    case 1: printf("You're Keen! Morning, $CU_First_Name."); break;
    case 2: printf("Good Morning, $CU_First_Name."); break;
    case 3: printf("You do know it's break time? Go and get your brew $CU_First_Name!"); break;
    case 4: printf("You do know it's lunch? Race to the microwave, $CU_First_Name!"); break;
    case 5: printf("Good Afternoon, $CU_First_Name."); break;
    case 6: printf("Shouldn't you be going home $CU_First_Name?"); break;
    case 7: printf("It's Friday, It's hometime now!"); break;
    case 8: printf("What are you doing, it's the weekend?!"); break;
    case 9: printf("It's Jeff O'clock!"); break; }
    ?>
  </h1>
</div>
<div class="d-flex flex-wrap">
  <?php GetTiles(); ?>
</div>
