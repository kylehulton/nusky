<?php $GLOBALS['pages'] = '';
function PrepPagination(){
  global $call, $lastpage, $page, $breakup, $next, $previous, $pages;
  if ($lastpage > 1){
    $pages.= '<ul class="pagination acrylic max">';
    if ($page > 1){
      $pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$previous.'\')">Previous</a></li>';
    } else {
      $pages.= '<li class="page-item disabled"><span class="page-link">Previous</span></li>';
    }
    if ($lastpage < 2 + ($breakup * 2)){ // Not enough pages to bother breaking up
      for ($counter = 1; $counter <= $lastpage; $counter++){
        if ($counter == $page){
          $pages.= '<li class="page-item active"><span class="page-link">'.$page.'<span class="sr-only">(current)</span></span></li>';
        } else {
          $pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$counter.'\')">'.$counter.'</a></li>';
        }
      }
    } elseif ($lastpage > 5 + ($breakup * 2)){ // enough pages to hide some
      if ($page < 1 + ($breakup * 2)){ // close to beginnging, only show earlier pages
        for ($counter = 1; $counter < 2 + ($breakup * 2); $counter++){
          if ($counter == $page){
            $pages.= '<li class="page-item active"><span class="page-link">'.$page.'<span class="sr-only">(current)</span></span></li>';
          } else {
            $pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$counter.'\')">'.$counter.'</a></li>';
          }
        }
        $pages.= '<li class="page-item"><span class="page-link">...</span></li>';
        $pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$lastpage.'\')">'.$lastpage.'</a></li>';
      } elseif ($lastpage - ($breakup * 2) > $page && $page > ($breakup * 2)){ // in the middle, hide some beginning and the end
        $pages.= '<li class="page-item"><span class="page-link"><a href="#" onclick="'.$call.'(\'1\')">1</a></span></li>';
  			$pages.= '<li class="page-item"><span class="page-link">...</span></li>';
        for ($counter = $page - $breakup; $counter <= $page + $breakup; $counter++){
  				if ($counter == $page){
  					$pages.= '<li class="page-item active"><span class="page-link">'.$page.'<span class="sr-only">(current)</span></span></li>';
  				} else {
  					$pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$counter.'\')">'.$counter.'</a></li>';
          }
        }
        $pages.= '<li class="page-item"><span class="page-link">...</span></li>';
        $pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$lastpage.'\')">'.$lastpage.'</a></li>';
      } else { // close to end, show later pages
        $pages.= '<li class="page-item"><span class="page-link"><a href="#" onclick="'.$call.'(\'1\')">1</a></span></li>';
  			$pages.= '<li class="page-item"><span class="page-link">...</span></li>';
        for ($counter = $lastpage - (2 + ($breakup * 2)); $counter <= $lastpage; $counter++){
          if ($counter == $page){
  					$pages.= '<li class="page-item active"><span class="page-link">'.$page.'<span class="sr-only">(current)</span></span></li>';
  				} else {
  					$pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$counter.'\')">'.$counter.'</a></li>';
          }
        }
      }
    }
    if ($page < $lastpage){
      $pages.= '<li class="page-item"><a class="page-link" href="#" onclick="'.$call.'(\''.$next.'\')">Next</a></li>';
    } else {
      $pages.= '<li class="page-item disabled"><span class="page-link">Next</span></li>';
    }
    $pages.= '</ul>';
  }
}

function Pagination(){
  global $pages;
  echo $pages;
}
