<?php
class stores {
  function listLocations () {
    $a = new database();
    $a->query('SELECT * FROM stores_locations');
    $a->execute();
    $b = $a->fetchAll();
    foreach ($b as $b) {
      $c = $b['stores_loc_code'];
      $d = $b['stores_loc_name'];
      if (empty($d)) {
        $d = $c;
      }
      echo '<div class="tile">
        <a title="'.$d.'" href="#" onclick="StoresOpenLocation(\''.$c.'\')">
          <div class="name">
            <span class="name align-bottom"><i class="ms-Icon ms-Icon--Package" aria-hidden="true"></i> '.$d.'</span>
          </div>
        </a>
      </div>';
    }
  }
  function listBins ($a) {
    $b = new database();
    $b->query('SELECT DISTINCT idstores, stores_name, stores_location, LEFT(stores_name, 1) as alphanum FROM stores WHERE stores_location=:c');
    $b->bind('c', $a);
    $b->execute();
    $c = $b->fetchAll();
    echo '<div class="row position-relative vh-75 overflow-y scroll" id="storesscroll" data-spy="scroll" data-target="#stores-navscroll" data-offset="20">';
    foreach ($c as $c) {
      $d = $c['stores_name'];
      $e = $c['alphanum'];
      echo '<div class="tile" id="bin'.$e.'">
        <a title="'.$d.'" href="#" onclick="StoresOpenBox(\''.$d.'\', \''.$a.'\')">
          <div class="name">
            <span class="name align-bottom"><i class="ms-Icon ms-Icon--Package" aria-hidden="true"></i> '.$d.'</span>
          </div>
        </a>
      </div>';
    }
    $b->query('SELECT DISTINCT LEFT(stores_name, 1) as alphanum FROM stores WHERE stores_location=:c');
    $b->bind('c', $a);
    $b->execute();
    $f = $b->fetchAll();
    echo '</div>
    <nav class="navbar navbar-light" id="stores-navscroll">
      <ul class="nav nav-pills">';
    foreach ($f as $f) {
      $g = $f['alphanum'];
      echo '<li class="nav-item">
        <a class="nav-link" href="#bin'.$g.'">'.$g.'</a>
      </li>';
    }
    echo '</ul>
    </nav>';
  }
}
