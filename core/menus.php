<?php
function GetLinks(){ global $links, $num_links;
  for ($link = 0; $link < $num_links; $link++){
    echo "<a class=\"nav-link\" id=\"".$links[$link]['3']."\" title=\"".$links[$link]['0']."\" href=\"".$links[$link]['1']."\" onclick=\"".$links[$link]['5']."\">".$links[$link]['2']."</a>";
  }
}
function GetTiles(){ global $links, $num_links;
  for ($tile = 0; $tile < $num_links; $tile++){
    echo "<div class=\"tile\">
      <a title=\"".$links[$tile]['0']."\" href=\"".$links[$tile]['1']."\" onclick=\"".$links[$tile]['5']."\">
        <div class=\"name\">
          <span class=\"name align-bottom\">".$links[$tile]['2']."</span>
        </div>
      </a>
    </div>";
  }
}
function GetUserControls(){ global $thispage, $CU_Fullname, $CU_Username; ?>
<form action="<?php echo $thispage; ?>" method="post">
  <div class="btn-group">
    <button id="UserControls" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ms-Icon ms-Icon--Contact" aria-hidden="true"></i> <?php echo $CU_Fullname; ?> </button>
    <div class="dropdown-menu mt-3 dropdown-menu-right" aria-labelledby="UserControls">
      <a class="btn dropdown-item" href="#" onclick="ScratchpadLoad()"><i class="ms-Icon ms-Icon--TextDocument" aria-hidden="true"></i> Scratchpad</a>
      <a class="btn dropdown-item" href="#" onclick="TimesheetLoad()"><i class="ms-Icon ms-Icon--TimeSheet" aria-hidden="true"></i> Timesheet</a>
      <div class="dropdown-divider"></div>
      <a class="btn dropdown-item" href="#" onclick="SettingsLoad()"><i class="ms-Icon ms-Icon--Settings" aria-hidden="true"></i> Settings</a>
      <?php if ($CU_Username == "kyleh") { echo '<a class="btn dropdown-item" href="#" onclick="AdmLoad()"><i class="ms-Icon ms-Icon--Admin" aria-hidden="true"></i> Administrative Tools</a>'; } ?>
      <div class="dropdown-divider"></div>
      <a class="btn dropdown-item" href="#" onclick="DoLogout()"><i class="ms-Icon ms-Icon--Unlock" aria-hidden="true"></i> Logout...</a>
    </div>
  </div>
</form><?php } ?>
